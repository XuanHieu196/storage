<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("name", 200)->unique();
            $table->char("code")->unique();
            $table->enum("type", ['DIVISION', 'ROLE'])->default('DIVISION');
            $table->text("description")->nullable();
            $table->bigInteger("group_id")->unsigned()->nullable();
            $table->boolean("is_active")->default('1');
            $table->dateTime("created_at")->nullable();
            $table->string("created_by", 20)->nullable();
            $table->dateTime("updated_at")->nullable();
            $table->string("updated_by", 20)->nullable();
            $table->dateTime("deleted_at")->nullable();
            $table->string("deleted_by", 20)->nullable();
            $table->foreign('group_id')->references('id')->on('permission_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
