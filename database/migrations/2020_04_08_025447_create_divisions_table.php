<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->char("code")->unique();
            $table->string("name", 50)->unique();
            $table->string("description", 255)->nullable();
            $table->enum("type", ['DEPARTMENT', 'PROJECT', 'TEAM'])->default('DEPARTMENT')->nullable();
            $table->boolean("is_active")->default('1');
            $table->timestamp("created_at")->nullable();
            $table->string("created_by", 50)->nullable();
            $table->timestamp("updated_at")->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamp("deleted_at")->nullable();
            $table->string("deleted_by", 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
