<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('division_permissions', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("detail_id")->unsigned();
            $table->bigInteger("permission_id")->unsigned();
            $table->text("description")->nullable();
            $table->boolean("is_active")->default('1')->nullable();
            $table->dateTime("created_at")->nullable();
            $table->string("created_by", 20)->nullable();
            $table->dateTime("updated_at")->nullable();
            $table->string("updated_by", 20)->nullable();
            $table->dateTime("deleted_at")->nullable();
            $table->string("deleted_by", 20)->nullable();
            $table->foreign('detail_id')->references('id')->on('division_details')->onDelete('cascade');
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
            $table->unique(['detail_id', 'permission_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('division_permissions');
    }
}
