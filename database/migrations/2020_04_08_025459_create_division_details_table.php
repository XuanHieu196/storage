<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createDivisionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('division_details', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("user_id")->unsigned()->nullable();
            $table->bigInteger("division_id")->unsigned();
            $table->enum("type", ['LEADER', 'EMPLOYEE'])->default('EMPLOYEE')->nullable();
            $table->string("description", 255)->nullable();
            $table->dateTime("start_time")->nullable();
            $table->dateTime("end_time")->nullable();
            $table->boolean("is_active")->default('1');
            $table->timestamp("created_at")->nullable();
            $table->string("created_by", 50)->nullable();
            $table->timestamp("updated_at")->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamp("deleted_at")->nullable();
            $table->string("deleted_by", 50)->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('division_details');
    }
}
