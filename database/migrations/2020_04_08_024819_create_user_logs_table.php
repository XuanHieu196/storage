<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_logs', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("action", 50);
            $table->string("target", 255)->nullable();
            $table->string("ip", 100)->nullable();
            $table->text("description")->nullable();
            $table->dateTime("created_at")->nullable();
            $table->string("created_by", 20)->nullable();
            $table->dateTime("updated_at");
            $table->string("updated_by", 20)->nullable();
            $table->dateTime("deleted_at")->nullable();
            $table->string("deleted_by", 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_logs');
    }
}
