<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("code", 32)->unique();
            $table->string("name", 200)->nullable();
            $table->string("key_url", 200)->unique();
            $table->string("key");
            $table->string("title", 200)->nullable();
            $table->bigInteger("folder_id")->unsigned()->nullable();
            $table->string("extension", 200)->nullable();
            $table->integer("version")->nullable();
            $table->string("size", 200)->nullable();
            $table->boolean("is_active")->default('1')->nullable();
            $table->dateTime("created_at")->nullable();
            $table->string("created_by", 20)->nullable();
            $table->dateTime("updated_at")->nullable();
            $table->string("updated_by", 20)->nullable();
            $table->dateTime("deleted_at")->nullable();
            $table->string("deleted_by", 20)->nullable();
            $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
