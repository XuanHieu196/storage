<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createMediaSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_shares', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("division_id")->unsigned()->nullable();
            $table->bigInteger("file_id")->unsigned()->nullable();
            $table->bigInteger("folder_id")->unsigned()->nullable();
            $table->bigInteger("user_id")->unsigned()->nullable();
            $table->enum("permission", ['VIEW', 'COMMENT', 'UPDATE'])->default('VIEW')->nullable();
            $table->boolean("is_active")->default('1')->nullable();
            $table->boolean("deleted")->nullable();
            $table->dateTime("created_at")->nullable();
            $table->string("created_by", 20)->nullable();
            $table->dateTime("updated_at")->nullable();
            $table->string("updated_by", 20)->nullable();
            $table->dateTime("deleted_at")->nullable();
            $table->string("deleted_by", 20)->nullable();
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade');
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
            $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_shares');
    }
}
