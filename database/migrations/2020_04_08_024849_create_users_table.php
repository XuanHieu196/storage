<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->char("code")->unique();
            $table->string("username", 50);
            $table->string("email", 50)->unique();
            $table->string("password", 72);
            $table->string("phone", 14);
            $table->enum("type", ['ADMIN', 'SUPER_ADMIN', 'SALE', 'ACCOUNTANT', 'UNKNOWN'])->default('UNKNOWN')->nullable();
            $table->string("verify_code", 6)->nullable();
            $table->dateTime("expired_code")->nullable();
            $table->bigInteger("role_id")->unsigned()->nullable();
            $table->string("note", 300)->nullable();
            $table->boolean("is_active")->default('1');
            $table->timestamp("created_at")->nullable();
            $table->string("created_by", 50)->nullable();
            $table->timestamp("updated_at")->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamp("deleted_at")->nullable();
            $table->string("deleted_by", 50)->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
