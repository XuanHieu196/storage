<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsShareAndPermissionColumnFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('folders', function (Blueprint $table) {
            $table->boolean('is_share')->default(0);
            $table->enum("permission", ['VIEW', 'COMMENT', 'UPDATE'])->default('VIEW')->nullable();
        });
        Schema::table('files', function (Blueprint $table) {
            $table->boolean('is_share')->default(0);
            $table->enum("permission", ['VIEW', 'COMMENT', 'UPDATE'])->default('VIEW')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('folders', function (Blueprint $table) {
            $table->dropColumn(['is_share', 'permission']);
        });

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn(['is_share', 'permission']);
        });
    }
}
