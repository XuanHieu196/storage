<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("user_id")->unsigned();
            $table->string("email", 50)->nullable();
            $table->string("first_name", 100);
            $table->string("last_name", 100)->nullable();
            $table->string("full_name", 100);
            $table->string("address", 500)->nullable();
            $table->string("phone", 20)->nullable();
            $table->dateTime("birthday")->nullable();
            $table->enum("genre", ['Male', 'Female', 'Other'])->default('Other');
            $table->text("avatar")->nullable();
            $table->boolean("is_active")->default('1');
            $table->timestamp("created_at")->nullable();
            $table->string("created_by", 50)->nullable();
            $table->timestamp("updated_at")->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamp("deleted_at")->nullable();
            $table->string("deleted_by", 50)->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
