<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->char("code")->unique();
            $table->string("name", 100);
            $table->text("description")->nullable();
            $table->boolean("is_active")->default('1');
            $table->integer("level")->nullable();
            $table->dateTime("created_at")->nullable();
            $table->string("created_by", 20)->nullable();
            $table->dateTime("updated_at")->nullable();
            $table->string("updated_by", 20)->nullable();
            $table->dateTime("deleted_at")->nullable();
            $table->string("deleted_by", 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
