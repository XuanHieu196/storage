<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->truncate();
        \App\Models\Permission::insert([
            [
                'id'       => 1,
                'name'     => 'VIEW DIVISION',
                'code'     => 'VIEW-DIVISION',
                'type'     => 'ROLE',
                'group_id' => 1
            ],
            [
                'id'    => 2,
                'name'     => 'CREATE DIVISION',
                'code'     => 'CREATE-DIVISION',
                'type'     => 'ROLE',
                'group_id' => 1
            ],
            [
                'id'    => 3,
                'name'     => 'UPDATE DIVISION',
                'code'     => 'UPDATE-DIVISION',
                'type'     => 'ROLE',
                'group_id' => 1
            ],
            [
                'id'    => 4,
                'name'     => 'DELETE DIVISION',
                'code'     => 'DELETE-DIVISION',
                'type'     => 'ROLE',
                'group_id' => 1
            ],
            [
                'id'       => 5,
                'name'     => 'VIEW DIVISION DETAIL',
                'code'     => 'VIEW-DIVISION-DETAIL',
                'type'     => 'ROLE',
                'group_id' => 2
            ],
            [
                'id'    => 6,
                'name'     => 'CREATE DIVISION DETAIL',
                'code'     => 'CREATE-DIVISION-DETAIL',
                'type'     => 'ROLE',
                'group_id' => 2
            ],
            [
                'id'    => 7,
                'name'     => 'UPDATE DIVISION DETAIL',
                'code'     => 'UPDATE-DIVISION-DETAIL',
                'type'     => 'ROLE',
                'group_id' => 2
            ],
            [
                'id'    => 8,
                'name'     => 'DELETE DIVISION DETAIL',
                'code'     => 'DELETE-DIVISION-DETAIL',
                'type'     => 'ROLE',
                'group_id' => 2
            ],
            [
                'id'       => 9,
                'name'     => 'VIEW DIVISION PERMISSION',
                'code'     => 'VIEW-DIVISION-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 3
            ],
            [
                'id'    => 10,
                'name'     => 'CREATE DIVISION PERMISSION',
                'code'     => 'CREATE-DIVISION-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 3
            ],
            [
                'id'    => 11,
                'name'     => 'UPDATE DIVISION PERMISSION',
                'code'     => 'UPDATE-DIVISION-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 3
            ],
            [
                'id'    => 12,
                'name'     => 'DELETE DIVISION PERMISSION',
                'code'     => 'DELETE-DIVISION-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 3
            ],
            [
                'id'       => 13,
                'name'     => 'VIEW FILE',
                'code'     => 'VIEW-FILE',
                'type'     => 'ROLE',
                'group_id' => 4
            ],
            [
                'id'    => 14,
                'name'     => 'CREATE FILE',
                'code'     => 'CREATE-FILE',
                'type'     => 'ROLE',
                'group_id' => 4
            ],
            [
                'id'    => 15,
                'name'     => 'UPDATE FILE',
                'code'     => 'UPDATE-FILE',
                'type'     => 'ROLE',
                'group_id' => 4
            ],
            [
                'id'    => 16,
                'name'     => 'DELETE FILE',
                'code'     => 'DELETE-FILE',
                'type'     => 'ROLE',
                'group_id' => 4
            ],
            [
                'id'       => 17,
                'name'     => 'VIEW FOLDER',
                'code'     => 'VIEW-FOLDER',
                'type'     => 'ROLE',
                'group_id' => 5
            ],
            [
                'id'    => 18,
                'name'     => 'CREATE FOLDER',
                'code'     => 'CREATE-FOLDER',
                'type'     => 'ROLE',
                'group_id' => 5
            ],
            [
                'id'    => 19,
                'name'     => 'UPDATE FOLDER',
                'code'     => 'UPDATE-FOLDER',
                'type'     => 'ROLE',
                'group_id' => 5
            ],
            [
                'id'    => 20,
                'name'     => 'DELETE FOLDER',
                'code'     => 'DELETE-FOLDER',
                'type'     => 'ROLE',
                'group_id' => 5
            ],
            [
                'id'       => 21,
                'name'     => 'VIEW MEDIA SHARE',
                'code'     => 'VIEW-MEDIA-SHARE',
                'type'     => 'ROLE',
                'group_id' => 6
            ],
            [
                'id'    => 22,
                'name'     => 'CREATE MEDIA SHARE',
                'code'     => 'CREATE-MEDIA-SHARE',
                'type'     => 'ROLE',
                'group_id' => 6
            ],
            [
                'id'    => 23,
                'name'     => 'UPDATE MEDIA SHARE',
                'code'     => 'UPDATE-MEDIA-SHARE',
                'type'     => 'ROLE',
                'group_id' => 6
            ],
            [
                'id'    => 24,
                'name'     => 'DELETE MEDIA SHARE',
                'code'     => 'DELETE-MEDIA-SHARE',
                'type'     => 'ROLE',
                'group_id' => 6
            ],
            [
                'id'       => 25,
                'name'     => 'VIEW PERMISSION',
                'code'     => 'VIEW-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 7
            ],
            [
                'id'    => 26,
                'name'     => 'CREATE PERMISSION',
                'code'     => 'CREATE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 7
            ],
            [
                'id'    => 27,
                'name'     => 'UPDATE PERMISSION',
                'code'     => 'UPDATE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 7
            ],
            [
                'id'    => 28,
                'name'     => 'DELETE PERMISSION',
                'code'     => 'DELETE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 7
            ],
            [
                'id'       => 29,
                'name'     => 'VIEW PERMISSION GROUP',
                'code'     => 'VIEW-PERMISSION-GROUP',
                'type'     => 'ROLE',
                'group_id' => 8
            ],
            [
                'id'    => 30,
                'name'     => 'CREATE PERMISSION GROUP',
                'code'     => 'CREATE-PERMISSION-GROUP',
                'type'     => 'ROLE',
                'group_id' => 8
            ],
            [
                'id'    => 31,
                'name'     => 'UPDATE PERMISSION GROUP',
                'code'     => 'UPDATE-PERMISSION-GROUP',
                'type'     => 'ROLE',
                'group_id' => 8
            ],
            [
                'id'    => 32,
                'name'     => 'DELETE PERMISSION GROUP',
                'code'     => 'DELETE-PERMISSION-GROUP',
                'type'     => 'ROLE',
                'group_id' => 8
            ],
            [
                'id'       => 33,
                'name'     => 'VIEW PROFILE',
                'code'     => 'VIEW-PROFILE',
                'type'     => 'ROLE',
                'group_id' => 9
            ],
            [
                'id'    => 34,
                'name'     => 'CREATE PROFILE',
                'code'     => 'CREATE-PROFILE',
                'type'     => 'ROLE',
                'group_id' => 9
            ],
            [
                'id'    => 35,
                'name'     => 'UPDATE PROFILE',
                'code'     => 'UPDATE-PROFILE',
                'type'     => 'ROLE',
                'group_id' => 9
            ],
            [
                'id'    => 36,
                'name'     => 'DELETE PROFILE',
                'code'     => 'DELETE-PROFILE',
                'type'     => 'ROLE',
                'group_id' => 9
            ],
            [
                'id'       => 37,
                'name'     => 'VIEW ROLE',
                'code'     => 'VIEW-ROLE',
                'type'     => 'ROLE',
                'group_id' => 10
            ],
            [
                'id'    => 38,
                'name'     => 'CREATE ROLE',
                'code'     => 'CREATE-ROLE',
                'type'     => 'ROLE',
                'group_id' => 10
            ],
            [
                'id'    => 39,
                'name'     => 'UPDATE ROLE',
                'code'     => 'UPDATE-ROLE',
                'type'     => 'ROLE',
                'group_id' => 10
            ],
            [
                'id'    => 40,
                'name'     => 'DELETE ROLE',
                'code'     => 'DELETE-ROLE',
                'type'     => 'ROLE',
                'group_id' => 10
            ],
            [
                'id'       => 41,
                'name'     => 'VIEW ROLE PERMISSION',
                'code'     => 'VIEW-ROLE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 11
            ],
            [
                'id'    => 42,
                'name'     => 'CREATE ROLE PERMISSION',
                'code'     => 'CREATE-ROLE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 11
            ],
            [
                'id'    => 43,
                'name'     => 'UPDATE ROLE PERMISSION',
                'code'     => 'UPDATE-ROLE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 11
            ],
            [
                'id'    => 44,
                'name'     => 'DELETE ROLE PERMISSION',
                'code'     => 'DELETE-ROLE-PERMISSION',
                'type'     => 'ROLE',
                'group_id' => 11
            ],
            [
                'id'       => 45,
                'name'     => 'VIEW USER',
                'code'     => 'VIEW-USER',
                'type'     => 'ROLE',
                'group_id' => 12
            ],
            [
                'id'    => 46,
                'name'     => 'CREATE USER',
                'code'     => 'CREATE-USER',
                'type'     => 'ROLE',
                'group_id' => 12
            ],
            [
                'id'    => 47,
                'name'     => 'UPDATE USER',
                'code'     => 'UPDATE-USER',
                'type'     => 'ROLE',
                'group_id' => 12
            ],
            [
                'id'    => 48,
                'name'     => 'DELETE USER',
                'code'     => 'DELETE-USER',
                'type'     => 'ROLE',
                'group_id' => 12
            ],
            [
                'id'       => 49,
                'name'     => 'VIEW',
                'code'     => 'VIEW',
                'type'     => 'DIVISION',
                'group_id' => null
            ],
            [
                'id'    => 50,
                'name'     => 'ADD',
                'code'     => 'ADD',
                'type'     => 'DIVISION',
                'group_id' => null
            ],
            [
                'id'    => 51,
                'name'     => 'DELETE',
                'code'     => 'DELETE',
                'type'     => 'DIVISION',
                'group_id' => null
            ],
        ]);
    }
}
