<?php

use Illuminate\Database\Seeder;

class PermissionGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_groups')->truncate();
        \App\Models\PermissionGroup::insert([
            [
                'id'    => 1,
                'name'  => 'Division',
                'code'  => 'DIVISION',
            ],
            [
                'id'    => 2,
                'name'  => 'Division detail',
                'code'  => 'DIVISION-DETAIL',
            ],
            [
                'id'    => 3,
                'name'  => 'Division permission',
                'code'  => 'DIVISION-PERMISSION',
            ],
            [
                'id'    => 4,
                'name'  => 'File',
                'code'  => 'FILE',
            ],
            [
                'id'    => 5,
                'name'  => 'Folder',
                'code'  => 'FOLDER',
            ],
            [
                'id'    => 6,
                'name'  => 'Media share',
                'code'  => 'MEDIA-SHARE',
            ],
            [
                'id'    => 7,
                'name'  => 'permission',
                'code'  => 'PERMISSION',
            ],
            [
                'id'    => 8,
                'name'  => 'Permission group',
                'code'  => 'PERMISSION-GROUP',
            ],
            [
                'id'    => 9,
                'name'  => 'Profile',
                'code'  => 'PROFILE',
            ],
            [
                'id'    => 10,
                'name'  => 'Role',
                'code'  => 'ROLE',
            ],
            [
                'id'    => 11,
                'name'  => 'Role permission',
                'code'  => 'ROLE-PERMISSION',
            ],
            [
                'id'    => 12,
                'name'  => 'User',
                'code'  => 'USER',
            ],
        ]);
    }
}
