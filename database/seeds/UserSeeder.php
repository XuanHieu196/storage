<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('users')->truncate();

        \App\Models\User::insert([
           [
            'code' => Str::uuid(),
            'username'=>'hieupx',
            'email'=>'hieupx@kdata.vn',
            'password'=> Hash::make('123456'),
            'phone' =>'0393078232',
            'role_id'=>1,
            'is_active'=>1
        ],[
            'code' => Str::uuid(),
            'username'=>'admin',
            'email'=>'admin@gmail.com',
            'password'=> Hash::make('123456'),
            'phone' =>'0393078233',
            'role_id'=>2,
            'is_active'=>1

        ],[
            'code' => Str::uuid(),
            'username'=>'user',
            'email'=>'user@gmail.com',
            'password'=> Hash::make('123456'),
            'phone' =>'0393078233',
            'role_id'=>3,
            'is_active'=>1
        ]
        ]);

    }
}
