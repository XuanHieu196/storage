<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        \App\Models\Role::insert([
            [
                'id'    => 1,
                'name'  => 'Super admin',
                'code'  => 'SUPER ADMIN',
                'level' => 1,
            ],
            [
                'id'    => 2,
                'name'  => 'admin',
                'code'  => 'ADMIN',
                'level' => 2,
            ],
            [
                'id'    => 3,
                'name'  => 'User',
                'code'  => 'USER',
                'level' => 3,
            ],
        ]);
    }
}
