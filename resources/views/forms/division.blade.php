<form action="{{isset($route)?$route:route('divisions.store')}}" method="POST" >
    {{csrf_field()}}
    <input type="hidden" name="_method" value="{{isset($method)?$method:'POST'}}"/>
        <div class="form-group">
        <label for="code">Code</label>
        <input type="text" class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" id="code" value="{{old('code',$model->code)}}" placeholder="" required="required" >
          @if($errors->has('code'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('code') }}</strong>
    </div>
  @endif 
    </div>

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{old('name',$model->name)}}" placeholder="" maxlength="50" required="required" >
          @if($errors->has('name'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('name') }}</strong>
    </div>
  @endif 
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" value="{{old('description',$model->description)}}" placeholder="" maxlength="255" >
          @if($errors->has('description'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('description') }}</strong>
    </div>
  @endif 
    </div>

<div class="form-group">
    <label for="type">Type</label>
    <select class="form-control {{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="type">
        <option value="DEPARTMENT" {{old('type',$model->type)=='DEPARTMENT'?"selected":""}} >DEPARTMENT</option>
<option value="PROJECT" {{old('type',$model->type)=='PROJECT'?"selected":""}} >PROJECT</option>
<option value="TEAM" {{old('type',$model->type)=='TEAM'?"selected":""}} >TEAM</option>

    </select>
      @if($errors->has('type'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('type') }}</strong>
    </div>
  @endif
</div>

<div class="form-check">
    <input class="form-check-input {{ $errors->has('is_active') ? ' is-invalid' : '' }}" type="checkbox" value="1"  name="is_active"
           id="is_active">
    <label class="form-check-label" for="is_active">
        Is Active
    </label>
      @if($errors->has('is_active'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('is_active') }}</strong>
    </div>
  @endif
</div>

    <div class="form-group">
        <label for="created_by">Created By</label>
        <input type="text" class="form-control {{ $errors->has('created_by') ? ' is-invalid' : '' }}" name="created_by" id="created_by" value="{{old('created_by',$model->created_by)}}" placeholder="" maxlength="50" >
          @if($errors->has('created_by'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('created_by') }}</strong>
    </div>
  @endif 
    </div>

    <div class="form-group">
        <label for="updated_by">Updated By</label>
        <input type="text" class="form-control {{ $errors->has('updated_by') ? ' is-invalid' : '' }}" name="updated_by" id="updated_by" value="{{old('updated_by',$model->updated_by)}}" placeholder="" maxlength="50" >
          @if($errors->has('updated_by'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('updated_by') }}</strong>
    </div>
  @endif 
    </div>

    <div class="form-group">
        <label for="deleted_by">Deleted By</label>
        <input type="text" class="form-control {{ $errors->has('deleted_by') ? ' is-invalid' : '' }}" name="deleted_by" id="deleted_by" value="{{old('deleted_by',$model->deleted_by)}}" placeholder="" maxlength="50" >
          @if($errors->has('deleted_by'))
    <div class="invalid-feedback">
        <strong>{{ $errors->first('deleted_by') }}</strong>
    </div>
  @endif 
    </div>


    <div class="form-group text-right ">
        <input type="reset" class="btn btn-default" value="Clear"/>
        <input type="submit" class="btn btn-primary" value="Save"/>

    </div>
</form>