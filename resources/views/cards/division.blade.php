<div class="card card-default">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-9">
                <a href="{{route('divisions.show',$record->id)}}"> {{$record->id}}</a>
            </div>
            <div class="col-sm-3 text-right">
                <div class="btn-group">
                    <a class="btn btn-secondary" href="{{route('divisions.edit',$record->id)}}">
    <span class="fa fa-pencil"></span>
</a>
                    <form onsubmit="return confirm('Are you sure you want to delete?')"
      action="{{route('divisions.destroy',$record->id)}}"
      method="post"
      style="display: inline">
    {{csrf_field()}}
    {{method_field('DELETE')}}
    <button type="submit" class="btn btn-secondary cursor-pointer">
        <i class="text-danger fa fa-remove"></i>
    </button>
</form>
                </div>
            </div>
        </div>
    </div>
    <div class="card-block">
        <table class="table table-bordered table-striped">
            <tbody>
            		<tr>
			<th>Code</th>
			<td>{{$record->code}}</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>{{$record->name}}</td>
		</tr>
		<tr>
			<th>Description</th>
			<td>{{$record->description}}</td>
		</tr>
		<tr>
			<th>Type</th>
			<td>{{$record->type}}</td>
		</tr>
		<tr>
			<th>Is Active</th>
			<td>{{$record->is_active}}</td>
		</tr>
		<tr>
			<th>Created By</th>
			<td>{{$record->created_by}}</td>
		</tr>
		<tr>
			<th>Updated By</th>
			<td>{{$record->updated_by}}</td>
		</tr>
		<tr>
			<th>Deleted By</th>
			<td>{{$record->deleted_by}}</td>
		</tr>

            </tbody>
        </table>
    </div>
</div>
