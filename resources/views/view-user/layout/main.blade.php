<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('view-user.layout.meta')
	@include('view-user.layout.style')
</head>
<body>
	@include('view-user.layout.header')
	@yield('content')
	@include('view-user.layout.footer')
	@include('view-user.layout.script')
</body>
</html>
