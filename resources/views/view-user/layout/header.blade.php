<header>
    <div class="Logo head-block head-block-1"><a href="/"><img src="{{asset('style/images/logo.png')}}" style="height: 22px;" alt="LOGO"></a></div>
    <div class="head-block head-block-2"><a href="{{route('home')}}"><i class="fas fa-folder"></i></a></div>
    <div class="head-block head-block-search">
        <form action="{{route('folders.search')}}" method="get" id="search">
            @php
                if(isset($_GET['name'])){
                    $name = $_GET['name'];
                }else{
                    $name = "";
                }
            @endphp
            <div class="input-group mb-3">
                <div class="input-group-prepend" onclick="$('#search').submit();return false;">
                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Search" name="name" value="{{$name}}">
            </div>
        </form>
    </div>
    <div class="head-block head-block-3 dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"><i class="fas fa-user"></i> {{Auth::user()->username}}</div>
    <div class="dropdown-custom dropdown-menu">
        <a class="dropdown-item" href="{{route('page.setting')}}"><i class="fas fa-cog"></i> Seting</a>
        <a class="dropdown-item" href="#" onclick="$('#post-logout').submit();return false;"><i class="fas fa-sign-out-alt"></i> Logout</a>
    </div>
</header>
<form action="/logout" method="post" id="post-logout">
    @csrf
</form>
