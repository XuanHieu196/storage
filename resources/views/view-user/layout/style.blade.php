<link rel="stylesheet" href="{{asset('style/bootstrap/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('style/fontawesome-web/css/all.css')}}">
<link href="{{asset('style/date/gijgo.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('style/select/select2.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{asset('style/css/mystyle.css')}}">
@yield('style')
