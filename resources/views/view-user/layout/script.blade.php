<script src="{{asset('style/js/jquery.min.js')}}"></script>
<script src="{{asset('style/js/jquery.pjax.js')}}"></script>
<script src="{{asset('style/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('style/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('style/date/gijgo.min.js')}}" type="text/javascript"></script>
<script src="{{asset('style/select/select2.min.js')}}"></script>
<script src="{{asset('style/js/jquery.form.min.js')}}"></script>
<script src="{{asset('style/js/myscript.js')}}"></script>
@yield('script')