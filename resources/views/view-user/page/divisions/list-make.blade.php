@if(!empty($list['data']))
@foreach($list['data'] as $item)
	<tr>
        {{-- <td><input type="checkbox" name="" value="" class="action-check"></td> --}}
        <td class="name">{{$item['name']}}</td>
        <td>{{$item['code']}}</td>
        <td>{{$item['type']}}</td>
        <td class="text-center">
            <div class="dropdown dropdown-custom">
                <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                <div class="dropdown-custom dropdown-menu">
                    <a class="dropdown-item" href="#" onclick="location.href='{{route('divisions.detail_list',['division'=>$item['id']])}}'"><i class="fas fa-info"></i> Detail</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_edit({{$item['id']}})"><i class="fas fa-pen"></i> Edit</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_delete({{$item['id']}},'{{$item['name']}}')"><i class="fas fa-trash-alt"></i> Delete</a>
                </div>
            </div>
        </td>
        <td class="text-center">{{$item['is_active'] == 1 ? 'Active' : 'Not active'}}</td>
    </tr>
@endforeach
@endif