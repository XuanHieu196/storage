<div><strong>Edit user</strong></div>
<input type="hidden" name="user_id" value="{{$model->user_id}}">
<input type="hidden" name="division_id" value="{{$id_division}}">
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="type">
        <option value="LEADER" @if($model->type == 'LEADER') selected="selected" @endif>LEADER</option>
        <option value="EMPLOYEE" @if($model->type == 'EMPLOYEE') selected="selected" @endif>EMPLOYEE</option>
    </select>
</div>
<div class="form-group">
    <label>Description:</label>
    <input type="text" class="form-control" name="description" value="{{$model->description}}">
</div>
<div class="form-group">
    <label>Start time:</label>
    <input type="text" class="form-control" name="start_time" value="{{$model->start_time}}">
</div>
<div class="form-group">
    <label>End time:</label>
    <input type="text" class="form-control" name="end_time" value="{{$model->end_time}}">
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
   <button class="btn btn-create" onclick="function_update({{$model->id}})">Update</button>
</div>
