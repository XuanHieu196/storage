@extends('view-user.layout.main')
@section('content')
	<main>
        <div>
            <div class="box-sidebar">
                @include('view-user.page.setting.general-sidebar')
            </div><!--
         --><div class="box-main">
                <div class="row">
                    <div class="col-6">
                        <div class="block-breadcrumb">
                            <ul class="list-breadcrumb">
                                <li><a href="{{route('home')}}"><i class="fas fa-home"></i> / </a></li>
                                <li><a href="#">Division / </a></li>
                                <li>
                                    <div class="dropdown dropdown-custom">
                                        <i class="fas fa-plus-square dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal-newDivision"><i class="fas fa-folder-plus" onclick="return false;"></i> New Division</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block-show block-show-scroll">
                    <table class="table-list" style="width:100%">
                        <thead>
                            <tr>
                                {{-- <td style="width: 30px"><input type="checkbox" name="" class="check-all"></td> --}}
                                <td style="width: 200px;">Name</td>
                                <td style="width: 200px;">Code</td>
                                <td style="width: 100px;">Type</td>
                                <td style="width: 80px;text-align: center;"></td>
                                <td style="width: 80px;text-align: center;">Active</td>
                            </tr>
                        </thead>
                        <tbody class="html_list">                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <div class="block-modal">
        <!-- The Modal -->
        <!-- data-toggle="modal" data-target="#myModal-newfolder" -->
        <!-- data-dismiss="modal" -->
        <div class="modal fade model-custom" id="myModal-newDivision">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        {{-- HTML Make --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-updateDivision">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        {{-- html make --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-deleteDivision">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to delete division: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id-delete-division" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="delete_division()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('view-user.page.general.waiting')
        @include('view-user.page.general.alert')
    </div>
@endsection
@section('script')
	<script type="text/javascript">
		arr_id_checked = [];
		$(document).on('click','.check-all', function(){
			if($( this ).prop( "checked" )){
				$('.action-check').prop( "checked", true );
				$( ".action-check" ).each(function( index ) {
					arr_id_checked.push($(this).val());
				});
				// console.log(arr_id_checked);
			}else{
				$('.action-check').prop( "checked", false );
				arr_id_checked = [];
				// console.log(arr_id_checked);
			}
		})
		$(document).on('click','.action-check', function(){
			arr_id_checked = [];
			$( ".action-check" ).each(function( index ) {
				if($(this).prop("checked")){
					arr_id_checked.push($(this).val());
				}
			});
			// console.log(arr_id_checked);
		})
	</script>
	<script type="text/javascript">
		function create_division(){
            division_code = $('#myModal-newDivision input[name=division-code]').val();
            division_name = $('#myModal-newDivision input[name=division-name]').val();
            division_description = $('#myModal-newDivision textarea[name=division-description]').val();
            division_type = $('#myModal-newDivision select[name=division-type]').val();
			division_active = 1;
			$.ajax({
				url:'{{route('divisions.store')}}',
				type:'POST',
				data:{
                    code:division_code,
                    name:division_name,
                    description:division_description,
                    type:division_type,
					is_active:1,
					_token:"{{ csrf_token() }}"
				}
			}).done(function(data){
				load_list();
                reload_form_create();
				$('#myModal-newDivision').modal('hide');
				$('#myModal-alert .text-content').html('Create Successfully!');
                $('#myModal-alert').modal('show');
			}).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
		}
        function show_modal_edit(id){
            url = '{{route('divisions.edit_view',['division'=>':id'])}}';
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'GET',
                data:{}
            }).done(function(data){
                $('#myModal-updateDivision .modal-body').html(data);
                $('#myModal-updateDivision').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function update(id){
            division_code = $('#myModal-updateDivision input[name=division-code]').val();
            division_name = $('#myModal-updateDivision input[name=division-name]').val();
            division_description = $('#myModal-updateDivision textarea[name=division-description]').val();
            division_type = $('#myModal-updateDivision select[name=division-type]').val();
            division_active = $('#myModal-updateDivision input[name=division-active]').val();

            url = "{{route('divisions.update',['division'=>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'PUT',
                data:{
                    code:division_code,
                    name:division_name,
                    description:division_description,
                    type:division_type,
                    is_active:1,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-updateDivision').modal('hide');
                $('#myModal-alert .text-content').html('Update Division Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_delete(id, name){
            $('#myModal-deleteDivision .text-content').html(name);
            $('#myModal-deleteDivision input[name=id-delete-division]').val(id);
            $('#myModal-deleteDivision').modal('show');
        }
        function delete_division(){
            id = $('#myModal-deleteDivision input[name=id-delete-division]').val();
            url = "{{route('divisions.destroy',['division'=>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-deleteDivision').modal('hide');
                $('#myModal-alert .text-content').html('Delete Division Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
		load_list();
		function load_list(){
            $('.alert-waiting').show();
			$.ajax({
				url:'{{route('divisions.list_view')}}',
				type:'GET',
				data:{}
			}).done(function(data){
				$('.html_list').html(data);
                $('.alert-waiting').hide();
			}).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('.alert-waiting').hide();
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
		}
        reload_form_create();
        function reload_form_create(){
            $.ajax({
                url:'{{route('divisions.reload_form_create')}}',
                type:'GET',
                data:{}
            }).done(function(data){
                $('#myModal-newDivision .modal-body').html(data);
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
	</script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.active-division').addClass('active');
        })
    </script>
@endsection