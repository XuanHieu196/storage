<div><strong>Update Division</strong></div>
<div class="form-group">
    <label>Code:</label>
    <input type="text" class="form-control" name="division-code" value="{{$model->code}}">
</div>
<div class="form-group">
    <label>Name:</label>
    <input type="text" class="form-control" name="division-name" value="{{$model->name}}">
</div>
<div class="form-group">
    <label>Description:</label>
    <textarea class="form-control" name="division-description">{{$model->description}}</textarea>
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="division-type">
        <option value="DEPARTMENT" {{ ($model->type == "DEPARTMENT") ? "selected='selected" : ''}}>DEPARTMENT</option>
        <option value="PROJECT" {{ ($model->type == "PROJECT") ? "selected='selected" : ''}}>PROJECT</option>
        <option value="TEAM" {{ ($model->type == "TEAM") ? "selected='selected" : ''}}>TEAM</option>
    </select>
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    <button class="btn btn-create" onclick="update({{$model->id}})">Update</button>
</div>