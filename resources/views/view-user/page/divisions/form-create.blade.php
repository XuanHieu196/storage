<div><strong>New Division</strong></div>
<div class="form-group">
    <label>Code:</label>
    <input type="text" class="form-control" name="division-code" value="">
</div>
<div class="form-group">
    <label>Name:</label>
    <input type="text" class="form-control" name="division-name" value="">
</div>
<div class="form-group">
    <label>Description:</label>
    <textarea class="form-control" name="division-description"></textarea>
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="division-type">
        <option value="DEPARTMENT">DEPARTMENT</option>
        <option value="PROJECT">PROJECT</option>
        <option value="TEAM">TEAM</option>
    </select>
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    <button class="btn btn-create" onclick="create_division()">Create</button>
</div>