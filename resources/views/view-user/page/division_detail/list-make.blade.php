@if(!empty($list))
@foreach($list as $item)
    @php
        $user = \App\Models\User::where('id', $item['user_id'])->first();
        if(!empty($user)){
            $user_name = $user->username;
        }else{
            $user_name = NULL;
        }
        $division = \App\Models\Division::where('id', $item['division_id'])->first();
        if(!empty($division)){
            $division_name = $division->name;
        }else{
            $division_name = NULL;
        }
    @endphp
	<tr>
        <td class="name">{{$user_name}}</td>
        <td class="">{{$division_name}}</td>
        <td class="">{{$item['type']}}</td>
        <td class="">{{$item['description']}}</td>
        <td class="">{{date('Y-m-d', strtotime($item['start_time']))}}</td>
        <td class="">{{date('Y-m-d', strtotime($item['end_time']))}}</td>
        <td class="text-center">
            <div class="dropdown dropdown-custom">
                <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                <div class="dropdown-custom dropdown-menu">
                    <a class="dropdown-item" href="#"><i class="fas fa-info"></i> Detail</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_edit({{$item['id']}})"><i class="fas fa-pen"></i> Edit</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_delete({{$item['id']}})"><i class="fas fa-trash-alt"></i> Delete</a>
                </div>
            </div>
        </td>
        <td class="text-center">{{$item['is_active'] == 1 ? 'Active' : 'Not active'}}</td>
    </tr>
@endforeach
@endif