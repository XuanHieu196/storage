<div><strong>New Division Detail</strong></div>
<div class="form-group">
    <label>Email user:</label>
    <select class="js-example-basic-multiple form-control list_user_id html_list_user__option" name="user_id[]" multiple="multiple" style="width: 100%;">
        @if(!empty($list_user))
        @foreach($list_user as $item)
        <option value="{{$item['id']}}">{{$item['email']}}</option>
        @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label>Division:</label>
    <select class="form-control list_division_id html_list_division__option" name="division_id">
        @if(!empty($list_division))
        @foreach($list_division as $item)
        <option value="{{$item['id']}}">{{$item['name']}}</option>
        @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="type">
        <option value="LEADER">LEADER</option>
        <option value="EMPLOYEE">EMPLOYEE</option>
    </select>
</div>
<div class="form-group">
    <label>Description:</label>
    <input type="text" class="form-control" name="description" value="">
</div>
<div class="form-group">
    <label>Start time:</label>
    <input type="text" class="form-control" name="start_time" value="">
</div>
<div class="form-group">
    <label>End time:</label>
    <input type="text" class="form-control" name="end_time" value="">
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    <button class="btn btn-create" onclick="function_create()">Create</button>
</div>