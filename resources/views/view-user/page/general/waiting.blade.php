<div class="alert-waiting" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Loading...</div>
</div>
<div class="alert-sharing" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Sharing...</div>
</div>
<div class="alert-unsharing" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Stop sharing...</div>
</div>
<div class="alert-copying" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Copying...</div>
</div>
<div class="alert-downloading" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Downloading...</div>
</div>
<div class="alert-moving" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Moving...</div>
</div>