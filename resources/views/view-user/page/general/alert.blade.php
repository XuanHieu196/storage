{{-- Modal alert success --}}
<div class="modal fade model-custom" id="myModal-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <div class="text-center text-success text-content"></div>
                </div>
                <div class="text-right">
                    <button class="btn btn-create" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>