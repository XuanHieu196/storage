@extends('view-user.layout.main')
@section('content')
    @php
        $view_load = $view_load;
        switch ($view_load) {
            case 'shared-with-you':
                $params = 'to_shared';
                $title = 'shared with you';
                break;
            case 'shared-with-others':
                $params = 'share';
                $title = 'Shared with others';
                break;
            case 'shared-by-link':
                $params = 'share_link';
                $title = 'Shared by link';
                break;
            default:
                $params = '';
                $title = '';
                break;
        }
    @endphp
	<main>
        <div>
            <div class="box-sidebar only-pc">
                @include('view-user.page.list.sidebar')
            </div><!--
         --><div class="box-main">
                <div class="row">
                    <div class="col-6">
                        <div class="block-breadcrumb">
                            <ul class="list-breadcrumb">
                                <li><a href="{{route('home')}}" class="pjax"><i class="fas fa-home"></i> / </a></li>
                                <li><a href="#">{{$title}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block-show block-show-scroll">
                    <table class="table-list" style="width:100%">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td style="width: 100px;text-align: center;">Owner</td>
                                <td style="width: 80px;text-align: center;">Action</td>
                                <td style="width: 100px;text-align: center;">Size</td>
                                <td style="width: 150px;text-align: center;">Last Modified</td>
                            </tr>
                        </thead>
                        <tbody class="html_list">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-sidebar only-mobile" style="width: 100%;">
                <ul class="list-show">
                    <li><a href="{{route('home')}}" class="pjax"><i class="fas fa-folder"></i> All File</a></li>
                    {{-- <li><a href="#"><i class="fas fa-share-alt"></i> Shared with you</a></li>
                    <li><a href="#"><i class="fas fa-share-alt"></i> Shared with others</a></li>
                    <li><a href="#"><i class="fas fa-link"></i> Shared by link</a></li> --}}
                </ul>
                <hr>
                <ul class="list-show foot">
                    {{-- <li><a href="#"><i class="fas fa-trash-alt"></i> Delete Files</a></li> --}}
                    <li><a href="#"><i class="fas fa-chart-pie"></i> Storage (40%)</a></li>
                    <li style="padding-left: 20px;padding-bottom: 0;">
                        <div style="width: 100%;height: 5px;background-color: #f1f1f1;">
                            <div style="width: 40%;height: 5px;background-color: #797979;"></div>
                        </div>
                    </li>
                     <li style="padding-left: 20px;">
                        <span style="color: #6e6e6e;font-size: 13px;">20 MB of 50 GB used</span>
                     </li>
                    <li><a href="{{route('page.setting')}}"><i class="fas fa-cog"></i> Setting</a></li>
                </ul>
            </div>
        </div>
    </main>
    <div class="block-modal">
        <!-- The Modal -->
        <!-- data-toggle="modal" data-target="#myModal-newfolder" -->
        <!-- data-dismiss="modal" -->
    </div>
    @include('view-user.page.list.general-uploading')
    @include('view-user.page.general.waiting')
    @include('view-user.page.general.alert')
@endsection
@section('script')
    <script src="{{asset('style/js/fileDownload.js')}}"></script>
	<script type="text/javascript">
        function action_download(id, type){
            // alert(id + ' ' + type);
            if(type == "folder"){
                url = "{{route('folders.download',['id'=>':id'])}}";
                url = url.replace(':id', id);
                $.fileDownload(url)
                .done(function () { 
                })
                .fail(function () {
                    $('#myModal-alert .text-content').html('<span class="text-danger">Download Fail!</span>');
                    $('#myModal-alert').modal('show');
                });
            }else{
                url = "{{route('files.download',['id'=>':id'])}}";
                url = url.replace(':id', id);
                $.fileDownload(url)
                .done(function () { 
                })
                .fail(function () {
                    $('#myModal-alert .text-content').html('<span class="text-danger">Download Fail!</span>');
                    $('#myModal-alert').modal('show');
                });
            }
        }
	</script>
    {{-- Load Folder/File --}}
    <script type="text/javascript">
        load_list();
        function load_list(){
            $('.alert-waiting').show();
            $.ajax({
                url:'{{route('folders.load_view_share')}}',
                type:'GET',
                data:{
                	key:'{{$params}}'
                }
            }).done(function(data){
            	$('.alert-waiting').hide();
                $('.html_list').html(data);
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.alert-waiting').hide();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            @if($params == 'to_shared')
            $('.list-show .to_shared').addClass('active');
            @endif
            @if($params == 'share')
            $('.list-show .share').addClass('active');
            @endif
            @if($params == 'share_link')
            $('.list-show .share_link').addClass('active');
            @endif
        })
    </script>
@endsection
