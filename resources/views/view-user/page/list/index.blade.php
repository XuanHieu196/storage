@extends('view-user.layout.main')
@section('content')
	<main>
        <div>
            <div class="box-sidebar only-pc">
                @include('view-user.page.list.sidebar')
            </div><!--
         --><div class="box-main">
            @include('acheckin-upload.includes.alerts')
                <div class="row">
                    <div class="col-6">
                        <div class="block-breadcrumb">
                            <ul class="list-breadcrumb">
                                <li><a href="{{route('home')}}" class="pjax"><i class="fas fa-home"></i> / </a></li>
                                <li>
                                    <div class="dropdown dropdown-custom">
                                        <i class="fas fa-plus-square dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal-newfolder"><i class="fas fa-folder-plus" onclick="return false;"></i> New Folder</a>
                                            <a class="dropdown-item" href="#" onclick="$('.btn-upload-file').trigger('click')"><i class="fas fa-file-upload"></i> Upload File</a>
                                            <a class="dropdown-item" href="#"><i class="fas fa-upload"></i> Upload Folder</a>
                                        </div>
                                    </div>
                                    <form action="{{route('files.store')}}" method="post" id="form-upload-file" enctype="multipart/form-data" style="display: none;">
                                        @csrf
                                        <input type="file" name="file" onchange="$('.btn-form-upload-file').trigger('click');" class="btn-upload-file">
                                        <input type="submit" name="" class="btn-form-upload-file">
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {{-- <div class="col-6 text-right">
                        <div class="icon-head-right">
                            <div>
                                <div class="dropdown dropdown-custom">
                                    <i class="fas fa-user-plus dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown" style="cursor: pointer;"></i>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fas fa-info"></i> Detail</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-pen"></i> Rename</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-copy"></i> Move or Copy</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-download"></i> Download</a>
                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal-share" onclick="return false;"><i class="fas fa-share-alt"></i> Shared</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-share-alt"></i> Unshared</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-trash-alt"></i> Delete</a>
                                    </div>
                                    <i class="fas fa-grip-horizontal dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown" style="cursor: pointer;margin-left: 5px;"></i>
                                    <div class="dropdown-menu">
                                        <!-- <a class="dropdown-item" href="#"><i class="fas fa-info"></i> Detail</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-pen"></i> Rename</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-copy"></i> Move or Copy</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-download"></i> Download</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-trash-alt"></i> Delete</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="block-show block-show-scroll">
                    <table class="table-list" style="width:100%">
                        <thead>
                            <tr>
                                {{-- <td style="width: 30px"><input type="checkbox" name="" class="check-all"></td> --}}
                                <td>Name</td>
                                <td style="width: 100px;text-align: center;">Owner</td>
                                <td style="width: 80px;text-align: center;">Share</td>
                                <td style="width: 80px;text-align: center;">Action</td>
                                <td style="width: 100px;text-align: center;">Size</td>
                                <td style="width: 150px;text-align: center;">Last Modified</td>
                            </tr>
                        </thead>
                        <tbody class="html_list_folder">
                        </tbody>
                        <tbody class="html_list_file">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-sidebar only-mobile" style="width: 100%;">
                <ul class="list-show">
                    <li><a href="{{route('home')}}" class="pjax"><i class="fas fa-folder"></i> All File</a></li>
                    {{-- <li><a href="#"><i class="fas fa-share-alt"></i> Shared with you</a></li>
                    <li><a href="#"><i class="fas fa-share-alt"></i> Shared with others</a></li>
                    <li><a href="#"><i class="fas fa-link"></i> Shared by link</a></li> --}}
                </ul>
                <hr>
                <ul class="list-show foot">
                    {{-- <li><a href="#"><i class="fas fa-trash-alt"></i> Delete Files</a></li> --}}
                    <li><a href="#"><i class="fas fa-chart-pie"></i> Storage (40%)</a></li>
                    <li style="padding-left: 20px;padding-bottom: 0;">
                        <div style="width: 100%;height: 5px;background-color: #f1f1f1;">
                            <div style="width: 40%;height: 5px;background-color: #797979;"></div>
                        </div>
                    </li>
                     <li style="padding-left: 20px;">
                        <span style="color: #6e6e6e;font-size: 13px;">20 MB of 50 GB used</span>
                     </li>
                    <li><a href="{{route('page.setting')}}"><i class="fas fa-cog"></i> Setting</a></li>
                </ul>
            </div>
        </div>
    </main>
    <div class="block-modal">
        <!-- The Modal -->
        <!-- data-toggle="modal" data-target="#myModal-newfolder" -->
        <!-- data-dismiss="modal" -->
        <div class="modal fade model-custom" id="myModal-newfolder">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>New Folder:</label>
                            <input type="text" class="form-control" name="new-folder" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-create" onclick="create_folder()">Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-updatefolder">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Rename Folder:</label>
                            <input type="text" class="form-control" name="update-folder" value="">
                            <input type="hidden" name="id-update-folder" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-create" onclick="update_folder()">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-deletefolder">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to delete folder: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id-delete-folder" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="delete_folder()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-updateFile">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Rename File:</label>
                            <input type="text" class="form-control" name="update-file" value="">
                            <input type="hidden" name="id-update-file" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-create" onclick="update_file()">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-deletefile">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to delete file: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id-delete-file" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="delete_file()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-alert">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="text-center text-success text-content"></div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-create" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-share-link">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <label>Link: <span class="status-copy text-success"></span></label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control input-container-link" value="" name="">
                            <div class="input-group-append" onclick="copyToClipboard('.input-container-link')">
                                <span class="input-group-text pointer action-copy-link">Copy</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Permission:</label>
                            <select class="form-control" name="permission" style="width: 100%;">
                                <option value="VIEW">VIEW</option>
                                <option value="COMMENT">COMMENT</option>
                                <option value="UPDATE">UPDATE</option>
                            </select>
                        </div>
                        <input type="hidden" name="type">
                        <input type="hidden" name="id-link-share">
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-create" onclick="share_link()">Share</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-unshare-link">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to unshare link: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id" value="">
                            <input type="hidden" name="type" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="unshare_link()">Unshare</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-edit-share">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body tab-content-edit">
                        {{-- view-user.page.folder.form-edit-folder --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-unshare">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to unshare: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id-unshare" value="">
                            <input type="hidden" name="type-unshare" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="unshare()">Unshare</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-copy">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Copy To Folder</label>
                            <select class="js-example-basic-multiple form-control input-copy-to" style="width: 100%;">
                            </select>
                        </div>
                        <input type="hidden" name="id-copy-from">
                        <input type="hidden" name="id-copy-to">
                        <input type="hidden" name="type-copy">
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-create" onclick="copy()">Copy</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-move">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Move To Folder</label>
                            <select class="js-example-basic-multiple form-control input-move-to" style="width: 100%;">
                            </select>
                        </div>
                        <input type="hidden" name="id-move-from">
                        <input type="hidden" name="id-move-to">
                        <input type="hidden" name="type-move">
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-create" onclick="move()">Move</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('view-user.page.list.general-uploading')
    @include('view-user.page.general.waiting')
    @include('view-user.page.general.alert')
@endsection
@section('script')
    <script src="{{asset('style/js/fileDownload.js')}}"></script>
	<script type="text/javascript">
		arr_id_checked = [];
		$(document).on('click','.check-all', function(){
			if($( this ).prop( "checked" )){
				$('.action-check').prop( "checked", true );
				$( ".action-check" ).each(function( index ) {
					arr_id_checked.push($(this).val());
				});
				// console.log(arr_id_checked);
			}else{
				$('.action-check').prop( "checked", false );
				arr_id_checked = [];
				// console.log(arr_id_checked);
			}
		})
		$(document).on('click','.action-check', function(){
			arr_id_checked = [];
			$( ".action-check" ).each(function( index ) {
				if($(this).prop("checked")){
					arr_id_checked.push($(this).val());
				}
			});
			// console.log(arr_id_checked);
		})
	</script>
	<script type="text/javascript">
		function create_folder(){
			name_folder = $('input[name=new-folder]').val();
			$.ajax({
				url:'{{route('folders.store')}}',
				type:'POST',
				data:{
					name:name_folder,
					path:name_folder,
					_token:"{{ csrf_token() }}"
				}
			}).done(function(data){
				load_list();
				$('input[name=new-folder]').val('');
				$('#myModal-newfolder').modal('hide');
				$('#myModal-alert .text-content').html('Create Successfully!');
                $('#myModal-alert').modal('show');
			}).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
		}
        function show_modal_edit_folder(id, name){
            $('input[name=update-folder]').val(name);
            $('input[name=id-update-folder]').val(id);
            $('#myModal-updatefolder').modal('show');
        }
        function update_folder(){
            name_folder = $('input[name=update-folder]').val();
            id_folder = $('input[name=id-update-folder]').val();
            url = "{{route('folders.update',['folder'=>':id'])}}";
            url = url.replace(':id', id_folder);
            $.ajax({
                url:url,
                type:'PUT',
                data:{
                    name:name_folder,
                    path:name_folder,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-updatefolder').modal('hide');
                $('#myModal-alert .text-content').html('Update Name Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_edit_file(id, name){
            $('input[name=update-file]').val(name);
            $('input[name=id-update-file]').val(id);
            $('#myModal-updateFile').modal('show');
        }
        function update_file(){
            name_file = $('input[name=update-file]').val();
            id_file = $('input[name=id-update-file]').val();
            url = "{{route('files.update',['id'=>':id'])}}";
            url = url.replace(':id', id_file);
            $.ajax({
                url:url,
                type:'PUT',
                data:{
                    name:name_file,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('input[name=new-file]').val('');
                $('#myModal-updateFile').modal('hide');
                $('#myModal-alert .text-content').html('Update File Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_delete_folder(id, name){
            $('#myModal-deletefolder .text-content').html(name);
            $('input[name=id-delete-folder]').val(id);
            $('#myModal-deletefolder').modal('show');
        }
        function delete_folder(){
            id_folder = $('input[name=id-delete-folder]').val();
            url = "{{route('folders.destroy',['folder'=>':id'])}}";
            url = url.replace(':id', id_folder);
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-deletefolder').modal('hide');
                $('#myModal-alert .text-content').html('Delete Folder Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_delete_file(id, name){
            $('#myModal-deletefile .text-content').html(name);
            $('input[name=id-delete-file]').val(id);
            $('#myModal-deletefile').modal('show');
        }
        function delete_file(){
            id_file = $('input[name=id-delete-file]').val();
            url = "{{route('files.destroy',['id'=>':id'])}}";
            url = url.replace(':id', id_file);
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-deletefile').modal('hide');
                $('#myModal-alert .text-content').html('Delete File Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        // Share Link
        function show_modal_share_link(id,type,link){
            $('.status-copy').html('');
            $('.input-container-link').val(link);
            $('#myModal-share-link input[name=id-link-share]').val(id);
            if(type == 'folder'){
                $('#myModal-share-link input[name=type]').val('folder');
            }else{
                $('#myModal-share-link input[name=type]').val('file');
            }
            $('#myModal-share-link').modal('show');
        }
        function share_link(){
            id = $('#myModal-share-link input[name=id-link-share]').val();
            type = $('#myModal-share-link input[name=type]').val();
            permission = $('#myModal-share-link select[name=permission]').val();
            if(type == 'folder'){
                url = '{{route('folders.share',['id'=>':id'])}}';
            }else{
                url = '{{route('files.share',['id'=>':id'])}}';
            }
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'PUT',
                data:{
                    permission: permission,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-share-link').modal('hide');
                $('#myModal-alert .text-content').html('Share Link Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        // Unshare Link
        function show_modal_unshare_link(id,type,name){
            $('#myModal-unshare-link input[name=id]').val(id);
            $('#myModal-unshare-link .text-content').html(name);
            if(type == 'folder'){
                $('#myModal-unshare-link input[name=type]').val('folder');
            }else{
                $('#myModal-unshare-link input[name=type]').val('file');
            }
            $('#myModal-unshare-link').modal('show');
        }
        function unshare_link(){
            id = $('#myModal-unshare-link input[name=id]').val();
            type = $('#myModal-unshare-link input[name=type]').val();
            permission = $('#myModal-share-link select[name=permission]').val();
            if(type == 'folder'){
                url = '{{route('folders.un_share',['id'=>':id'])}}';
            }else{
                url = '{{route('files.un_share',['id'=>':id'])}}';
            }
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'PUT',
                data:{
                    permission: permission,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-unshare-link').modal('hide');
                $('#myModal-alert .text-content').html('Unshare Link Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        // Unshare
        function show_modal_unshare(id, name, type){
            $('#myModal-unshare-link .text-content').html(name);
            if(type == 'folder'){
                $('#myModal-share-link input[name=type]').val('folder');
            }else{
                $('#myModal-share-link input[name=type]').val('file');
            }
            $('#myModal-share-link').modal('show');
        }
        var input_user = $('.input-user').html();
        var input_division = $('.input-division').html();
        function share(){
            $('#myModal-share').modal('hide');
            $('.alert-sharing').show();
            $('#form-share').ajaxForm({
                complete: function(xhr) {
                    $('#myModal-alert .text-content').html('Share Successfully!');
                    $('.alert-sharing').hide();
                    $('#myModal-alert').modal('show');
                    $('.input-user').html(input_user);
                    $('.input-division').html(input_division);
                    load_list();
                }
            });
        }
        function show_modal_unshare(id, name, type){
            $('#myModal-unshare .text-content').html(name);
            $('input[name=id-unshare]').val(id);
            $('input[name=type-unshare]').val(type);
            $('#myModal-unshare').modal('show');
        }
        function unshare(){
            $('#myModal-unshare').modal('hide');
            $('.alert-unsharing').show();
            id_item = $('input[name=id-unshare]').val();
            type_item = $('input[name=type-unshare]').val();
            if(type_item == 'folder'){
                url = "{{route('folders.unshare',['id'=>':id'])}}";
            }else if(type_item == 'file'){
                url = "{{route('files.unshare',['id'=>':id'])}}";
            }
            url = url.replace(':id', id_item);
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('.alert-unsharing').hide();
                $('#myModal-unshare').modal('hide');
                $('#myModal-alert .text-content').html('Unshare Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_edit_share(id, type){
            if(type == 'folder'){
                $('.tab-content-edit').html('<div class="text-center text-green">Waitting...</div>');
                url = "{{route('folders.share_permission',['id'=>':id'])}}";
                url = url.replace(':id', id);
                $.ajax({
                    url:url,
                    type:'PUT',
                    data:{
                        _token:"{{ csrf_token() }}"
                    }
                }).done(function(data){
                    $('.tab-content-edit').html(data);
                    $('#myModal-edit-share input[name=file_id__edit_share]').val('');
                    $('#myModal-edit-share input[name=folder_id__edit_share]').val(id);
                    select2();
                    $('input[name=id-edit-share]').val(id);
                    $('#myModal-edit-share').modal('show');
                }).fail(function(data) {
                    mes_error = data.responseJSON.message;
                    load_list();
                    $('.modal').modal('hide');
                    $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                    $('#myModal-alert').modal('show');
                })
            }else{
                $('.tab-content-edit').html('<div class="text-center text-green">Waitting...</div>');
                url = "{{route('files.share_permission',['id'=>':id'])}}";
                url = url.replace(':id', id);
                $.ajax({
                    url:url,
                    type:'PUT',
                    data:{
                        _token:"{{ csrf_token() }}"
                    }
                }).done(function(data){
                    $('.tab-content-edit').html(data);
                    $('#myModal-edit-share input[name=file_id__edit_share]').val(id);
                    $('#myModal-edit-share input[name=folder_id__edit_share]').val('');
                    select2();
                    $('input[name=id-edit-share]').val(id);
                    $('#myModal-edit-share').modal('show');
                }).fail(function(data) {
                    mes_error = data.responseJSON.message;
                    load_list();
                    $('.modal').modal('hide');
                    $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                    $('#myModal-alert').modal('show');
                }) 
            }
        }
        function update_share(){
            // VIEW
            per_view_user = $('.edit-share-tab--per-view .input-user').val();
            per_view_division = $('.edit-share-tab--per-view .input-division').val();
            per_view = $('.edit-share-tab--per-view .type-permission').val();

            // COMMENT
            per_comment_user = $('.edit-share-tab--per-comment .input-user').val();
            per_comment_division = $('.edit-share-tab--per-comment .input-division').val();
            per_comment = $('.edit-share-tab--per-comment .type-permission').val();

            // UPDATE
            per_update_user = $('.edit-share-tab--per-update .input-user').val();
            per_update_division = $('.edit-share-tab--per-update .input-division').val();
            per_update = $('.edit-share-tab--per-update .type-permission').val();

            id = $('input[name=id-edit-share]').val();
            id_file = $('input[name=file_id__edit_share]').val();
            id_folder = $('input[name=folder_id__edit_share]').val();
            url = "{{route('media_shares.sync')}}";
            i = 0;
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    users:per_view_user,
                    divisions:per_view_division,
                    permission:per_view,
                    file_id:id_file,
                    folder_id:id_folder,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                i++;
                if(i == 3){
                    load_list();
                    $('#myModal-edit-share').modal('hide');
                    $('#myModal-alert .text-content').html('Update Share Successfully!');
                    $('#myModal-alert').modal('show');
                }
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    users:per_comment_user,
                    divisions:per_comment_division,
                    permission:per_comment,
                    file_id:id_file,
                    folder_id:id_folder,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                i++;
                if(i == 3){
                    load_list();
                    $('#myModal-edit-share').modal('hide');
                    $('#myModal-alert .text-content').html('Update Share Successfully!');
                    $('#myModal-alert').modal('show');
                }
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    users:per_update_user,
                    divisions:per_update_division,
                    permission:per_update,
                    file_id:id_file,
                    folder_id:id_folder,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                i++;
                if(i == 3){
                    load_list();
                    $('#myModal-edit-share').modal('hide');
                    $('#myModal-alert .text-content').html('Update Share Successfully!');
                    $('#myModal-alert').modal('show');
                }
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                load_list();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_copy(id, type){
            // Reset select
            list_tree();
            $('#myModal-copy input[name=id-copy-from]').val(id);
            $('#myModal-copy input[name=id-copy-to]').val('');
            $('#myModal-copy input[name=type-copy]').val(type);
            $('#myModal-copy').modal('show');
        }
        function copy(){
            type_copy = $('input[name=type-copy]').val();
            $('#myModal-copy').modal('hide');
            if(type_copy == "folder"){
                $('.alert-copying').show();
                id_from = $('#myModal-copy input[name=id-copy-from]').val();
                id_to = $('#myModal-copy input[name=id-copy-to]').val();
                url = "{{route('folders.copy',['folder'=>':id'])}}";
                url = url.replace(':id', id_from);
                if(id_to == 'null'){
                    params = {_token:"{{ csrf_token() }}"};
                }else{
                    params = {folderToId:id_to,_token:"{{ csrf_token() }}"}
                }
                $.ajax({
                    url:url,
                    type:'PUT',
                    data:params
                }).done(function(data){
                    list_tree();
                    $('.alert-copying').hide();
                    $('#myModal-alert .text-content').html('Copy Successfully!');
                    $('#myModal-alert').modal('show');
                }).fail(function(data) {
                    mes_error = data.responseJSON.message;
                    list_tree();
                    $('.alert-copying').hide();
                    $('.modal').modal('hide');
                    $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                    $('#myModal-alert').modal('show');
                })
            }else{
                $('.alert-copying').show();
                id_from = $('#myModal-copy input[name=id-copy-from]').val();
                id_to = $('#myModal-copy input[name=id-copy-to]').val();
                url = "{{route('files.copy',['id'=>':id'])}}";
                url = url.replace(':id', id_from);
                if(id_to == 'null'){
                    params = {_token:"{{ csrf_token() }}"};
                }else{
                    params = {folderToId:id_to,_token:"{{ csrf_token() }}"}
                }
                $.ajax({
                    url:url,
                    type:'PUT',
                    data:params
                }).done(function(data){
                    list_tree();
                    $('.alert-copying').hide();
                    $('#myModal-alert .text-content').html('Copy Successfully!');
                    $('#myModal-alert').modal('show');
                }).fail(function() {
                    list_tree();
                    $('.alert-copying').hide();
                    $('#myModal-alert .text-content').html('<span class="text-danger">Copy Fail!</span>');
                    $('#myModal-alert').modal('show');
                }).fail(function(data) {
                    mes_error = data.responseJSON.message;
                    list_tree();
                    $('.alert-copying').hide();
                    $('.modal').modal('hide');
                    $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                    $('#myModal-alert').modal('show');
                })
            }
        }
        $(document).on('change','.input-copy-to',function(){
            get_id_copy__to = $(this).val();
            $('#myModal-copy input[name=id-copy-to]').val(get_id_copy__to);
        }) 
        function show_modal_move(id, type){
            // Reset select
            list_tree();
            $('#myModal-move input[name=id-move-from]').val(id);
            $('#myModal-move input[name=id-move-to]').val('');
            $('#myModal-move input[name=type-move]').val(type);
            $('#myModal-move').modal('show');
        }
        function move(){
            type_move = $('input[name=type-move]').val();
            $('#myModal-move').modal('hide');
            if(type_move == "folder"){
                $('.alert-moving').show();
                id_from = $('#myModal-move input[name=id-move-from]').val();
                id_to = $('#myModal-move input[name=id-move-to]').val();
                url = "{{route('folders.move',['folder'=>':id'])}}";
                url = url.replace(':id', id_from);
                if(id_to == 'null'){
                    params = {_token:"{{ csrf_token() }}"};
                }else{
                    params = {folderToId:id_to,_token:"{{ csrf_token() }}"}
                }
                $.ajax({
                    url:url,
                    type:'PUT',
                    data:params
                }).done(function(data){
                    list_tree();
                    load_list();
                    $('.alert-moving').hide();
                    $('#myModal-alert .text-content').html('Move Successfully!');
                    $('#myModal-alert').modal('show');
                }).fail(function(data) {
                    mes_error = data.responseJSON.message;
                    list_tree();
                    load_list();
                    $('.alert-moving').hide();
                    $('.modal').modal('hide');
                    $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                    $('#myModal-alert').modal('show');
                })
            }else{
                $('.alert-moving').show();
                id_from = $('#myModal-move input[name=id-move-from]').val();
                id_to = $('#myModal-move input[name=id-move-to]').val();
                url = "{{route('files.move',['id'=>':id'])}}";
                url = url.replace(':id', id_from);
                if(id_to == 'null'){
                    params = {_token:"{{ csrf_token() }}"};
                }else{
                    params = {folderToId:id_to,_token:"{{ csrf_token() }}"}
                }
                $.ajax({
                    url:url,
                    type:'PUT',
                    data:params
                }).done(function(data){
                    list_tree();
                    load_list();
                    $('.alert-moving').hide();
                    $('#myModal-alert .text-content').html('Move Successfully!');
                    $('#myModal-alert').modal('show');
                }).fail(function(data) {
                    mes_error = data.responseJSON.message;
                    list_tree();
                    load_list();
                    $('.alert-moving').hide();
                    $('.modal').modal('hide');
                    $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                    $('#myModal-alert').modal('show');
                })
            }
        }
        $(document).on('change','.input-move-to',function(){
            get_id_copy__to = $(this).val();
            $('#myModal-move input[name=id-move-to]').val(get_id_copy__to);
        }) 
        function action_download(id, type){
            // alert(id + ' ' + type);
            if(type == "folder"){
                url = "{{route('folders.download',['id'=>':id'])}}";
                url = url.replace(':id', id);
                $.fileDownload(url)
                .done(function () { 
                })
                .fail(function () {
                    $('#myModal-alert .text-content').html('<span class="text-danger">Download Fail!</span>');
                    $('#myModal-alert').modal('show');
                });
            }else{
                url = "{{route('files.download',['id'=>':id'])}}";
                url = url.replace(':id', id);
                $.fileDownload(url)
                .done(function () { 
                })
                .fail(function () {
                    $('#myModal-alert .text-content').html('<span class="text-danger">Download Fail!</span>');
                    $('#myModal-alert').modal('show');
                });
            }
        }
        list_tree();
        function list_tree(){
            url = "{{route('folders.list_tree')}}";
            $.ajax({
                url:url,
                type:'GET',
                data:{
                }
            }).done(function(data){
                $('.input-copy-to').html(data);
                $('.input-move-to').html(data);
                select2();
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
	</script>
    {{-- Load Folder/File --}}
    <script type="text/javascript">
        load_list();
        function load_list(){
            load_list_folder();
        }
        function load_list_folder(){
            $('.alert-waiting').show();
            $.ajax({
                url:'{{route('folders.list_view')}}',
                type:'GET',
                data:{}
            }).done(function(data){
                $('.html_list_folder').html(data);
                load_list_file();
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.alert-waiting').hide();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function load_list_file(){
            $('.alert-waiting').show();
            $.ajax({
                url:'{{route('files.list_view')}}',
                type:'GET',
                data:{}
            }).done(function(data){
                $('.html_list_file').html(data);
                $('.alert-waiting').hide();
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.alert-waiting').hide();
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
    </script>
    {{-- Submit Form --}}
    <script type="text/javascript">
        $('#form-upload-file').ajaxForm({
            uploadProgress: function() {
                $('.alert-uploading').show();
            },
            complete: function(xhr) {
                $('.alert-uploading').hide();
                $('#form-upload-file')[0].reset();
                load_list();
            }
        });
    </script>
    {{-- Select2 --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
        function select2(){
            $('.js-example-basic-multiple').select2();
        }
    </script>
    {{-- Copy --}}
    <script type="text/javascript">
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).val()).select();
            document.execCommand("copy");
            $temp.remove();
            $('.status-copy').html('<small>Copied</small>');
        }
    </script>
@endsection
