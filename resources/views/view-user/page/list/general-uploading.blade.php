<div class="alert-uploading" style="
    position: fixed;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.62);
    text-align: center;
    padding-top: 50px;
    display: none;
">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <br>
    <div style="color: #FFF">Uploading...</div>
</div>