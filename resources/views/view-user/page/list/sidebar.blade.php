<ul class="list-show">
    <li><a href="{{route('home')}}" class="pjax"><i class="fas fa-folder"></i> All File</a></li>
    <li><a href="{{route('folders.shared-view_load',['view_load'=>'shared-with-you'])}}" class="to_shared"><i class="fas fa-share-alt"></i> Shared with you</a></li>
    <li><a href="{{route('folders.shared-view_load',['view_load'=>'shared-with-others'])}}" class="share"><i class="fas fa-share-alt"></i> Shared with others</a></li>
    <li><a href="{{route('folders.shared-view_load',['view_load'=>'shared-by-link'])}}" class="share_link"><i class="fas fa-link"></i> Shared by link</a></li>
</ul>
<hr>
<ul class="list-show foot">
    {{-- <li><a href="#"><i class="fas fa-trash-alt"></i> Delete Files</a></li> --}}
    @php
    	$percent = round(Auth::user()->size / (5*1024*1024*1024) * 100, 2);
    @endphp
    <li><a href="#"><i class="fas fa-chart-pie"></i> Storage ({{$percent}}%)</a></li>
    <li style="padding-left: 20px;padding-bottom: 0;">
        <div style="width: 100%;height: 5px;background-color: #f1f1f1;">
            <div style="width: {{$percent}}%;height: 5px;background-color: #797979;"></div>
        </div>
    </li>
     <li style="padding-left: 20px;">
        <span style="color: #6e6e6e;font-size: 13px;">{{format_size(Auth::user()->size)}} of 5 GB used</span>
     </li>
    <li><a href="{{route('page.setting')}}"><i class="fas fa-cog"></i> Setting</a></li>
</ul>