<ul class="list-show list-show-setting">
    <li><a href="#"><i class="fas fa-cog"></i> Setting</a></li>
    <li><a href="{{route('divisions.index')}}" class="active-division">Division</a></li>
    {{-- <li><a href="{{route('division_details.index')}}" class="active-division-detail">Division Detail</a></li> --}}
    {{-- <li><a href="{{route('division_permissions.index')}}" class="active-division-permission">Division Permission</a></li> --}}
    {{-- <li><a href="{{route('permissions.index')}}" class="active-permission">Permission</a></li>
    <li><a href="{{route('permission_groups.index')}}" class="active-permision-group">Permission Group</a></li> --}}
    {{-- <li><a href="{{route('role_permissions.index')}}" class="active-role-permission">Role Permission</a></li> --}}
    <li><a href="{{route('users.index')}}" class="active-user">User</a></li>
</ul>