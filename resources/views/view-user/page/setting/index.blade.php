@extends('view-user.layout.main')
@section('content')
	<main>
        <div>
            <div class="box-sidebar">
                @include('view-user.page.setting.general-sidebar')
            </div><!--
         --><div class="box-main">
                <div class="row">
                    <div class="col-6">
                        <div class="block-breadcrumb">
                            <ul class="list-breadcrumb">
                                <li><a href="{{route('home')}}" class="pjax"><i class="fas fa-home"></i> / </a></li>
                                <li>Setting</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block-show block-show-scroll">
                    <table class="table-list" style="width:100%">
                        <thead>
                            <tr>
                                {{-- <td style="width: 30px"><input type="checkbox" name="" class="check-all"></td> --}}
                                <td>Name</td>
                                <td style="width: 100px;text-align: center;">Owner</td>
                                <td style="width: 80px;text-align: center;"></td>
                                <td style="width: 100px;text-align: center;">Size</td>
                                <td style="width: 150px;text-align: center;">Last Modified</td>
                            </tr>
                        </thead>
                        <tbody class="html_list_folder">                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')

@endsection