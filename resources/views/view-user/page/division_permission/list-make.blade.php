@if(!empty($list))
@foreach($list as $item)
    @php
        $detail_id = \App\Models\DivisionDetail::where('id', $item['detail_id'])->first();
        if(!empty($detail_id)){
            $detail_id_type = $detail_id->type;
        }else{
            $detail_id_type = NULL;
        }
        $permission_id = \App\Models\Permission::where('id', $item['permission_id'])->first();
        if(!empty($permission_id)){
            $permission_id_name = $permission_id->name;
        }else{
            $permission_id_name = NULL;
        }
    @endphp
	<tr>
        <td class="name">{{$detail_id_type}}</td>
        <td class="name">{{$permission_id_name}}</td>
        <td class="">{{$item['description']}}</td>
        <td class="text-center">
            <div class="dropdown dropdown-custom">
                <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                <div class="dropdown-custom dropdown-menu">
                    <a class="dropdown-item" href="#"><i class="fas fa-info"></i> Detail</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_edit({{$item['id']}})"><i class="fas fa-pen"></i> Edit</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_delete({{$item['id']}})"><i class="fas fa-trash-alt"></i> Delete</a>
                </div>
            </div>
        </td>
        <td class="text-center">{{$item['is_active'] == 1 ? 'Active' : 'Not active'}}</td>
    </tr>
@endforeach
@endif