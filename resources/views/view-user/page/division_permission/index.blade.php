@extends('view-user.layout.main')
@section('content')
	<main>
        <div>
            <div class="box-sidebar">
                @include('view-user.page.setting.general-sidebar')
            </div><!--
         --><div class="box-main">
                <div class="row">
                    <div class="col-6">
                        <div class="block-breadcrumb">
                            <ul class="list-breadcrumb">
                                <li><a href="{{route('home')}}"><i class="fas fa-home"></i> / </a></li>
                                <li><a href="#">Division Permission/ </a></li>
                                <li>
                                    <div class="dropdown dropdown-custom">
                                        <i class="fas fa-plus-square dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal-create"><i class="fas fa-folder-plus" onclick="return false;"></i> New Division Permission</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block-show block-show-scroll">
                    <table class="table-list" style="width:100%">
                        <thead>
                            <tr>
                                <td style="width: 100px;">Division Detail Permission</td>
                                <td style="width: 100px;">Type Permission</td>
                                <td style="width: 100px;">Description</td>
                                <td style="width: 80px;text-align: center;"></td>
                                <td style="width: 80px;text-align: center;">Active</td>
                            </tr>
                        </thead>
                        <tbody class="html_list_data">                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <div class="block-modal">
        <!-- The Modal -->
        <!-- data-toggle="modal" data-target="#myModal-newfolder" -->
        <!-- data-dismiss="modal" -->
        <div class="modal fade model-custom" id="myModal-create">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        {{-- html make --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-update">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        {{-- html make --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to delete: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id-delete" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="function_delete()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('view-user.page.general.waiting')
        @include('view-user.page.general.alert')
    </div>
@endsection
@section('script')
	<script type="text/javascript">
		function function_create(){
            user_id         = $('#myModal-create input[name=user_id]').val();
            division_id     = $('#myModal-create input[name=division_id]').val();
            type            = $('#myModal-create input[name=type]').val();
            description     = $('#myModal-create input[name=description]').val();
            start_time      = $('#myModal-create input[name=start_time]').val();
            end_time        = $('#myModal-create input[name=end_time]').val();
			$.ajax({
				url:'{{route('division_details.store')}}',
				type:'POST',
				data:{
                    user_id         :user_id,
                    division_id     :division_id,
                    type            :type,
                    description     :description,
                    start_time      :start_time,
                    end_time        :end_time,
                    is_active       :1,
                    _token          :"{{ csrf_token() }}"
				}
			}).done(function(data){
				load_list();
                reload_form_create();
				$('#myModal-create').modal('hide');
				$('#myModal-alert .text-content').html('Create Successfully!');
                $('#myModal-alert').modal('show');
			}).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
		}
        function show_modal_edit(id){
            url = '{{route('division_details.edit_view',['division_detail'=>':id'])}}';
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'GET',
                data:{}
            }).done(function(data){
                $('#myModal-update .modal-body').html(data);
                $('#myModal-update').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function update_division(id){
            user_id         = $('#myModal-update input[name=user_id]').val();
            division_id     = $('#myModal-update input[name=division_id]').val();
            type            = $('#myModal-update input[name=type]').val();
            description     = $('#myModal-update input[name=description]').val();
            start_time      = $('#myModal-update input[name=start_time]').val();
            end_time        = $('#myModal-update input[name=end_time]').val();
            created_by      = $('#myModal-update input[name=created_by]').val();
            updated_by      = $('#myModal-update input[name=updated_by]').val();
            deleted_by      = $('#myModal-update input[name=deleted_by]').val();

            url = "{{route('division_details.update',['division_detail'=>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'PUT',
                data:{
                    user_id         :user_id,
                    division_id     :division_id,
                    type            :type,
                    description     :description,
                    start_time      :start_time,
                    end_time        :end_time,
                    created_by      :created_by,
                    updated_by      :updated_by,
                    deleted_by      :deleted_by,
                    is_active       :1,
                    _token          :"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-update').modal('hide');
                $('#myModal-alert .text-content').html('Update Division Detail Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_delete(id, name){
            $('#myModal-delete .text-content').html(name);
            $('#myModal-delete input[name=id-delete]').val(id);
            $('#myModal-delete').modal('show');
        }
        function delete_division(){
            id = $('#myModal-delete input[name=id-delete]').val();
            url = "{{route('division_details.destroy',['division_detail'=>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-delete').modal('hide');
                $('#myModal-alert .text-content').html('Delete Division Detail Successfully!');
                $('#myModal-alert').modal('show');
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
		load_list();
		function load_list(){
            $('.alert-waiting').show();
			$.ajax({
				url:'{{route('division_permissions.list_view')}}',
				type:'GET',
				data:{}
			}).done(function(data){
				$('.html_list_data').html(data);
                $('.alert-waiting').hide();
			}).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('.alert-waiting').hide();
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
		}
        reload_form_create();
        function reload_form_create(){
            $.ajax({
                url:'{{route('division_permissions.reload_form_create')}}',
                type:'GET',
                data:{}
            }).done(function(data){
                $('#myModal-create .modal-body').html(data);
                select2();
            }).fail(function(data) {
                mes_error = data.responseJSON.message;
                $('.modal').modal('hide');
                $('#myModal-alert .text-content').html('<span style="color:red">'+mes_error+'</span>');
                $('#myModal-alert').modal('show');
            })
        }
	</script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.active-division-permission').addClass('active');
        })
    </script>
    <script type="text/javascript">
        function select2(){
            $('.js-example-basic-multiple').select2();
        }
        $(document).ready(function() {
            select2();
        });
    </script>
@endsection