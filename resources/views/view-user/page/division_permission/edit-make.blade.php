<div><strong>New Division Detail</strong></div>
<div class="form-group">
    <label>User id:</label>
    <input type="text" class="form-control" name="user_id" value="{{$model->code}}">
</div>
<div class="form-group">
    <label>Division id:</label>
    <input type="text" class="form-control" name="division_id" value="{{$model->code}}">
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="type">
        <option value="LEADER" @if($model->type == 'LEADER') selected="selected" @endif>LEADER</option>
        <option value="EMPLOYEE" @if($model->type == 'EMPLOYEE') selected="selected" @endif>EMPLOYEE</option>
    </select>
</div>
<div class="form-group">
    <label>Description:</label>
    <input type="text" class="form-control" name="description" value="{{$model->code}}">
</div>
<div class="form-group">
    <label>Start time:</label>
    <input type="text" class="form-control" name="start_time" value="{{$model->code}}">
</div>
<div class="form-group">
    <label>End time:</label>
    <input type="text" class="form-control" name="end_time" value="{{$model->code}}">
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
   <button class="btn btn-create" onclick="function_update({{$model->id}})">Update</button>
</div>