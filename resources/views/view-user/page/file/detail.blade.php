@php
    $size = format_size($record->size);
@endphp
@php
    // time
    $time = '';
    $time_update = $record->updated_at;
    $time_create = $record->created_at;
    if($time_update != NULL){
        $time = date('Y-m-d H:i:s', strtotime($time_update));
    }else{
        $time = date('Y-m-d H:i:s', strtotime($time_create));
    }
@endphp
@php
    // Share
    $id = $record->id;
    $count_status_share = \App\Models\MediaShare::where('file_id', $id)->count();
@endphp
<tr>
    <td class="name"><a href="#"><i class="fas fa-file-alt"></i> {{$record->name}}</a></td>
    <td class="text-center">Me</td>
    <td class="text-center">
        @if($count_status_share)
        <i class="fas fa-share-alt"></i>
        @endif
    </td>
    <td class="text-center">
        <div class="dropdown dropdown-custom">
            <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#" onclick="show_modal_edit_file({{$record->id}},'{{$record->name}}')"><i class="fas fa-pen"></i> Rename</a>
                <a class="dropdown-item" href="#" onclick="show_modal_copy({{$record->id}},'file')"><i class="fas fa-copy"></i> Copy</a>
                <a class="dropdown-item" href="#" onclick="show_modal_move({{$record->id}},'file')"><i class="fas fa-copy"></i> Move</a>
                <a class="dropdown-item" href="#" onclick="action_download({{$record->id}},'file')"><i class="fas fa-download"></i> Download</a>
                <a class="dropdown-item" href="#" onclick="show_modal_share_link({{$record->id}},'file','{{route('files.show',['id'=>$record->id])}}')"><i class="fas fa-share-alt"></i> Share Link</a>
                <a class="dropdown-item" href="#" onclick="show_modal_unshare_link({{$record->id}},'file','{{$record->name}}')"><i class="fas fa-share-alt"></i> Unshare Link</a>
                <a class="dropdown-item" href="#" onclick="show_modal_edit_share({{$record->id}},'file')"><i class="fas fa-share-alt"></i> Share</a>
                <a class="dropdown-item" href="#" onclick="show_modal_unshare({{$record->id}},'{{$record->name}}','file')"><i class="fas fa-share-alt"></i> Unshared</a>
                <a class="dropdown-item" href="#" onclick="show_modal_delete_file({{$record->id}},'{{$record->name}}')"><i class="fas fa-trash-alt"></i> Delete</a>
            </div>
        </div>
    </td>
    <td class="text-center">{{$size}}</td>
    <td class="text-center">{{$time}}</td>
</tr>