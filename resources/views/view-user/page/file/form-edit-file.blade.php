@php
    $array_view_users = $data[0]['view']['users'];
    if(empty($array_view_users)){
        $array_view_users = [0];
    }
    $array_view_divisions = $data[0]['view']['division_id'];
    if(empty($array_view_divisions)){
        $array_view_divisions = [0];
    }

    $array_comment_users = $data[0]['comment']['users'];
    if(empty($array_comment_users)){
        $array_comment_users = [0];
    }
    $array_comment_divisions = $data[0]['comment']['division_id'];
    if(empty($array_comment_divisions)){
        $array_comment_divisions = [0];
    }

    $array_update_users = $data[0]['update']['users'];
    if(empty($array_update_users)){
        $array_update_users = [0];
    }
    $array_update_divisions = $data[0]['update']['division_id'];
    if(empty($array_update_divisions)){
        $array_update_divisions = [0];
    }
@endphp
<div style="color: #0ca44f"><strong>Permission:</strong></div><p></p>
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#edit-share-tab--per-view">VIEW</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#edit-share-tab--per-comment">COMMENT</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#edit-share-tab--per-update">UPDATE</a>
    </li>
</ul>
<!-- Tab panes -->
<div class="tab-content tab-content-edit">
    <div id="edit-share-tab--per-view" class="tab-pane active edit-share-tab--per-view"><br>
        <div class="form-group">
            <label>Share with others:</label>
            <select class="js-example-basic-multiple form-control input-user" name="users[]" multiple="multiple" style="width: 100%;">
                @if(!empty($list_user))
                @foreach($list_user as $item)
                <option value="{{$item['id']}}" @if(in_array($item['id'],$array_view_users)) selected="selected" @endif>{{$item['email']}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Division:</label>
            <select class="js-example-basic-multiple form-control input-division" name="divisions[]" multiple="multiple" style="width: 100%;">
                @if(!empty($list_division))
                @foreach($list_division as $item)
                <option value="{{$item['id']}}" @if(in_array($item['id'],$array_view_divisions)) selected="selected" @endif>{{$item['name']}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <input type="hidden" name="type-permission" value="VIEW" class="type-permission">
    </div>
    <div id="edit-share-tab--per-comment" class="tab-pane fade edit-share-tab--per-comment"><br>
        <div class="form-group">
            <label>Share with others:</label>
            <select class="js-example-basic-multiple form-control input-user" name="users[]" multiple="multiple" style="width: 100%;">
                @if(!empty($list_user))
                @foreach($list_user as $item)
                <option value="{{$item['id']}}" @if(in_array($item['id'],$array_comment_users)) selected="selected" @endif>{{$item['email']}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Division:</label>
            <select class="js-example-basic-multiple form-control input-division" name="divisions[]" multiple="multiple" style="width: 100%;">
                @if(!empty($list_division))
                @foreach($list_division as $item)
                <option value="{{$item['id']}}" @if(in_array($item['id'],$array_comment_divisions)) selected="selected" @endif>{{$item['name']}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <input type="hidden" name="type-permission" value="COMMENT" class="type-permission">
    </div>
    <div id="edit-share-tab--per-update" class="tab-pane fade edit-share-tab--per-update"><br>
        <div class="form-group">
            <label>Share with others:</label>
            <select class="js-example-basic-multiple form-control input-user" name="users[]" multiple="multiple" style="width: 100%;">
                @if(!empty($list_user))
                @foreach($list_user as $item)
                <option value="{{$item['id']}}" @if(in_array($item['id'],$array_update_users)) selected="selected" @endif>{{$item['email']}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Division:</label>
            <select class="js-example-basic-multiple form-control input-division" name="divisions[]" multiple="multiple" style="width: 100%;">
                @if(!empty($list_division))
                @foreach($list_division as $item)
                <option value="{{$item['id']}}" @if(in_array($item['id'],$array_update_divisions)) selected="selected" @endif>{{$item['name']}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <input type="hidden" name="type-permission" value="UPDATE" class="type-permission">
    </div>
</div>
<input type="hidden" name="file_id__edit_share">
<input type="hidden" name="folder_id__edit_share">
<input type="hidden" name="id-edit-share">
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-create" onclick="update_share()">Update</button>
</div>