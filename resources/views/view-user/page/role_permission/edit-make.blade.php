<div><strong>Edit Role Permission</strong></div>
<div class="form-group">
    <label>Role:</label>
    <input type="text" class="form-control" name="role_id" value="{{$model->role_id}}">
</div>
<div class="form-group">
    <label>Permission:</label>
    <input type="text" class="form-control" name="permission_id" value="{{$model->permission_id}}">
</div>

<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
   <button class="btn btn-create" onclick="function_update({{$model->id}})">Update</button>
</div>