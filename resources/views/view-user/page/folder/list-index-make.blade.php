@php
    $data_list = $list->toArray()['data'];
@endphp
@if(!empty($data_list))
@foreach($data_list as $item)
    @php
    if($item['parent_id'] >= 1){
        continue;
    }
    @endphp
    @php
        // Size
        $size = format_size($item['size']);
    @endphp
    @php
        // time
        $time = '';
        $time_update = $item['updated_at'];
        $time_create = $item['created_at'];
        if($time_update != NULL){
            $time = date('Y-m-d H:i:s', strtotime($time_update));
        }else{
            $time = date('Y-m-d H:i:s', strtotime($time_create));
        }
    @endphp
    @php
        // Share
        $id = $item['id'];
        $count_status_share = \App\Models\MediaShare::where('folder_id', $id)->count();
    @endphp
	<tr>
        {{-- <td><input type="checkbox" name="" value="" class="action-check"></td> --}}
        <td class="name"><a href="{{route('folders.detail_index',['folder'=>$item['id']])}}" class="pjax"><i class="fas fa-folder icon-green"></i> {{$item['name']}}</a></td>
        <td class="text-center">Me</td>
        <td class="text-center">
            @if($count_status_share)
            <i class="fas fa-share-alt"></i>
            @endif
        </td>
        <td class="text-center">
            <div class="dropdown dropdown-custom">
                <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#" onclick="show_modal_edit_folder({{$item['id']}},'{{$item['name']}}')"><i class="fas fa-pen"></i> Rename</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_copy({{$item['id']}},'folder')"><i class="fas fa-copy"></i> Copy</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_move({{$item['id']}},'folder')"><i class="fas fa-copy"></i> Move</a>
                    <a class="dropdown-item" href="#" onclick="action_download({{$item['id']}},'folder')"><i class="fas fa-download"></i> Download</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_share_link({{$item['id']}},'folder','{{route('folders.detail_index',['folder'=>$item['id']])}}')"><i class="fas fa-share-alt"></i> Share Link</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_unshare_link({{$item['id']}},'folder','{{$item['name']}}')"><i class="fas fa-share-alt"></i> Unshare Link</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_edit_share({{$item['id']}},'folder')"><i class="fas fa-share-alt"></i> Share</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_unshare({{$item['id']}},'{{$item['name']}}','folder')"><i class="fas fa-share-alt"></i> Unshared</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_delete_folder({{$item['id']}},'{{$item['name']}}')"><i class="fas fa-trash-alt"></i> Delete</a>
                </div>
            </div>
        </td>
        <td class="text-center">{{$size}}</td>
        <td class="text-center">{{$time}}</td>
    </tr>
@endforeach
@endif