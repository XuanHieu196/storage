<div><strong>New Permission Group</strong></div>
<div class="form-group">
    <label>Name:</label>
    <input type="text" class="form-control" name="name" value="{{$model->name}}">
</div>
<div class="form-group">
    <label>Description:</label>
    <input type="text" class="form-control" name="description" value="{{$model->description}}">
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
   <button class="btn btn-create" onclick="function_update({{$model->id}})">Update</button>
</div>