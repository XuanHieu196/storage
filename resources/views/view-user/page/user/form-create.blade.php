<div><strong>New User</strong></div>
<div class="form-group">
    <label>Full name:</label>
    <input type="text" class="form-control" name="fullname" value="">
</div>
<div class="form-group">
    <label>Name Login:</label>
    <input type="text" class="form-control" name="username" value="">
</div>
<div class="form-group">
    <label>Email Login:</label>
    <input type="text" class="form-control" name="email" value="">
</div>
<div class="form-group">
    <label>Password:</label>
    <input type="password" class="form-control" name="password" value="">
</div>
<div class="form-group">
    <label>Phone:</label>
    <input type="text" class="form-control" name="phone" value="">
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="type">
        <option value="ADMIN">ADMIN</option>
        <option value="SUPER_ADMIN">SUPER_ADMIN</option>
        <option value="SALE">SALE</option>
        <option value="ACCOUNTANT">ACCOUNTANT</option>
    </select>
</div>
<div class="form-group">
    <label>Role:</label>
    <select class="form-control" name="role_id">
        <option value="1">SUPER ADMIN</option>
        <option value="2">ADMIN</option>
        <option value="3">USER</option>
    </select>
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    <button class="btn btn-create" onclick="function_create()">Create</button>
</div>