@extends('view-user.layout.main')
@section('content')
	<main>
        <div>
            <div class="box-sidebar">
                @include('view-user.page.setting.general-sidebar')
            </div><!--
         --><div class="box-main">
                <div class="row">
                    <div class="col-6">
                        <div class="block-breadcrumb">
                            <ul class="list-breadcrumb">
                                <li><a href="{{route('home')}}"><i class="fas fa-home"></i> / </a></li>
                                <li><a href="#">User / </a></li>
                                <li>
                                    <div class="dropdown dropdown-custom">
                                        <i class="fas fa-plus-square dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal-create"><i class="fas fa-folder-plus" onclick="return false;"></i> New User</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block-show block-show-scroll">
                    <table class="table-list" style="width:100%">
                        <thead>
                            <tr>
                                <td style="width: 100px;">User name</td>
                                <td style="width: 100px;">Email</td>
                                <td style="width: 100px;">Phone</td>
                                <td style="width: 100px;">Type</td>
                                <td style="width: 80px;">Role</td>
                                <td style="width: 80px;text-align: center;"></td>
                                <td style="width: 80px;">Active</td>
                            </tr>
                        </thead>
                        <tbody class="html_list_data">                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <div class="block-modal">
        <!-- The Modal -->
        <!-- data-toggle="modal" data-target="#myModal-newfolder" -->
        <!-- data-dismiss="modal" -->
        <div class="modal fade model-custom" id="myModal-create">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        {{-- HTML Make --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-update">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        {{-- html make --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade model-custom" id="myModal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div>Are you want to delete: <strong><span class="text-content text-danger"></span></strong></div>
                            <input type="hidden" name="id-delete" value="">
                        </div>
                        <div class="text-right">
                            <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-delete" onclick="function_delete()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('view-user.page.general.waiting')
        @include('view-user.page.general.alert')
    </div>
@endsection
@section('script')
	<script type="text/javascript">
		function function_create(){
            username  = $('#myModal-create input[name=username]').val();
            fullname  = $('#myModal-create input[name=fullname]').val();
            email     = $('#myModal-create input[name=email]').val();
            type      = $('#myModal-create select[name=type]').val();
            role_id  = $('#myModal-create select[name=role_id]').val();
            password  = $('#myModal-create input[name=password]').val();
            phone     = $('#myModal-create input[name=phone]').val();
			$.ajax({
				url:'{{route('users.store')}}',
				type:'POST',
				data:{
                    fullname        :fullname,
                    username        :username,
                    email           :email,
                    type            :type,
                    role_id         :role_id,
                    password        :password,
                    code            :email,
                    phone           :phone,
                    is_active       :1,
                    _token          :"{{ csrf_token() }}"
				}
			}).done(function(data){
				load_list();
                reload_form_create();

				$('#myModal-create').modal('hide');
				$('#myModal-alert .text-content').html('Create Successfully!');
                $('#myModal-alert').modal('show');
			}).fail(function() {
                load_list();
                $('#myModal-create').modal('hide');
                $('#myModal-alert .text-content').html('<span class="text-danger">Create Fail!</span>');
                $('#myModal-alert').modal('show');
            })
		}
        $(document).on('click', '#check-change-password',function(){
            status_input_password();
        })
        function status_input_password(){
            if ($('#check-change-password').is(':checked')) {
                $('.block-check-change-password').show();
            }else{
                $('.block-check-change-password').hide();
            }
        }
        function show_modal_edit(id){
            url = '{{route('users.edit_view',['user'=>':id'])}}';
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'GET',
                data:{}
            }).done(function(data){
                $('#myModal-update .modal-body').html(data);
                status_input_password();
                $('#myModal-update').modal('show');
            })
        }
        function function_update(id){
            username  = $('#myModal-update input[name=username]').val();
            fullname  = $('#myModal-update input[name=fullname]').val();
            email     = $('#myModal-update input[name=email]').val();
            type      = $('#myModal-update select[name=type]').val();
            role_id  = $('#myModal-update select[name=role_id]').val();
            password  = $('#myModal-update input[name=password]').val();
            phone     = $('#myModal-update input[name=phone]').val();

            // Check update password
            if ($('#check-change-password').is(':checked')) {
                data={
                    fullname        :fullname,
                    username        :username,
                    email           :email,
                    type            :type,
                    role_id         :role_id,
                    password        :password,
                    code            :email,
                    phone           :phone,
                    is_active       :1,
                    _token          :"{{ csrf_token() }}"
                }
            }else{
                data={
                    fullname        :fullname,
                    username        :username,
                    email           :email,
                    type            :type,
                    role_id         :role_id,
                    code            :email,
                    phone           :phone,
                    is_active       :1,
                    _token          :"{{ csrf_token() }}"
                }
            }

            url = "{{route('users.update',['user'=>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'PUT',
                data:data
            }).done(function(data){
                load_list();
                $('#myModal-update').modal('hide');
                $('#myModal-alert .text-content').html('Update User Successfully!');
                $('#myModal-alert').modal('show');
            })
        }
        function show_modal_delete(id, name){
            $('#myModal-delete .text-content').html(name);
            $('#myModal-delete input[name=id-delete]').val(id);
            $('#myModal-delete').modal('show');
        }
        function function_delete(){
            id = $('#myModal-delete input[name=id-delete]').val();
            url = "{{route('users.destroy',['user'=>':id'])}}";
            url = url.replace(':id', id);
            $.ajax({
                url:url,
                type:'DELETE',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                load_list();
                $('#myModal-delete').modal('hide');
                $('#myModal-alert .text-content').html('Delete User Successfully!');
                $('#myModal-alert').modal('show');
            })
        }
		load_list();
		function load_list(){
            $('.alert-waiting').show();
			$.ajax({
				url:'{{route('users.list_view')}}',
				type:'GET',
				data:{}
			}).done(function(data){
				$('.html_list_data').html(data);
                $('.alert-waiting').hide();
			})
		}
        reload_form_create();
        function reload_form_create(){
            $.ajax({
                url:'{{route('users.reload_form_create')}}',
                type:'GET',
                data:{}
            }).done(function(data){
                $('#myModal-create .modal-body').html(data);
            })
        }
	</script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.active-user').addClass('active');
        })
    </script>
@endsection