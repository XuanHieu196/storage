<div><strong>Edit User</strong></div>
<div class="form-group">
    <label>Full name:</label>
    <input type="text" class="form-control" name="fullname" value="{{$model->profiles->full_name}}">
</div>
<div class="form-group">
    <label>Name Login:</label>
    <input type="text" class="form-control" name="username" value="{{$model->username}}">
</div>
<div class="form-group">
    <label>Email Login:</label>
    <input type="text" class="form-control" name="email" value="{{$model->email}}">
</div>
<div class="custom-control custom-checkbox mb-3">
    <input type="checkbox" class="custom-control-input" id="check-change-password" name="check-change-password">
    <label class="custom-control-label" for="check-change-password">Change Password</label>
</div>
<div class="form-group block-check-change-password">
    <label>Password:</label>
    <input type="password" class="form-control input-check-change-password" name="password" value="">
</div>
<div class="form-group">
    <label>Phone:</label>
    <input type="text" class="form-control" name="phone" value="{{$model->phone}}">
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="type">
        <option value="ADMIN" @if($model->type == 'ADMIN') selected="selected" @endif>ADMIN</option>
        <option value="SUPER_ADMIN" @if($model->type == 'SUPER_ADMIN') selected="selected" @endif>SUPER_ADMIN</option>
        <option value="SALE" @if($model->type == 'SALE') selected="selected" @endif>SALE</option>
        <option value="ACCOUNTANT" @if($model->type == 'ACCOUNTANT') selected="selected" @endif>ACCOUNTANT</option>
    </select>
</div>
<div class="form-group">
    <label>Role:</label>
    <select class="form-control" name="role_id">
        <option value="1" @if($model->role_id == '1') selected="selected" @endif>SUPER ADMIN</option>
        <option value="2" @if($model->role_id == '2') selected="selected" @endif>ADMIN</option>
        <option value="3" @if($model->role_id == '3') selected="selected" @endif>USER</option>
    </select>
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
   <button class="btn btn-create" onclick="function_update({{$model->id}})">Update</button>
</div>