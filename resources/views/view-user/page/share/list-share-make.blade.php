@if(!empty($data))
    @foreach($data as $item)
        @php
            if($item['type'] != "FOLDER"){
                continue;
            }

            // Size
            $size = format_size($item['size']);

            // time
            $time = '';
            $time_update = $item['updated_at'];
            $time_create = $item['created_at'];
            if($time_update != NULL){
                $time = date('Y-m-d H:i:s', strtotime($time_update));
            }else{
                $time = date('Y-m-d H:i:s', strtotime($time_create));
            }
        @endphp
        <tr>
            <td class="name"><a href="{{route('folders.detail_index',['folder'=>$item['id']])}}" class="pjax"><i class="fas fa-folder icon-green"></i> {{$item['name']}}</a></td>
            <td class="text-center">Me</td>
            <td class="text-center">
                <div class="dropdown dropdown-custom">
                <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#" onclick="action_download({{$item['id']}},'folder')"><i class="fas fa-download"></i> Download</a>
                </div>
            </div>
            </td>
            <td class="text-center">{{$size}}</td>
            <td class="text-center">{{$time}}</td>
        </tr>
    @endforeach
    @foreach($data as $item)
        @php
            if($item['type'] != "FILE"){
                continue;
            }

            // Size
            $size = format_size($item['size']);

            // time
            $time = '';
            $time_update = $item['updated_at'];
            $time_create = $item['created_at'];
            if($time_update != NULL){
                $time = date('Y-m-d H:i:s', strtotime($time_update));
            }else{
                $time = date('Y-m-d H:i:s', strtotime($time_create));
            }
        @endphp
        <tr>
            <td class="name"><a href="#"><i class="fas fa-file-alt"></i> {{$item['name']}}</a></td>
            <td class="text-center">Me</td>
            <td class="text-center">
                <div class="dropdown dropdown-custom">
                    <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" onclick="action_download({{$item['id']}},'file')"><i class="fas fa-download"></i> Download</a>
                    </div>
                </div>
            </td>
            <td class="text-center">{{$size}}</td>
            <td class="text-center">{{$time}}</td>
        </tr>
    @endforeach
@endif