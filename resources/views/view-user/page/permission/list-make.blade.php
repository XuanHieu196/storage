@if(!empty($list))
@foreach($list as $item)
    @php
        $permision_group = \App\Models\PermissionGroup::where('id', $item['group_id'])->first();
        if(!empty($permision_group)){
            $permision_group_name = $permision_group->name;
        }else{
            $permision_group_name = NULL;
        }
    @endphp
	<tr>
        <td class="name">{{$item['name']}}</td>
        <td class="">{{$item['type']}}</td>
        <td class="">{{$item['description']}}</td>
        <td class="">{{$permision_group_name}}</td>
        <td class="text-center">
            <div class="dropdown dropdown-custom">
                <i class="fas fa-ellipsis-h dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown"></i>
                <div class="dropdown-custom dropdown-menu">
                    <a class="dropdown-item" href="#"><i class="fas fa-info"></i> Detail</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_edit({{$item['id']}})"><i class="fas fa-pen"></i> Edit</a>
                    <a class="dropdown-item" href="#" onclick="show_modal_delete({{$item['id']}})"><i class="fas fa-trash-alt"></i> Delete</a>
                </div>
            </div>
        </td>
        <td class="text-center">{{$item['is_active'] == 1 ? 'Active' : 'Not active'}}</td>
    </tr>
@endforeach
@endif