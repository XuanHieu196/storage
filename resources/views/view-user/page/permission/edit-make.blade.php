<div><strong>New Permission</strong></div>
<div class="form-group">
    <label>Name:</label>
    <input type="text" class="form-control" name="name" value="{{$model->name}}">
</div>
<div class="form-group">
    <label>Type:</label>
    <select class="form-control" name="type">
        <option value="DIVISION"  @if($model->type == 'DIVISION') selected="selected" @endif>DIVISION</option>
        <option value="ROLE"  @if($model->type == 'ROLE') selected="selected" @endif>ROLE</option>
    </select>
</div>
<div class="form-group">
    <label>Description:</label>
    <input type="text" class="form-control" name="description" value="{{$model->description}}">
</div>
<div class="form-group">
    <label>Group ID:</label>
    <input type="text" class="form-control" name="group_id" value="{{$model->group_id}}">
</div>
<div class="text-right">
    <button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
   <button class="btn btn-create" onclick="function_update({{$model->id}})">Update</button>
</div>