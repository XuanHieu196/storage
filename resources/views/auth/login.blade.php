<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="SSO KDATA">
    <meta name="author" content="Kdata; kdata.vn; KDATA">
    <meta name="keyword" content="">
    <link rel="icon" type="image/ico" href=""/>
    <title>Storage</title>
    <!-- Main styles for this application-->
    <link href="{{asset('style/auth/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('style/auth/bootstrap4.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">
    <!--custom css-->
    <link href="{{asset('style/auth/mystyle.css')}}" rel="stylesheet" />
    <style type="text/css">
    .form-auth .input-group {
        border: 1px solid #1a393e;
        margin-bottom: 20px;
        border-radius: 7px;
        overflow: hidden;
    }
    .form-auth .input-group .input-group-addon{
        line-height: 33px;
        padding: 0 10px;
    }
    .form-auth .input-group input{
        background-color: #FFF;
    }
    .form-auth input{
        color: #1a393e!important;
    }
</style>

</head>
<body class="app flex-row align-items-center" style="background: url({{asset('style/auth/bg.jpg')}}); background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="page-auth form-auth">

            <div class="row">
                <div class="col-md-12">
                    <div class="row justify-content">
                        <div class="col-md-4">
                            <div class="box-content">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box-padding">
                                                <div class="title">Đăng nhập</div>
                                                <div class="row mes_info">
                                                    <div class="col-lg-12 content">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            {{-- <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span> --}}
                                                            <input class="form-control" type="email" name="email" placeholder="Email" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            {{-- <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span> --}}
                                                            <input class="form-control" type="password" name="password" placeholder="Mật khẩu" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="text-center">
                                                            <button type="submit" class="btn-submit">Đăng nhập</button>
                                                            <div class="clearfix"></div>
                                                            <div style="margin-top: 10px;"></div>
                                                            {{-- <span style="color: #000;font-size: 13px;">Bạn chưa có tài khoản? <a href="/register" style="color: #0095da;text-decoration: underline;font-size: 13px;">Đăng ký ngay</a></span>
                                                            <div style="margin-top: 0px;"></div>
                                                            <a href="{{ route('password.request') }}" class="btn-forgot">Quên mật khẩu</a> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{asset('style/auth/jquery.min.js')}}"></script>
    <script src="{{asset('style/auth/bootstrap.min.js')}}"></script>            
</body>
</html>