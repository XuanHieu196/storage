<?php

return [
    'divisions'            => 'Division',
    'division_details'     => 'Division detail',
    'division_permissions' => 'Division permission',
    'files'                => 'File',
    'folders'              => 'Folder',
    'media_shares'         => 'Media share',
    'password_resets'      => 'Password reset',
    'permissions'          => 'Permission',
    'permission_groups'    => 'Permission group',
    'profiles'             => 'Profile',
    'roles'                => 'Role',
    'role_permissions'     => 'Role permission',
    'users'                => 'User',
    'user_logs'            => 'User log',
];
