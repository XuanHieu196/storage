// $('#datepicker').datepicker({
//     uiLibrary: 'bootstrap4',
//     format: 'yyyy-mm-dd'
// });
$(document).on('click', 'input[name=check-password-protect]', function(){
	if($( this ).prop( "checked" )){
		$('input[name=value-password-protect]').show();
	}else{
		$('input[name=value-password-protect]').hide();
	}
})
$(document).on('click', 'input[name=check-expire]', function(){
	if($( this ).prop( "checked" )){
		$('#myModal-share .td-expire').show();
	}else{
		$('#myModal-share .td-expire').hide();
	}
})
// $(".js-example-basic-multiple").select2({
//   // maximumSelectionLength: 2
// });
function copy(id) {
	var copyText = document.getElementById(id);
	copyText.select();
	copyText.setSelectionRange(0, 99999)
	document.execCommand("copy");
}
$(function(){
// pjax
	$(document).pjax('a.pjax-a', 'body')
})
$(document).ready(function(){
	// does current browser support PJAX
	if ($.support.pjax) {
	$.pjax.defaults.timeout = 3000; // time in milliseconds
	}

});