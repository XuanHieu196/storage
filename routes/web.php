<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('page-web',function(){
	return view('view-user.hello');
});
Route::get('home',function(){
	return redirect('/');
});
Auth::routes();

Route::post('/upload','UploadController@postUpload')->name('post.upload');
Route::get('/upload','UploadController@getUpload');

Route::get('','HomeController@index')->name('home');
Route::get('setting','HomeController@setting_index')->name('page.setting');
