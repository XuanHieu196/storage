<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'drive'], function () {
    Route::get('drives', [
        'uses' => 'FolderController@listApi',
    ]);

//    Route::get('folders', [
//        'uses' => 'FolderController@listApi',
//    ]);

    Route::get('folders/detail/{id}', [
        'uses' => 'FolderController@detailApi',
    ]);

//    Route::get('media_shares/list', [
//        'uses' => 'MediaShareController@listApi',
//    ]);

    // User
    Route::post('users', [
        'uses' => 'UserController@storeApi',
    ]);

    Route::put('users/{id}', [
        'uses' => 'UserController@updateApi',
    ]);

    Route::get('users/{id}', [
        'uses' => 'UserController@detailApi',
    ]);
});
