<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth', 'permission']], function () {

    // Folder
    require __DIR__ . '/web/folder.php';

    // Division
    require __DIR__ . '/web/division.php';

    // Division details
    require __DIR__ . '/web/division_detail.php';

    // Division permission
    require __DIR__ . '/web/division_permission.php';

    // Permission
    require __DIR__ . '/web/permission.php';

    // Role permission
    require __DIR__ . '/web/role_permission.php';

    // Media share
    require __DIR__ . '/web/media_share.php';

    // Permission group
    require __DIR__ . '/web/permission_group.php';

    // User
    require __DIR__ . '/web/user.php';

    // File
    require __DIR__ . '/web/file.php';
});


