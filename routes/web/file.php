<?php

use Illuminate\Support\Facades\Route;

Route::get('files', [
    'uses'       => 'FileController@index',
    'as'         => 'files.index',
]);
Route::get('files/list', [
    'uses'       => 'FileController@list',
    'as'         => 'files.list',
]);
Route::get('files/list_view', [
    'uses'       => 'FileController@list_view',
    'as'         => 'files.list_view',
]);
Route::get('files/list_view', [
    'uses'       => 'FileController@list_view',
    'as'         => 'files.list_view',
]);
Route::get('files/create', [
    'uses'       => 'FileController@create',
    'as'         => 'files.create',
]);

Route::post('files', [
    'uses'       => 'FileController@store',
    'as'         => 'files.store',
]);

Route::get('files/{id}', [
    'uses'       => 'FileController@show',
    'as'         => 'files.show',
]);

Route::get('files_view/{id}', [
    'uses'       => 'FileController@show_view',
    'as'         => 'files.show_view',
]);

Route::get('files/{id}/edit', [
    'uses'       => 'FileController@edit',
    'as'         => 'files.edit',
]);

Route::put('files/{id}', [
    'uses'       => 'FileController@update',
    'as'         => 'files.update',
]);

Route::delete('files/{id}', [
    'uses'       => 'FileController@destroy',
    'as'         => 'files.destroy',
]);


Route::get('files/download/{id}', [
    'uses'       => 'FileController@download',
    'as'         => 'files.download',
]);

Route::put('files/copy/{id}', [
    'uses'       => 'FileController@copy',
    'as'         => 'files.copy',
]);

Route::delete('files/unshare/{id}', [
    'uses'       => 'FileController@unShare',
    'as'         => 'files.unshare',
]);

Route::put('files/share_permission/{id}', [
    'uses'       => 'FileController@sharePermission',
    'as'         => 'files.share_permission',
]);

Route::put('files/move/{id}', [
    'uses'       => 'FileController@move',
    'as'         => 'files.move',
]);

// share link
Route::put('files/share/{id}', [
    'uses'       => 'FileController@shareLink',
    'as'         => 'files.share',
]);

// Un share link
Route::put('files/un_share/{id}', [
    'uses'       => 'FileController@unShareLink',
    'as'         => 'files.un_share',
]);


Route::get('files/preview/{id}', [
    'uses'       => 'FileController@preview',
    'as'         => 'files.preview',
]);
