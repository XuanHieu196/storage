<?php

use Illuminate\Support\Facades\Route;

Route::get('division_details', [
    'uses'       => 'DivisionDetailController@index',
    'as'         => 'division_details.index',
//    'permission' => 'VIEW-DIVISION-DETAIL'
]);
Route::get('division_details/list-view', [
    'uses'       => 'DivisionDetailController@list_view',
    'as'         => 'division_details.list_view',
//    'permission' => 'VIEW-DIVISION-DETAIL'
]);
Route::get('division_details/ajax/reload-form-create', [
    'uses'       => 'DivisionDetailController@reload_form_create',
    'as'         => 'division_details.reload_form_create',
//    'permission' => 'VIEW-DIVISION'
]);
Route::get('division_details/create', [
    'uses'       => 'DivisionDetailController@create',
    'as'         => 'division_details.create',
//    'permission' => 'CREATE-DIVISION-DETAIL'
]);

Route::post('division_details', [
    'uses'       => 'DivisionDetailController@store',
    'as'         => 'division_details.store',
//    'permission' => 'CREATE-DIVISION-DETAIL'
]);

Route::get('division_details/{division_detail}', [
    'uses'       => 'DivisionDetailController@show',
    'as'         => 'division_details.show',
//    'permission' => 'VIEW-DIVISION-DETAIL'
]);

Route::get('division_details/{division_detail}/edit', [
    'uses'       => 'DivisionDetailController@edit',
    'as'         => 'division_details.edit',
//    'permission' => 'UPDATE-DIVISION-DETAIL'
]);

Route::get('division_details/{division_detail}/edit-view', [
    'uses'       => 'DivisionDetailController@edit_view',
    'as'         => 'division_details.edit_view',
//    'permission' => 'UPDATE-DIVISION-DETAIL'
]);

Route::put('division_details/{division_detail}', [
    'uses'       => 'DivisionDetailController@update',
    'as'         => 'division_details.update',
//    'permission' => 'UPDATE-DIVISION-DETAIL'
]);

Route::delete('division_details/{division_detail}', [
    'uses'       => 'DivisionDetailController@destroy',
    'as'         => 'division_details.destroy',
//    'permission' => 'DELETE-DIVISION-DETAIL'
]);

// --------------------------
// For Division
Route::get('division_details/list-view/{id_division}', [
    'uses'       => 'DivisionDetailController@list_view_id_division',
    'as'         => 'division_details.list_view_id_division',
//    'permission' => 'VIEW-DIVISION-DETAIL'
]);
Route::get('division_details/ajax/reload-form-create/{id_division}', [
    'uses'       => 'DivisionDetailController@reload_form_create_id_division',
    'as'         => 'division_details.reload_form_create_id_division',
//    'permission' => 'VIEW-DIVISION'
]);
Route::get('division_details/{division_detail}/edit-view/{id_division}', [
    'uses'       => 'DivisionDetailController@edit_view_id_division',
    'as'         => 'division_details.edit_view_id_division',
//    'permission' => 'UPDATE-DIVISION-DETAIL'
]);
// --------------------------
