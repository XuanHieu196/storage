<?php

use Illuminate\Support\Facades\Route;

Route::get('permissions', [
    'uses'       => 'PermissionController@index',
    'as'         => 'permissions.index',
    'permission' => 'VIEW-PERMISSION'
]);
Route::get('permissions/list-view', [
    'uses'       => 'PermissionController@list_view',
    'as'         => 'permissions.list_view',
    'permission' => 'VIEW-PERMISSION'
]);
Route::get('permissions/create', [
    'uses'       => 'PermissionController@create',
    'as'         => 'permissions.create',
    'permission' => 'CREATE-PERMISSION'
]);

Route::post('permissions', [
    'uses'       => 'PermissionController@store',
    'as'         => 'permissions.store',
    'permission' => 'CREATE-PERMISSION'
]);

Route::get('permissions/{permission}', [
    'uses'       => 'PermissionController@show',
    'as'         => 'permissions.show',
    'permission' => 'VIEW-PERMISSION'
]);

Route::get('permissions/{permission}/edit', [
    'uses'       => 'PermissionController@edit',
    'as'         => 'permissions.edit',
    'permission' => 'UPDATE-PERMISSION'
]);
Route::get('permissions/{permission}/edit-view', [
    'uses'       => 'PermissionController@edit_view',
    'as'         => 'permissions.edit_view',
    'permission' => 'UPDATE-PERMISSION'
]);
Route::put('permissions/{permission}', [
    'uses'       => 'PermissionController@update',
    'as'         => 'permissions.update',
    'permission' => 'UPDATE-PERMISSION'
]);

Route::delete('permissions/{permission}', [
    'uses'       => 'PermissionController@destroy',
    'as'         => 'permissions.destroy',
    'permission' => 'DELETE-PERMISSION'
]);
