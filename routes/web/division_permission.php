<?php

use Illuminate\Support\Facades\Route;

Route::get('division_permissions', [
    'uses'       => 'DivisionPermissionController@index',
    'as'         => 'division_permissions.index',
    'permission' => 'VIEW-DIVISION-PERMISSION'
]);
Route::get('division_permissions/list-view', [
    'uses'       => 'DivisionPermissionController@list_view',
    'as'         => 'division_permissions.list_view',
    'permission' => 'VIEW-DIVISION-PERMISSION'
]);
Route::get('division_permissions/ajax/reload-form-create', [
    'uses'       => 'DivisionPermissionController@reload_form_create',
    'as'         => 'division_permissions.reload_form_create',
    'permission' => 'VIEW-DIVISION'
]);
Route::get('division_permissions/create', [
    'uses'       => 'DivisionPermissionController@create',
    'as'         => 'division_permissions.create',
    'permission' => 'CREATE-DIVISION-PERMISSION'
]);

Route::post('division_permissions', [
    'uses'       => 'DivisionPermissionController@store',
    'as'         => 'division_permissions.store',
    'permission' => 'CREATE-DIVISION-PERMISSION'
]);

Route::get('division_permissions/{division_permission}', [
    'uses'       => 'DivisionPermissionController@show',
    'as'         => 'division_permissions.show',
    'permission' => 'VIEW-DIVISION-PERMISSION'
]);

Route::get('division_permissions/{division_permission}/edit', [
    'uses'       => 'DivisionPermissionController@edit',
    'as'         => 'division_permissions.edit',
    'permission' => 'UPDATE-DIVISION-PERMISSION'
]);
Route::get('division_permissions/{division_permission}/edit-view', [
    'uses'       => 'DivisionPermissionController@edit_view',
    'as'         => 'division_permissions.edit_view',
    'permission' => 'UPDATE-DIVISION-PERMISSION'
]);
Route::put('division_permissions/{division_permission}', [
    'uses'       => 'DivisionPermissionController@update',
    'as'         => 'division_permissions.update',
    'permission' => 'UPDATE-DIVISION-PERMISSION'
]);

Route::delete('division_permissions/{division_permission}', [
    'uses'       => 'DivisionPermissionController@destroy',
    'as'         => 'division_permissions.destroy',
    'permission' => 'DELETE-DIVISION-PERMISSION'
]);
