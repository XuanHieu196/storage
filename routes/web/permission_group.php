<?php

use Illuminate\Support\Facades\Route;

Route::get('permission_groups', [
    'uses'       => 'PermissionGroupController@index',
    'as'         => 'permission_groups.index',
    'permission' => 'VIEW-PERMISSION-GROUP'
]);
Route::get('permission_groups/list-view', [
    'uses'       => 'PermissionGroupController@list_view',
    'as'         => 'permission_groups.list_view',
    'permission' => 'VIEW-PERMISSION-GROUP'
]);
Route::get('permission_groups/create', [
    'uses'       => 'PermissionGroupController@create',
    'as'         => 'permission_groups.create',
    'permission' => 'CREATE-PERMISSION-GROUP'
]);

Route::post('permission_groups', [
    'uses'       => 'PermissionGroupController@store',
    'as'         => 'permission_groups.store',
    'permission' => 'CREATE-PERMISSION-GROUP'
]);

Route::get('permission_groups/{permission_group}', [
    'uses'       => 'PermissionGroupController@show',
    'as'         => 'permission_groups.show',
    'permission' => 'VIEW-PERMISSION-GROUP'
]);

Route::get('permission_groups/{permission_group}/edit', [
    'uses'       => 'PermissionGroupController@edit',
    'as'         => 'permission_groups.edit',
    'permission' => 'UPDATE-PERMISSION-GROUP'
]);
Route::get('permission_groups/{permission_group}/edit-view', [
    'uses'       => 'PermissionGroupController@edit_view',
    'as'         => 'permission_groups.edit_view',
    'permission' => 'UPDATE-PERMISSION-GROUP'
]);
Route::put('permission_groups/{permission_group}', [
    'uses'       => 'PermissionGroupController@update',
    'as'         => 'permission_groups.update',
    'permission' => 'UPDATE-PERMISSION-GROUP'
]);

Route::delete('permission_groups/{permission_group}', [
    'uses'       => 'PermissionGroupController@destroy',
    'as'         => 'permission_groups.destroy',
    'permission' => 'DELETE-PERMISSION-GROUP'
]);
