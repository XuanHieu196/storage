<?php

use Illuminate\Support\Facades\Route;

Route::get('media_shares', [
    'uses'       => 'MediaShareController@index',
    'as'         => 'media_shares.index',
]);

Route::get('media_shares/list', [
    'uses'       => 'MediaShareController@list',
    'as'         => 'media_shares.list',
]);

Route::get('media_shares/create', [
    'uses'       => 'MediaShareController@create',
    'as'         => 'media_shares.create',
]);

Route::post('media_shares', [
    'uses'       => 'MediaShareController@store',
    'as'         => 'media_shares.store',
]);

Route::get('media_shares/{media_share}', [
    'uses'       => 'MediaShareController@show',
    'as'         => 'media_shares.show',
]);

Route::get('media_shares/{media_share}/edit', [
    'uses'       => 'MediaShareController@edit',
    'as'         => 'media_shares.edit',
]);

Route::put('media_shares/{media_share}', [
    'uses'       => 'MediaShareController@update',
    'as'         => 'media_shares.update',
]);

Route::delete('media_shares/{id}', [
    'uses'       => 'MediaShareController@destroy',
    'as'         => 'media_shares.destroy',
]);

Route::delete('media_shares/sync/all', [
    'uses'       => 'MediaShareController@sync',
    'as'         => 'media_shares.sync',
]);
