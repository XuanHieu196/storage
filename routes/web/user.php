<?php

use Illuminate\Support\Facades\Route;

Route::get('users', [
    'uses'       => 'UserController@index',
    'as'         => 'users.index',
    'permission' => 'VIEW-USER'
]);
Route::get('users/list-view', [
    'uses'       => 'UserController@list_view',
    'as'         => 'users.list_view',
    'permission' => 'VIEW-USER'
]);
Route::get('users/reload-form-create', [
    'uses'       => 'UserController@reload_form_create',
    'as'         => 'users.reload_form_create',
    'permission' => 'VIEW-DIVISION'
]);
Route::get('users/create', [
    'uses'       => 'UserController@create',
    'as'         => 'users.create',
    'permission' => 'CREATE-USER'
]);

Route::post('users', [
    'uses'       => 'UserController@store',
    'as'         => 'users.store',
    'permission' => 'CREATE-USER'
]);

Route::get('users/{user}', [
    'uses'       => 'UserController@show',
    'as'         => 'users.show',
    'permission' => 'VIEW-USER'
]);

Route::get('users/{user}/edit', [
    'uses'       => 'UserController@edit',
    'as'         => 'users.edit',
    'permission' => 'UPDATE-USER'
]);
Route::get('users/{user}/edit-view', [
    'uses'       => 'UserController@edit_view',
    'as'         => 'users.edit_view',
    'permission' => 'UPDATE-USER'
]);
Route::put('users/{user}', [
    'uses'       => 'UserController@update',
    'as'         => 'users.update',
    'permission' => 'UPDATE-USER'
]);

Route::delete('users/{user}', [
    'uses'       => 'UserController@destroy',
    'as'         => 'users.destroy',
    'permission' => 'DELETE-USER'
]);
