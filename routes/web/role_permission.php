<?php

use Illuminate\Support\Facades\Route;

Route::get('role_permissions', [
    'uses'       => 'RolePermissionController@index',
    'as'         => 'role_permissions.index',
    'permission' => 'VIEW-ROLE-PERMISSON'
]);
Route::get('role_permissions/list-view', [
    'uses'       => 'RolePermissionController@list_view',
    'as'         => 'role_permissions.list_view',
    'permission' => 'VIEW-ROLE-PERMISSON'
]);
Route::get('role_permissions/create', [
    'uses'       => 'RolePermissionController@create',
    'as'         => 'role_permissions.create',
    'permission' => 'CREATE-ROLE-PERMISSON'
]);

Route::post('role_permissions', [
    'uses'       => 'RolePermissionController@store',
    'as'         => 'role_permissions.store',
    'permission' => 'CREATE-ROLE-PERMISSON'
]);

Route::get('role_permissions/{role_permission}', [
    'uses'       => 'RolePermissionController@show',
    'as'         => 'role_permissions.show',
    'permission' => 'VIEW-ROLE-PERMISSON'
]);

Route::get('role_permissions/{role_permission}/edit', [
    'uses'       => 'RolePermissionController@edit',
    'as'         => 'role_permissions.edit',
    'permission' => 'UPDATE-ROLE-PERMISSON'
]);
Route::get('role_permissions/{role_permission}/edit-view', [
    'uses'       => 'RolePermissionController@edit_view',
    'as'         => 'role_permissions.edit_view',
    'permission' => 'UPDATE-ROLE-PERMISSON'
]);
Route::put('role_permissions/{role_permission}', [
    'uses'       => 'RolePermissionController@update',
    'as'         => 'role_permissions.update',
    'permission' => 'UPDATE-ROLE-PERMISSON'
]);

Route::delete('role_permissions/{role_permission}', [
    'uses'       => 'RolePermissionController@destroy',
    'as'         => 'role_permissions.destroy',
    'permission' => 'DELETE-ROLE-PERMISSON'
]);
