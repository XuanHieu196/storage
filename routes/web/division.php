<?php

use Illuminate\Support\Facades\Route;

Route::get('divisions', [
    'uses'       => 'DivisionController@index',
    'as'         => 'divisions.index',
//    'permission' => 'VIEW-DIVISION'
]);

Route::get('divisions/list', [
    'uses'       => 'DivisionController@list',
    'as'         => 'divisions.list',
//    'permission' => 'VIEW-DIVISION'
]);
Route::get('divisions/list-view', [
    'uses'       => 'DivisionController@list_view',
    'as'         => 'divisions.list_view',
//    'permission' => 'VIEW-DIVISION'
]);
Route::get('divisions/ajax/reload-form-create', [
    'uses'       => 'DivisionController@reload_form_create',
    'as'         => 'divisions.reload_form_create',
//    'permission' => 'VIEW-DIVISION'
]);
Route::get('divisions/create', [
    'uses'       => 'DivisionController@create',
    'as'         => 'divisions.create',
//    'permission' => 'CREATE-DIVISION'
]);

Route::post('divisions', [
    'uses'       => 'DivisionController@store',
    'as'         => 'divisions.store',
//    'permission' => 'CREATE-DIVISION'
]);

Route::get('divisions/{division}', [
    'uses'       => 'DivisionController@show',
    'as'         => 'divisions.show',
//    'permission' => 'VIEW-DIVISION'
]);

Route::get('divisions/{division}/edit', [
    'uses'       => 'DivisionController@edit',
    'as'         => 'divisions.edit',
//    'permission' => 'UPDATE-DIVISION'
]);
Route::get('divisions/{division}/edit-view', [
    'uses'       => 'DivisionController@edit_view',
    'as'         => 'divisions.edit_view',
//    'permission' => 'UPDATE-DIVISION'
]);

Route::put('divisions/{division}', [
    'uses'       => 'DivisionController@update',
    'as'         => 'divisions.update',
//    'permission' => 'UPDATE-DIVISION'
]);

Route::delete('divisions/{division}', [
    'uses'       => 'DivisionController@destroy',
    'as'         => 'divisions.destroy',

//    'permission' => 'DELETE-DIVISION'
]);

Route::get('divisions/detail-list/{division}', [
    'uses'       => 'DivisionController@detail_list',
    'as'         => 'divisions.detail_list',
//    'permission' => 'VIEW-DIVISION'
]);


// Drive share for division
Route::get('divisions/drive/{id}', [
    'uses'       => 'DivisionController@driveShare',
    'as'         => 'divisions.drive_share',
]);
