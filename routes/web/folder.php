<?php

use Illuminate\Support\Facades\Route;

Route::get('folders', [
    'uses'       => 'FolderController@index',
    'as'         => 'folders.index',
]);

Route::get('folders/tree', [
    'uses'       => 'FolderController@list_tree',
    'as'         => 'folders.list_tree',
]);

Route::get('folders/load', [
    'uses'       => 'FolderController@load',
    'as'         => 'folders.load',
]);
Route::get('folders/load-view/share', [
    'uses'       => 'FolderController@load_view_share',
    'as'         => 'folders.load_view_share',
]);
Route::get('folders/load-view/search', [
    'uses'       => 'FolderController@load_view_search',
    'as'         => 'folders.load_view_search',
]);

Route::get('folders/detail/{folder}', [
    'uses'       => 'FolderController@detail',
    'as'         => 'folders.detail',
]);
Route::get('folders/detail-view/{folder}', [
    'uses'       => 'FolderController@detail_view',
    'as'         => 'folders.detail_view',
]);

Route::get('folders/detail-index/{folder}', [
    'uses'       => 'FolderController@detail_index',
    'as'         => 'folders.detail_index',
]);

Route::get('folders/list', [
    'uses'       => 'FolderController@list',
    'as'         => 'folders.list',
]);
Route::get('folders/list-view', [
    'uses'       => 'FolderController@list_view',
    'as'         => 'folders.list_view',
]);

Route::get('folders/create', [
    'uses'       => 'FolderController@create',
    'as'         => 'folders.create',
]);

Route::post('folders', [
    'uses'       => 'FolderController@store',
    'as'         => 'folders.store',
]);

Route::get('folders/{folder}', [
    'uses'       => 'FolderController@show',
    'as'         => 'folders.show',
]);

Route::get('folders/{folder}/edit', [
    'uses'       => 'FolderController@edit',
    'as'         => 'folders.edit',
]);

Route::put('folders/{folder}', [
    'uses'       => 'FolderController@update',
    'as'         => 'folders.update',
//    'permission' => 'UPDATE-FOLDER'
]);

Route::delete('folders/{folder}', [
    'uses'       => 'FolderController@destroy',
    'as'         => 'folders.destroy',
//    'permission' => 'DELETE-FOLDER'
]);

Route::get('folders/download/{id}', [
    'uses'       => 'FolderController@download',
    'as'         => 'folders.download',
//    'permission' => 'VIEW-FOLDER'
]);

Route::put('folders/copy/{folder}', [
    'uses'       => 'FolderController@copy',
    'as'         => 'folders.copy',
//    'permission' => 'UPDATE-FOLDER'
]);

Route::delete('folders/unshare/{id}', [
    'uses'       => 'FolderController@unShare',
    'as'         => 'folders.unshare',
//    'permission' => 'VIEW-FOLDER'
]);

Route::put('folders/share_permission/{id}', [
    'uses'       => 'FolderController@sharePermission',
    'as'         => 'folders.share_permission',
//    'permission' => 'VIEW-FOLDER'
]);

Route::put('folders/move/{folder}', [
    'uses'       => 'FolderController@move',
    'as'         => 'folders.move',
    //    'permission' => 'UPDATE-FOLDER'
]);

// share link
Route::put('folders/share/{id}', [
    'uses'       => 'FolderController@shareLink',
    'as'         => 'folders.share',
    //    'permission' => 'UPDATE-FOLDER'
]);

// Un share link
Route::put('folders/un_share/{id}', [
    'uses'       => 'FolderController@unShareLink',
    'as'         => 'folders.un_share',
    //    'permission' => 'UPDATE-FOLDER'
]);
// -------
//Route Share
Route::get('share/{view_load}', [
    'uses'       => 'FolderController@view_share',
    'as'         => 'folders.shared-view_load',
]);
//Route Search
Route::get('search', [
    'uses'       => 'FolderController@view_search',
    'as'         => 'folders.search',
]);
