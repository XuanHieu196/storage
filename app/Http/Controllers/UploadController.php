<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    //
    public function postUpload(Request $request)
    {
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();

        $uploadDir = 'upload/';
        $fullpath = $uploadDir . $fileName;
        Storage::disk()->put($fullpath, file_get_contents($file), 'public');

        $fullSrc = 'upload/' . $fileName;
        if (Storage::disk('s3')->exists($fullSrc)) {
            return Storage::disk('s3')->url($fullSrc);
        }
        return null;
        return back()->with('success', 'Image Successfully Saved');
    }

    public function getUpload()
    {
        return view('user.upload');
    }
}
