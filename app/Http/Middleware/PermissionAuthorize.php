<?php

namespace App\Http\Middleware;

use App\Models\RolePermission;
use Closure;
use Illuminate\Support\Arr;

class PermissionAuthorize
{

    /**
     * @var
     */
    protected $permissions;
    protected $router;

    /**
     * Authorize constructor.
     */
    public function __construct(\Illuminate\Routing\Route $router)
    {
        $this->router = $router;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permissions = [];
        if (!empty(auth()->user()) && !empty(auth()->user()->role_id)) {
            $currentPermissions = RolePermission::with(['permission'])
                ->where('role_id', auth()->user()->role_id)->get()->toArray();
            $permissions = Arr::pluck($currentPermissions, null, 'permission.code');
        }
        //$action = $this->getRequiredRoleForRoute($request->route());
        $action = Arr::get($this->router->getAction(), 'permission', null);

        if (!$action) {
            return $next($request);
            //throw new AccessDeniedHttpException('Permission denied!');
        }
        if (empty($permissions[$action])) {
            session()->flash('app_message', __('messages.access_permission'));
            return redirect()->route('folders.index');
        }
        return $next($request);
    }

}
