<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyApi
{

    protected $token;
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */

    /**
     * Authorize constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->token = $this->parseAuthHeader($request, 'authorization', 'storage');
    }

    public function handle($request, Closure $next)
    {
        $token = $this->parseAuthHeader($request, 'authorization', 'storage');
        if (!empty($token)) {
            if ($token !== env('ACHECKIN_TOKEN')) {
                return response()->json(['error' => ['messages' =>__('unauthorized')]], 401);
            }
        } else {
            return response()->json(['error' => ['messages' =>__('invalid_token')]], 401);
        }
        return $next($request);
    }

    public function parseAuthHeader($request, $header, $method)
    {
        $header = $request->header($header);

        if (!starts_with(strtolower($header), $method)) {
            return false;
        }

        return trim(str_ireplace($method, '', $header));
    }
}
