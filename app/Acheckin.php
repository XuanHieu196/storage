<?php

namespace App;

use App\Models\DivisionDetail;
use App\Models\File;
use App\Models\Folder;
use App\Models\User;
use App\Web\CMS\Models\FolderModel;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class Acheckin
{
    private static $_data;

    public static final function info()
    {
        self::__getUserInfo();

        return self::$_data;
    }


    private static final function __getUserInfo()
    {
        try {
            $user = User::where('email', auth()->user()['email'])->first();

            if (empty($user)) {
                return response()->json([
                    'message'     => "The user is not existed",
                    'status_code' => Response::HTTP_UNAUTHORIZED,
                ], Response::HTTP_UNAUTHORIZED);
            }
            self::$_data = [
                'id'    => $user->id,
                'email' => $user->email,
                'name'  => $user->name,
                'phone' => $user->name,
            ];
        } catch (\Exception $exception) {
            return response()->json([
                'message'     => $exception->getMessage(),
                'status_code' => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    //------------------static function-------------------------------


    public static final function isActive()
    {
        $userInfo = self::info();

        return $userInfo['active'] == 1 ? true : false;
    }

    public static final function getCurrentUserId()
    {
        $userInfo = self::info();

        return $userInfo['id'];
    }


    public static final function urlBase($url = null)
    {
        $base = env("APP_URL");
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $base = $_SERVER['HTTP_REFERER'];
        } elseif (!empty($_SERVER['HTTP_ORIGIN'])) {
            $base = $_SERVER['HTTP_ORIGIN'];
        }
        $base = $base . $url;
        $base = str_replace(" ", "", $base);
        $base = str_replace("\\", "/", $base);
        $base = str_replace("//", "/", $base);
        $base = str_replace(":/", "://", $base);

        return $base;
    }

    public static final function strToSlug($str)
    {
        // replace non letter or digits by -
        $str = preg_replace('~[^\pL\d]+~u', '-', $str);

        // transliterate
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);

        // remove unwanted characters
        $str = preg_replace('~[^-\w]+~', '', $str);

        // trim
        $str = trim($str, '-');

        // remove duplicate -
        $str = preg_replace('~-+~', '-', $str);

        // lowercase
        $str = strtolower($str);

        if (empty($str)) {
            return 'n-a';
        }

        return $str;
    }

    public static final function array_get($array, $key, $default = null)
    {
        if (!Arr::accessible($array)) {
            return value($default);
        }

        if (is_null($key)) {
            return $array;
        }

        if (Arr::exists($array, $key)) {
            if ($array[$key] === "" || $array[$key] === null) {
                return $default;
            }
            return $array[$key];
        }

        foreach (explode('.', $key) as $segment) {
            if (Arr::accessible($array) && Arr::exists($array, $segment)) {
                $array = $array[$segment];
            } else {
                return value($default);
            }
        }

        return $array;
    }

    /**
     * @param string $date
     * @param string $format
     * @return false|string
     */
    public static final function dateFEtoBE(string $date, string $format = "d/m/Y H:i")
    {
        $date = trim($date);

        if (empty($date)) {
            return "";
        }
        $dateTime = explode(" ", $date);

        if (count($dateTime) != 2) {
            return " ";
        }

        $date = $dateTime[0];
        $time = $dateTime[1] . ":00";

        $days = explode("/", $date);
        if (count($days) != 3) {
            return "";
        }

        return date("Y-m-d H:i:s", strtotime("{$days[2]}-{$days[1]}-{$days[0]} $time"));
    }

    public static final function divisionDetails()
    {
        return DivisionDetail::where('user_id', auth()->id())->get();
    }

    public static final function divisionDetailIds()
    {
        $ids = [];
        $divisionDetails = DivisionDetail::where('user_id', auth()->id())->get();
        foreach ($divisionDetails as $divisionDetail) {
            array_push($ids, $divisionDetail->division_id);
        }
        return $ids;
    }

    public static final function getSize(&$size = 0)
    {
        $folders = Folder::with(['ancestors', 'files'])->whereCreatedBy(auth()->id())->get()->toTree();

        $folderModel = new FolderModel();
        foreach ($folders as $folder) {
            foreach ($folder->files as $file) {
                $size += $file->size;
            }
            foreach ($folder->children as $children) {
                $folderModel->getSize($folder, $size);
            }
        }
        return $size;
    }

    public static final function sizeTotal()
    {
        $size = 0;
        $files = File::whereCreatedBy(auth()->id())
            ->whereNull('folder_id')->get();
        foreach ($files as $file) {
            $size += $file->size;
        }

        $size += self::getSize($size);
        return $size;
    }

    public static final function sizeTotalFolder($folder_id)
    {
        $size = 0;
        $folder = Folder::with(['ancestors', 'files'])->descendantsAndSelf($folder_id)->toTree()->first();
        $folderModel = new FolderModel();

        foreach ($folder->files as $file) {
            $size += $file->size;
        }
        foreach ($folder->children as $children) {
            $folderModel->getSize($children, $size);
        }
        return $size;
    }

    public static final function divisionIds(User $user)
    {
        $ids = [];
        $divisionDetails = DivisionDetail::whereUserId($user->id)->get();
        foreach ($divisionDetails as $divisionDetail) {
            array_push($ids, $divisionDetail->division_id);
        }
        return $ids;
    }



    public static final function apiDivisionDetailIds($userId)
    {
        $ids = [];
        $divisionDetails = DivisionDetail::where('user_id', $userId)->get();
        foreach ($divisionDetails as $divisionDetail) {
            array_push($ids, $divisionDetail->division_id);
        }
        return $ids;
    }
}
