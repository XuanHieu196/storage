<?php

use App\Web\CMS\Models\FolderModel;

/**
 * Generate a random string, using a cryptographically secure
 * pseudorandom number generator (random_int)
 *
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
 *
 * @param int $length How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 * @throws Exception
 */
function random_str(
    int $length = 64,
    string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
): string
{
    if ($length < 1) {
        throw new \RangeException("Length must be a positive integer");
    }
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces [] = $keyspace[random_int(0, $max)];
    }
    return implode('', $pieces);
}

function getSubName($count = 0)
{
    if ($count != 0) {

        return $count;
    }
    return null;
}

define("DIVISION_TYPE_NAME", [
    "DEPARTMENT" => 'department',
    "PROJECT"    => 'project',
    "TEAM"       => 'team',
]);

define("DIVISION_TYPE", [
    "DEPARTMENT" => 'DEPARTMENT',
    "PROJECT"    => 'PROJECT',
    "TEAM"       => 'TEAM',
]);


if (!function_exists('is_image')) {
    /**
     * @param $base64
     *
     * @return bool
     */
    function is_image($base64)
    {
        if (empty($base64)) {
            return false;
        }

        $base = base64_decode($base64);

        if (empty($base)) {
            return false;
        }

        $file_size = strlen($base);

        if ($file_size / 1024 / 1024 > 1) {
            return false;
        }

        return true;
    }
}

function fullPath($folder)
{
    $folderModel = new FolderModel();
    $fullPath = $folderModel->getFolderPath($folder->id, $path);
    return $path;
}

if (! function_exists('starts_with')) {
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string  $haystack
     * @param  string|array  $needles
     * @return bool
     */
    function starts_with($haystack, $needles)
    {
        return \Illuminate\Support\Str::startsWith($haystack, $needles);
    }
}
function format_size($size){
    // Size
    if($size >= 1024 * 1024 * 1024){
        $size = round($size / 1024 / 1024 / 1024) . ' GB';
    }else if($size > 1024 * 1024 ){
        $size = round($size / 1024 / 1024) . ' MB';
    }
    else if($size > 1024){
        $size = round($size / 1024) . ' KB';
    }else{
        $size = $size . ' B';
    }
    return $size;
}
