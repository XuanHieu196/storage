<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\DivisionDetail;



class DivisionDetailTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["division","user","divisionpermissions",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(DivisionDetail $divisionDetail)
    {
        $data= [
			"id" => $divisionDetail->id,
			"user_id" => $divisionDetail->user_id,
			"division_id" => $divisionDetail->division_id,
			"type" => $divisionDetail->type,
			"description" => $divisionDetail->description,
			"start_time" => $divisionDetail->start_time,
			"end_time" => $divisionDetail->end_time,
			"is_active" => $divisionDetail->is_active,
			"created_at" => $divisionDetail->created_at,
			"created_by" => $divisionDetail->created_by,
			"updated_at" => $divisionDetail->updated_at,
			"updated_by" => $divisionDetail->updated_by,
			"deleted_at" => $divisionDetail->deleted_at,
			"deleted_by" => $divisionDetail->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include division
     * @param DivisionDetail $divisionDetail
     * @return \League\Fractal\Resource\item;
     */
    public function includeDivision(DivisionDetail $divisionDetail, ParamBag $paramBag = null)
    {
        return $this->item($divisionDetail->division, new DivisionTransformer($paramBag->get('fields')));
    }
    /**
     * Include user
     * @param DivisionDetail $divisionDetail
     * @return \League\Fractal\Resource\item;
     */
    public function includeUser(DivisionDetail $divisionDetail, ParamBag $paramBag = null)
    {
        return $this->item($divisionDetail->user, new UserTransformer($paramBag->get('fields')));
    }
    /**
     * Include divisionPermissions
     * @param DivisionDetail $divisionDetail
     * @return \League\Fractal\Resource\collection;
     */
    public function includeDivisionPermissions(DivisionDetail $divisionDetail, ParamBag $paramBag = null)
    {
        return $this->collection($divisionDetail->divisionPermissions, new DivisionPermissionTransformer($paramBag->get('fields')));
    }
}
