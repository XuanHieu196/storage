<?php

namespace App\Web\CMS\Transformers;

use App\Web\CMS\Models\FolderModel;
use League\Fractal\ParamBag;
use App\Models\MediaShare;


class MediaShareTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page', 'fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["user", "folder", "file", "division",];

    /**
     * @var array
     */
    protected $defaultIncludes = [];


    public function transform(MediaShare $mediaShare)
    {
        $file = $mediaShare->file;
        $folder = $mediaShare->folder;
        $data = [];

        if (!empty($file)) {
            $data[] = [
                "id"         => $mediaShare->id,
                "file_id"    => $file->id,
                "name"       => $file->name,
                "path"       => $file->path,
                "full_path"  => null,
                "key_url"    => $file->key_url,
                "key"        => $file->key,
                "type"       => "FILE",
                "size"       => $file->size,
                "share"      => $file->share,
                "is_active"  => $file->is_active,
                "created_by" => $file->createdBy->email,
                "created_at" => $file->created_at,
                "updated_at" => $file->updated_at,
            ];
        }

        if (!empty($folder)) {

            $data[] = [
                "id"         => $mediaShare->id,
                "folder_id"  => $folder->id,
                "name"       => $folder->name,
                "path"       => $folder->path,
                "full_path"  => fullPath($folder),
                "key_url"    => $folder->key_url,
                "key"        => $folder->key,
                "type"       => "FOLDER",
                "size"       => $folder->size,
                "share"      => $folder->share,
                "is_active"  => $folder->is_active,
                "created_by" => $folder->createdBy->email,
                "created_at" => $folder->created_at,
                "updated_at" => $folder->updated_at,
            ];
        }

        return $this->filterFields($data);
    }


    /**
     * Include user
     * @param MediaShare $mediaShare
     * @return \League\Fractal\Resource\item;
     */
    public function includeUser(MediaShare $mediaShare, ParamBag $paramBag = null)
    {
        return $this->item($mediaShare->user, new UserTransformer($paramBag->get('fields')));
    }

    /**
     * Include folder
     * @param MediaShare $mediaShare
     * @return \League\Fractal\Resource\item;
     */
    public function includeFolder(MediaShare $mediaShare, ParamBag $paramBag = null)
    {
        return $this->item($mediaShare->folder, new FolderTransformer($paramBag->get('fields')));
    }

    /**
     * Include file
     * @param MediaShare $mediaShare
     * @return \League\Fractal\Resource\item;
     */
    public function includeFile(MediaShare $mediaShare, ParamBag $paramBag = null)
    {
        return $this->item($mediaShare->file, new FileTransformer($paramBag->get('fields')));
    }

    /**
     * Include division
     * @param MediaShare $mediaShare
     * @return \League\Fractal\Resource\item;
     */
    public function includeDivision(MediaShare $mediaShare, ParamBag $paramBag = null)
    {
        return $this->item($mediaShare->division, new DivisionTransformer($paramBag->get('fields')));
    }
}
