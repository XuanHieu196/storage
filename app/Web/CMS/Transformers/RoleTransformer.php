<?php
namespace App\Web\CMS\Transformers;
use League\Fractal\ParamBag;
use App\Models\Role;



class RoleTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["rolepermissions","users",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(Role $role)
    {
        $data= [
			"id" => $role->id,
			"code" => $role->code,
			"name" => $role->name,
			"description" => $role->description,
			"is_active" => $role->is_active,
			"level" => $role->level,
			"created_at" => $role->created_at,
			"created_by" => $role->created_by,
			"updated_at" => $role->updated_at,
			"updated_by" => $role->updated_by,
			"deleted_at" => $role->deleted_at,
			"deleted_by" => $role->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include rolePermissions
     * @param Role $role
     * @return \League\Fractal\Resource\collection;
     */
    public function includeRolePermissions(Role $role, ParamBag $paramBag = null)
    {
        return $this->collection($role->rolePermissions, new RolePermissionTransformer($paramBag->get('fields')));
    }
    /**
     * Include users
     * @param Role $role
     * @return \League\Fractal\Resource\collection;
     */
    public function includeUsers(Role $role, ParamBag $paramBag = null)
    {
        return $this->collection($role->users, new UserTransformer($paramBag->get('fields')));
    }
}
