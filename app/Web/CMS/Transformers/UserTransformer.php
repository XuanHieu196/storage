<?php
namespace App\Web\CMS\Transformers;

use App\Acheckin;
use League\Fractal\ParamBag;
use App\Models\User;



class UserTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["role","divisiondetails","mediashares","profiles",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(User $user)
    {
        $data= [
            'id'          => $user->id,
            'code'        => $user->code,
            'type'        => $user->type,
            "username" => $user->username,
            "note" => $user->note,
            "sizeTotal" => Acheckin::sizeTotal(),
            'role_code'  => object_get($user, "role.code", null),
            'first_name'  => object_get($user, "profile.first_name", null),
            'last_name'   => object_get($user, "profile.last_name", null),
            'short_name'  => object_get($user, "profile.short_name", null),
            'full_name'   => object_get($user, "profile.full_name", null),
            'address'     => object_get($user, "profile.address", null),
            'birthday'    => object_get($user, 'profile.birthday', null),
            'genre'       => object_get($user, "profile.genre", "O"),
            'avatar'      => $avatar,
            'is_active'   => $user->is_active,
            'created_at'  => date('d/m/Y H:i', strtotime($user->created_at)),
            'updated_at'  => date('d/m/Y H:i', strtotime($user->updated_at)),

        ];
        return $this->filterFields($data);

    }


    /**
     * Include role
     * @param User $user
     * @return \League\Fractal\Resource\item;
     */
    public function includeRole(User $user, ParamBag $paramBag = null)
    {
        return $this->item($user->role, new RoleTransformer($paramBag->get('fields')));
    }
    /**
     * Include divisionDetails
     * @param User $user
     * @return \League\Fractal\Resource\collection;
     */
    public function includeDivisionDetails(User $user, ParamBag $paramBag = null)
    {
        return $this->collection($user->divisionDetails, new DivisionDetailTransformer($paramBag->get('fields')));
    }
    /**
     * Include mediaShares
     * @param User $user
     * @return \League\Fractal\Resource\collection;
     */
    public function includeMediaShares(User $user, ParamBag $paramBag = null)
    {
        return $this->collection($user->mediaShares, new MediaShareTransformer($paramBag->get('fields')));
    }
    /**
     * Include profiles
     * @param User $user
     * @return \League\Fractal\Resource\collection;
     */
    public function includeProfiles(User $user, ParamBag $paramBag = null)
    {
        return $this->collection($user->profiles, new ProfileTransformer($paramBag->get('fields')));
    }


}
