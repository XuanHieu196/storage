<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\Profile;



class ProfileTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["user",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(Profile $profile)
    {
        $data= [
			"id" => $profile->id,
			"user_id" => $profile->user_id,
			"email" => $profile->email,
			"first_name" => $profile->first_name,
			"last_name" => $profile->last_name,
			"full_name" => $profile->full_name,
			"address" => $profile->address,
			"phone" => $profile->phone,
			"birthday" => $profile->birthday,
			"genre" => $profile->genre,
			"avatar" => $profile->avatar,
			"is_active" => $profile->is_active,
			"created_at" => $profile->created_at,
			"created_by" => $profile->created_by,
			"updated_at" => $profile->updated_at,
			"updated_by" => $profile->updated_by,
			"deleted_at" => $profile->deleted_at,
			"deleted_by" => $profile->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include user
     * @param Profile $profile
     * @return \League\Fractal\Resource\item;
     */
    public function includeUser(Profile $profile, ParamBag $paramBag = null)
    {
        return $this->item($profile->user, new UserTransformer($paramBag->get('fields')));
    }
}
