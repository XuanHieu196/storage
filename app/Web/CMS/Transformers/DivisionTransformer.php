<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\Division;


class DivisionTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["divisiondetails","mediashares",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(Division $division)
    {
        $data= [
			"id" => $division->id,
			"code" => $division->code,
			"name" => $division->name,
			"description" => $division->description,
			"type" => $division->type,
			"is_active" => $division->is_active,
            "created_at" => $division->created_at,
            "created_by" => $division->created_by,
            "updated_at" => $division->updated_at,
            "updated_by" => $division->updated_by,
        ];
        return $this->filterFields($data);

    }


    /**
     * Include divisionDetails
     * @param Division $division
     * @return \League\Fractal\Resource\collection;
     */
    public function includeDivisionDetails(Division $division, ParamBag $paramBag = null)
    {
        return $this->collection($division->divisionDetails, new DivisionDetailTransformer($paramBag->get('fields')));
    }
    /**
     * Include mediaShares
     * @param Division $division
     * @return \League\Fractal\Resource\collection;
     */
    public function includeMediaShares(Division $division, ParamBag $paramBag = null)
    {
        return $this->collection($division->mediaShares, new MediaShareTransformer($paramBag->get('fields')));
    }
}
