<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\DivisionPermission;



class DivisionPermissionTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["permission","detail",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(DivisionPermission $divisionPermission)
    {
        $data= [
			"id" => $divisionPermission->id,
			"detail_id" => $divisionPermission->detail_id,
			"permission_id" => $divisionPermission->permission_id,
			"description" => $divisionPermission->description,
			"is_active" => $divisionPermission->is_active,
			"created_at" => $divisionPermission->created_at,
			"created_by" => $divisionPermission->created_by,
			"updated_at" => $divisionPermission->updated_at,
			"updated_by" => $divisionPermission->updated_by,
			"deleted_at" => $divisionPermission->deleted_at,
			"deleted_by" => $divisionPermission->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include permission
     * @param DivisionPermission $divisionPermission
     * @return \League\Fractal\Resource\item;
     */
    public function includePermission(DivisionPermission $divisionPermission, ParamBag $paramBag = null)
    {
        return $this->item($divisionPermission->permission, new PermissionTransformer($paramBag->get('fields')));
    }
    /**
     * Include detail
     * @param DivisionPermission $divisionPermission
     * @return \League\Fractal\Resource\item;
     */
    public function includeDetail(DivisionPermission $divisionPermission, ParamBag $paramBag = null)
    {
        return $this->item($divisionPermission->detail, new DivisionDetailTransformer($paramBag->get('fields')));
    }
}
