<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\RolePermission;



class RolePermissionTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["permission","role",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(RolePermission $rolePermission)
    {
        $data= [
			"id" => $rolePermission->id,
			"role_id" => $rolePermission->role_id,
			"permission_id" => $rolePermission->permission_id,
			"description" => $rolePermission->description,
			"is_active" => $rolePermission->is_active,
			"created_at" => $rolePermission->created_at,
			"created_by" => $rolePermission->created_by,
			"updated_at" => $rolePermission->updated_at,
			"updated_by" => $rolePermission->updated_by,
			"deleted_at" => $rolePermission->deleted_at,
			"deleted_by" => $rolePermission->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include permission
     * @param RolePermission $rolePermission
     * @return \League\Fractal\Resource\item;
     */
    public function includePermission(RolePermission $rolePermission, ParamBag $paramBag = null)
    {
        return $this->item($rolePermission->permission, new PermissionTransformer($paramBag->get('fields')));
    }
    /**
     * Include role
     * @param RolePermission $rolePermission
     * @return \League\Fractal\Resource\item;
     */
    public function includeRole(RolePermission $rolePermission, ParamBag $paramBag = null)
    {
        return $this->item($rolePermission->role, new RoleTransformer($paramBag->get('fields')));
    }
}
