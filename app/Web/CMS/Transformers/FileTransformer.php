<?php

namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\File;


class FileTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page', 'fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["folder", "mediashares",];

    /**
     * @var array
     */
    protected $defaultIncludes = [];


    public function transform(File $file)
    {
        $data = [
            "id"         => $file->id,
            "code"       => $file->code,
            "name"       => $file->name,
            "key_url"    => $file->key_url,
            "title"      => $file->title,
            "key"        => $file->key,
            "folder_id"  => $file->folder_id,
            "extension"  => $file->extension,
            "version"    => $file->version,
            "size"       => $file->size,
            "is_active"  => $file->is_active,
            "created_at" => $file->created_at,
            "created_by" => $file->createdBy->email,
            "updated_at" => $file->updated_at,
            "updated_by" => $file->updated_by,
            "deleted_at" => $file->deleted_at,
            "deleted_by" => $file->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include folder
     * @param File $file
     * @return \League\Fractal\Resource\item;
     */
    public function includeFolder(File $file, ParamBag $paramBag = null)
    {
        return $this->item($file->folder, new FolderTransformer($paramBag->get('fields')));
    }

    /**
     * Include mediaShares
     * @param File $file
     * @return \League\Fractal\Resource\collection;
     */
    public function includeMediaShares(File $file, ParamBag $paramBag = null)
    {
        return $this->collection($file->mediaShares, new MediaShareTransformer($paramBag->get('fields')));
    }
}
