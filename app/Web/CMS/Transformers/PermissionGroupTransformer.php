<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\PermissionGroup;



class PermissionGroupTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["permissions",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(PermissionGroup $permissionGroup)
    {
        $data= [
			"id" => $permissionGroup->id,
			"name" => $permissionGroup->name,
			"code" => $permissionGroup->code,
			"description" => $permissionGroup->description,
			"is_active" => $permissionGroup->is_active,
			"created_at" => $permissionGroup->created_at,
			"created_by" => $permissionGroup->created_by,
			"updated_at" => $permissionGroup->updated_at,
			"updated_by" => $permissionGroup->updated_by,
			"deleted_at" => $permissionGroup->deleted_at,
			"deleted_by" => $permissionGroup->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include permissions
     * @param PermissionGroup $permissionGroup
     * @return \League\Fractal\Resource\collection;
     */
    public function includePermissions(PermissionGroup $permissionGroup, ParamBag $paramBag = null)
    {
        return $this->collection($permissionGroup->permissions, new PermissionTransformer($paramBag->get('fields')));
    }
}
