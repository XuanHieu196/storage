<?php
namespace App\Web\CMS\Transformers;

use League\Fractal\ParamBag;
use App\Models\Permission;



class PermissionTransformer extends TransformerAbstract
{
     /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page','fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["group","divisionpermissions","rolepermissions",];

     /**
      * @var array
      */
    protected $defaultIncludes = [];


    public function transform(Permission $permission)
    {
        $data= [
			"id" => $permission->id,
			"name" => $permission->name,
			"code" => $permission->code,
			"type" => $permission->type,
			"description" => $permission->description,
			"group_id" => $permission->group_id,
			"is_active" => $permission->is_active,
			"created_at" => $permission->created_at,
			"created_by" => $permission->created_by,
			"updated_at" => $permission->updated_at,
			"updated_by" => $permission->updated_by,
			"deleted_at" => $permission->deleted_at,
			"deleted_by" => $permission->deleted_by,

        ];
        return $this->filterFields($data);

    }


    /**
     * Include group
     * @param Permission $permission
     * @return \League\Fractal\Resource\item;
     */
    public function includeGroup(Permission $permission, ParamBag $paramBag = null)
    {
        return $this->item($permission->group, new PermissionGroupTransformer($paramBag->get('fields')));
    }
    /**
     * Include divisionPermissions
     * @param Permission $permission
     * @return \League\Fractal\Resource\collection;
     */
    public function includeDivisionPermissions(Permission $permission, ParamBag $paramBag = null)
    {
        return $this->collection($permission->divisionPermissions, new DivisionPermissionTransformer($paramBag->get('fields')));
    }
    /**
     * Include rolePermissions
     * @param Permission $permission
     * @return \League\Fractal\Resource\collection;
     */
    public function includeRolePermissions(Permission $permission, ParamBag $paramBag = null)
    {
        return $this->collection($permission->rolePermissions, new RolePermissionTransformer($paramBag->get('fields')));
    }
}
