<?php

namespace App\Web\CMS\Transformers;

use App\Acheckin;
use App\Web\CMS\Models\FolderModel;
use App\Models\Folder;


class FolderTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    private $validParams = ['q', 'limit', 'page', 'fields'];

    /**
     * @var array
     */
    protected $availableIncludes = ["parent", "files", "folders", "mediashares",];

    /**
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Folder $folder)
    {
        $listFile = [];
        $children = [];
        $files = $folder->files;
        foreach ($files as $item) {
            $listFile[] = [
                "id"         => $item->id,
                "code"       => $item->code,
                "name"       => $item->name,
                "key_url"    => $item->key_url,
                "title"      => $item->title,
                "key"        => $item->key,
                "folder_id"  => $item->folder_id,
                "extension"  => $item->extension,
                "version"    => $item->version,
                "size"       => $item->size,
                "share"      => $item->share,
                "is_active"  => $item->is_active,
                "created_by" => $item->createdBy->email,
                "created_at" => $item->created_at,
                "updated_at" => $item->updated_at,
            ];
        }

        //        foreach ($folder->children as $item) {
        //            $children[] = [
        //                "id"         => $item->id,
        //                "name"       => $item->name,
        //                "path"       => $item->path,
        //                "full_path"  => fullPath($item),
        //                "key_url"    => $item->key_url,
        //                "parent_id"  => $item->parent_id,
        //                "key"        => $item->key,
        //                "size"       => $item->size,
        //                //                "children"   => $item->children,
        //                //                "files"      => $item->files,
        //                //                "parent"     => $item->parent,
        //                "share"     => $item->share,
        //                "is_active"  => $item->is_active,
        //                "created_by" => $item->createdBy->email,
        //                "created_at" => $item->created_at,
        //                "updated_at" => $item->updated_at,
        //            ];
        //        }
        $data = [
            "id"         => $folder->id,
            "name"       => $folder->name,
            "path"       => $folder->path,
            "full_path"  => fullPath($folder),
            "key_url"    => $folder->key_url,
            "parent_id"  => $folder->parent_id,
            "key"        => $folder->key,
            "size"       => $folder->size,
            "children"   => $folder->children,
            "files"      => $listFile,
            "parent"     => $folder->parent,
            "share"      => $folder->share,
            "is_active"  => $folder->is_active,
            "created_by" => $folder->createdBy->email,
            "created_at" => $folder->created_at,
            "updated_at" => $folder->updated_at,
        ];
        return $data;
    }
}
