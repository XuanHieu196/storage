<?php

namespace App\Web\CMS\Controllers;

use App\Log;
use App\Models\Division;
use App\Models\MediaShare;
use App\Models\User;
use App\Web\CMS\Models\FolderModel;
use App\Models\Folder;
use App\Web\CMS\Requests\Folders\DriveApi;
use App\Web\CMS\Requests\Folders\Index;
use App\Web\CMS\Requests\Folders\Store;
use App\Web\CMS\Requests\Folders\Update;
use App\Web\CMS\Requests\Folders\Destroy;
use App\Web\CMS\Transformers\FolderTransformer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use View;

class FolderController extends BaseController
{
    /**
     * @var FolderModel
     */
    protected $model;

    /**
     * FolderController constructor.
     */
    public function __construct()
    {
        $this->model = new FolderModel();
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $googleDriveStorage = Storage::disk('google_drive');
        $recursive = false;
        $dir = '/';

        $contents = collect($googleDriveStorage->listContents($dir, $recursive));

        dd($contents);
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return view('view-user.page.list.index', ['list_user' => $list_user, 'list_division' => $list_division]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return \Dingo\Api\Http\Response
     */
    public function list(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 999);
        $folders = $this->model->search($input, ['ancestors', 'files'], $limit);
        return fractal($folders, new FolderTransformer);
    }

    public function list_view(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 999);
        $folders = $this->model->search($input, ['ancestors', 'files'], $limit);
        $folders = fractal($folders, new FolderTransformer);
        return View::make('view-user.page.folder.list-index-make', ['list' => $folders]);
    }

    public function list_tree(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 999);
        $folders = $this->model->search($input, ['ancestors', 'files'], $limit);
        $data = $folders->toArray()['data'];
        $categories = [];
        if (!empty($data)) {
            foreach ($data as $value) {
                $categories[] = ['id' => $value['id'], 'parent_id' => $value['parent_id'], 'name' => $value['name']];
            }
        }
        echo "<option value=''>--Select Folder--</option>";
        echo "<option value='null'>Root Folder</option>";
        $this->showCategories($categories);
    }

    public function showCategories($categories, $parent_id = 0, $char = '')
    {
        foreach ($categories as $key => $item) {
            if ($item['parent_id'] == $parent_id) {
                echo "<option value='" . $item['id'] . "'>" . $char . $item['name'] . "</option>";
                unset($categories[$key]);
                $this->showCategories($categories, $item['id'], $char . '|---');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return array
     */
    public function detail($id)
    {
        try {
            $folder = $this->model->detail($id);
            Log::view($this->model->getTable(), $folder->name);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => __('messages.view_failed', ['name' => __('tables.folders')])]], 500);
        }

        return fractal($folder, new FolderTransformer);
    }

    public function detail_view(Folder $folder)
    {
        try {
            $folder = $this->model->detail($folder->id);
            Log::view($this->model->getTable(), $folder->name);
        } catch (Exception $exception) {
            return response()->json(['messages' => __('messages.view_failed', ['name' => __('tables.folders')])], 500);
        }

        $result = fractal($folder, new FolderTransformer);
        $data_file = $result->toArray()['data']['files'];
        // dd($result);die;
        return View::make('view-user.page.folder.list-detail-make', ['list' => $result, 'data_file' => $data_file]);


    }

    public function detail_index($id)
    {
        $breadcrumb = [];
        $folder = Folder::where('id', $id)->first();
        $folder_name = $folder->name;
        $parent_id = $folder->parent_id;
        $folder_id = $folder->id;
        $breadcrumb[] = ['id' => $folder_id, 'name' => $folder_name];
        if ($parent_id > 0) {
            while ($parent_id !== NULL) {
                $folder_parent = Folder::where('id', $parent_id)->first();
                $folder_parent_name = $folder_parent->name;
                $folder_parent_id = $folder_parent->id;
                $breadcrumb[] = ['id' => $folder_parent_id, 'name' => $folder_parent_name];
                $parent_id = $folder_parent->parent_id;
            }
        }
        $breadcrumb = array_reverse($breadcrumb, true);
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return view('view-user.page.list.detail', ['id' => $id, 'breadcrumb' => $breadcrumb, 'list_user' => $list_user, 'list_division' => $list_division]);
    }

    /**
     * Display the specified resource.
     *
     * @param Folder $folder
     * @return Response
     */
    public function show(Folder $folder)
    {
        $folder = $this->model->detail($folder->id);
        $folder = fractal($folder, new FolderTransformer);
        Log::view($this->model->getTable(), $folder->name);
        return view('pages.folders.show', [
            'record' => $folder,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $folders = Folder::all(['id']);

        return view('pages.folders.create', [
            'model'   => new Folder,
            "folders" => $folders,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        try {
            DB::beginTransaction();
            $folder = $this->model->store($request->all());
            Log::create($this->model->getTable(), $folder->name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.store_failed', ['name' => __('tables.folders')])]], 500);
        }

        return response()->json(['messages' => __('messages.store_success', ['name' => __('tables.folders')])], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Folder $folder
     * @return Response
     */
    public function edit(Folder $folder)
    {
        $folders = Folder::all(['id']);
        Log::view($this->model->getTable(), $folder->name);
        return view('pages.folders.edit', [
            'model'   => $folder,
            "folders" => $folders,
        ]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param Folder $folder
     * @return Response
     * @throws Exception
     */
    public function update(Update $request, Folder $folder)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $folder->id;
            $folder = $this->model->update($input);
            Log::update($this->model->getTable(), $folder->name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.update_failed', ['name' => __('tables.folders')])]], 500);
        }

        return response()->json(['messages' => __('messages.update_success', ['name' => __('tables.folders')])], 200);
    }

    /**
     * Delete a  resource from  storage.
     *
     * @param Folder $folder
     * @return Response
     */
    public function destroy(Folder $folder)
    {
        try {
            DB::beginTransaction();
            $folder = $this->model->checkPermission($folder->id);
            $this->model->delete($folder);
            Log::delete($this->model->getTable(), $folder->name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.delete_failed', ['name' => __('tables.folders')])]], 500);
        }

        return response()->json(['messages' => __('messages.delete_success', ['name' => __('tables.folders')])], 200);
    }

    /**
     * Download a  resource from  storage.
     *
     * @param $id
     * @return Response
     */
    public function download($id)
    {
        try {
            return $this->model->download($id);
            //            $folder = Folder::find($id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        //        Storage::disk('local')->delete($folder->path . '.zip');
        //        return response()->json(['messages' => __('messages.download_success')], 500);
    }

    /**
     * Download a  resource from  storage.
     *
     * @param Destroy $request
     * @param Folder $folder
     * @return Response
     */
    public function copy(Destroy $request, Folder $folder)
    {
        $folderTo = Folder::find($request->input('folderToId', 0));
        try {
            DB::beginTransaction();
            $this->model->copy($folder, "COPY", $folderTo);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['messages' => __('messages.copy_success')], 200);
    }

    /**
     *
     *
     * @param $id
     * @return Response
     */
    public function unShare($id)
    {
        try {
            DB::beginTransaction();
            $folder = Folder::find($id);
            if (empty($folder) || $folder->created_by != auth()->id()) {
                return response()->json(['error' => ['messages' => __('messages.permission')]], 500);
            }
            MediaShare::whereFolderId($id)->delete();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.app_error')]], 500);
        }

        return response()->json(['messages' => __('messages.unshare_success')], 200);
    }

    /**
     * sharePermission
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function sharePermission($id)
    {

        $folder = $this->model->checkPermission($id);
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return View::make('view-user.page.folder.form-edit-folder', [
            'data'          => $folder->drive_share,
            'list_user'     => $list_user,
            'list_division' => $list_division
        ]);
    }

    /**
     * Move a  resource from  storage.
     *
     * @param Destroy $request
     * @param Folder $folder
     * @return Response
     */
    public function move(Destroy $request, Folder $folder)
    {
        $input = $request->all();
        $folderToId = Arr::get($input, 'folderToId', 0);
        try {
            DB::beginTransaction();
            $this->model->move($folder, $folderToId);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['messages' => __('messages.move_success')], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function shareLink(Request $request,$id)
    {
        try {
            $input = $request->all();
            $folder = $this->model->shareLink($input, $id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['data' => $folder], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function unShareLink(Request $request,$id)
    {
        try {
            $input = $request->all();
            $folder = $this->model->unShareLink($input, $id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['data' => $folder], 200);
    }

    // --------------- API ---------------

    /**
     * @param DriveApi $request
     * @return JsonResponse
     */
    public function listApi(DriveApi $request)
    {
        try {
            $input = $request->all();
            $data = $this->model->listApi($input);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function detailApi(DriveApi $request, $id)
    {
        try {
            $input = $request->all();
            $data = $this->model->detailApi($input, $id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['data' => $data], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return \Dingo\Api\Http\Response
     */
    public function load(Index $request)
    {
        try {
            $input = $request->all();
            $folders = $this->model->load($input);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['data' => $folders], 200);
    }
    public function load_view_share(Index $request)
    {
        try {
            $input = $request->all();
            $folders = $this->model->load($input);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return View::make('view-user.page.share.list-share-make',['data' => $folders]);
    }
    public function view_share($view_load){
        return view('view-user.page.list.share',['view_load'=>$view_load]);
    }
    public function load_view_search(Index $request)
    {
        try {
            $input = $request->all();
            $folders = $this->model->load($input);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return View::make('view-user.page.search.list-search-make',['data' => $folders]);
    }
    public function view_search(){
        return view('view-user.page.list.search');
    }
}
