<?php

namespace App\Web\CMS\Controllers;

use App\Log;
use App\Models\DivisionDetail;
use App\Models\File;
use App\Models\Folder;
use App\Models\MediaShare;
use App\Web\CMS\Models\UserModel;
use App\Web\CMS\Requests\Users\CreateApi;
use App\Web\CMS\Requests\Users\UpdateApi;
use App\Web\CMS\Transformers\UserTransformer;
use Exception;
use App\Models\User;
use App\Models\Role;
use App\Web\CMS\Requests\Users\Index;
use App\Web\CMS\Requests\Users\Show;
use App\Web\CMS\Requests\Users\Create;
use App\Web\CMS\Requests\Users\Store;
use App\Web\CMS\Requests\Users\Edit;
use App\Web\CMS\Requests\Users\Update;
use App\Web\CMS\Requests\Users\Destroy;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use View;


class UserController extends BaseController
{
    /**
     * @var UserModel
     */
    protected $model;


    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return Response
     */
    public function index(Index $request)
    {
        return view('view-user.page.user.index');
    }

    public function list_view(Index $request)
    {
        return View::make('view-user.page.user.list-make', ['list' => User::with('profiles')->get()->toArray()]);
    }

    /**
     * Display the specified resource.
     *
     * @param Show $request
     * @param User $user
     * @return Response
     */
    public function show(Show $request, User $user)
    {
        return view('pages.users.show', [
            'record' => $user,
        ]);

    }

    public function reload_form_create()
    {
        return View::make('view-user.page.user.form-create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Create $request
     * @return Response
     */
    public function create(Create $request)
    {
        $roles = Role::all(['id']);

        return view('pages.users.create', [
            'model' => new User,
            "roles" => $roles,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $user = $this->model->upsert($input);
            Log::view($this->model->getTable(), $user->email);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Edit $request
     * @param User $user
     * @return Response
     */
    public function edit(Edit $request, User $user)
    {
        $roles = Role::all(['id']);

        return view('pages.users.edit', [
            'model' => $user,
            "roles" => $roles,

        ]);
    }

    public function edit_view(Edit $request, User $user)
    {
        return View::make('view-user.page.user.edit-make', ['model' => $user]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param User $user
     * @return Response
     */
    public function update(Update $request, User $user)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $user->id;
            $user = $this->model->upsert($input);
            Log::view($this->model->getTable(), $user->email);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return $user;
    }

    /**
     * Delete a  resource from  storage.
     *
     * @param User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        try {

            if (!in_array(auth()->user()->type, User::typeAdmin())) {
                return response()->json(['error' => ['messages' => __('messages.permission', ['name' => __('tables.users')])]], 500);
            }

            if ($user->type == User::TYPE_SUPER_ADMIN && auth()->user()->type != User::TYPE_SUPER_ADMIN) {
                return response()->json(['error' => ['messages' => __('messages.permission', ['name' => __('tables.users')])]], 500);
            }

            $name = $user->profiles->full_name;
            MediaShare::model()->delete(['created_by', $user->id]);
            Folder::model()->delete(['created_by', $user->id]);
            File::model()->delete(['created_by', $user->id]);
            File::model()->delete(['user_id', $user->id]);
            DivisionDetail::model()->delete(['user_id', $user->id]);
            $user->delete();
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => __('messages.delete_failed', ['name' => $name])]], 500);
        }

        return response()->json(['messages' => __('messages.delete_success', ['name' => $name])], 200);
    }


    // ----------------- API ----------------------

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateApi $request
     * @return Response
     */
    public function storeApi(CreateApi $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $user = $this->model->upsertApi($input);
            Log::create($this->model->getTable(), $user->email);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return fractal($user, new UserTransformer());
    }

    /**
     * Update a existing resource in storage.
     *
     * @param UpdateApi $request
     * @param $id
     * @return Response
     */
    public function updateApi(UpdateApi $request, $id)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $id;
            $user = $this->model->upsertApi($input);
            Log::update($this->model->getTable(), $user->email);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return fractal($user, new UserTransformer());
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param $id
     * @return Response
     */
    public function detailApi(Update $request, $id)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $id;
            $user = User::model()->find($id);
            if (empty($user)) {
                return response()->json(['error' => ['messages' => __('messages.not_existed', ['name' => "ID: #$id"])]], 404);
            }
            Log::view($this->model->getTable(), $user->email);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return fractal($user, new UserTransformer());
    }
}
