<?php

namespace App\Web\CMS\Controllers;

use App\Http\Controllers\Controller;
use App\Log;
use App\Models\Division;
use App\Models\DivisionDetail;
use App\Models\MediaShare;
use App\Web\CMS\Models\DivisionModel;
use App\Web\CMS\Requests\Divisions\Index;
use App\Web\CMS\Requests\Divisions\Show;
use App\Web\CMS\Requests\Divisions\Create;
use App\Web\CMS\Requests\Divisions\Store;
use App\Web\CMS\Requests\Divisions\Edit;
use App\Web\CMS\Requests\Divisions\Update;
use App\Web\CMS\Requests\Divisions\Destroy;
use App\Web\CMS\Transformers\DivisionTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use View;


class DivisionController extends BaseController
{
    /**
     * @var DivisionModel
     */
    protected $model;
    /**
     * @var Manager
     */
    private $fractal;

    /**
     * @var DivisionTransformer
     */
    private $divisionTransformer;

    /**
     * DivisionController constructor.
     * @param Manager $fractal
     * @param DivisionTransformer $divisionTransformer
     */
    public function __construct(Manager $fractal, DivisionTransformer $divisionTransformer)
    {
        $this->model = new DivisionModel();
        $this->fractal = $fractal;
        $this->divisionTransformer = $divisionTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return Response
     */
    public function index(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 10);
        $divisions = $this->model->search($input, [], $limit);

        return view('view-user.page.divisions.index', ['records' => $divisions]);
        // return view('pages.divisions.index', ['records' => $divisions]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return array
     */
    public function list(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 10);
        $divisions = $this->model->search($input, [], $limit);
        $divisions = new Collection($divisions, $this->divisionTransformer);
        $divisions = $this->fractal->createData($divisions);

        return $divisions->toArray();
    }
    public function list_view(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 999);
        $divisions = $this->model->search($input, [], $limit);
        $divisions = new Collection($divisions, $this->divisionTransformer);
        $divisions = $this->fractal->createData($divisions);

        return View::make('view-user.page.divisions.list-make',['list'=>$divisions->toArray()]);
    }

    /**
     * Display the specified resource.
     *
     * @param Show $request
     * @param Division $division
     * @return Response
     */
    public function show(Show $request, Division $division)
    {
        return view('pages.divisions.show', [
            'record' => $division,
        ]);

    }

    public function detail_list(Show $request, Division $division)
    {
        $name_division = $division->toArray()['name'];
        $id_division = $division->toArray()['id'];
        return view('view-user.page.divisions.detail.index',['name_division'=>$name_division,'id_division'=>$id_division]);

    }
    public function reload_form_create(){
        return View::make('view-user.page.divisions.form-create');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param Create $request
     * @return Response
     */
    public function create(Create $request)
    {

        return view('pages.divisions.create', [
            'model' => new Division,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $division = $this->model->upsert($input);
            Log::create($this->model->getTable(), $division->name);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage()], 500);
        }

        return response()->json(['message' => __('messages.store_success', ['name' => __('tables.divisions')])], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Edit $request
     * @param Division $division
     * @return Response
     */
    public function edit(Edit $request, Division $division)
    {

        return view('pages.divisions.edit', [
            'model' => $division,

        ]);
    }
    public function edit_view(Edit $request, Division $division)
    {
        return View::make('view-user.page.divisions.edit-make',['model' => $division]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param Division $division
     * @return Response
     */
    public function update(Update $request, Division $division)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $division->id;
            $division = $this->model->upsert($input);
            Log::update($this->model->getTable(), $division->name);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => __('messages.update_failed', ['name' => __('tables.divisions')])], 500);
        }

        return response()->json(['message' => __('messages.update_success', ['name' => __('tables.divisions')])], 200);
    }

    /**
     * Delete a  resource from  storage.
     *
     * @param Destroy $request
     * @param Division $division
     * @return Response
     * @throws Exception
     */
    public function destroy(Destroy $request, Division $division)
    {
        try {
            DB::beginTransaction();
            DivisionDetail::model()->whereDivisionId($division->id)->delete();
            MediaShare::model()->whereDivisionId($division->id)->delete();
            $division->delete();
            Log::delete($this->model->getTable(), $division->name);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => __('messages.delete_failed', ['name' => __('tables.divisions')])], 500);
        }

        return response()->json(['message' => __('messages.delete_success', ['name' => __('tables.divisions')])], 200);
    }

    public function driveShare(Request $request, $id) {
        try {
            $input = $request->all();
            $data = $this->model->driveShare($input, $id);
            Log::view($this->model->getTable(), 'get drive for division #ID ' . $id);
        } catch (\Exception $exception) {
            return response()->json(['message' => __('messages.delete_failed', ['name' => __('tables.divisions')])], 500);
        }

        return response()->json(['data' => $data], 200);
    }
}
