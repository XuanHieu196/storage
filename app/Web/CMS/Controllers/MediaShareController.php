<?php

namespace App\Web\CMS\Controllers;

use App\Log;
use App\Web\CMS\Models\MediaShareModel;
use App\Models\Division;
use App\Models\File;
use App\Models\Folder;
use App\Models\MediaShare;
use App\Models\User;
use App\Web\CMS\Requests\MediaShares\Index;
use App\Web\CMS\Requests\MediaShares\Show;
use App\Web\CMS\Requests\MediaShares\Create;
use App\Web\CMS\Requests\MediaShares\Store;
use App\Web\CMS\Requests\MediaShares\Edit;
use App\Web\CMS\Requests\MediaShares\Update;
use App\Web\CMS\Requests\MediaShares\Destroy;
use App\Web\CMS\Transformers\MediaShareTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;


class MediaShareController extends BaseController
{
    /**
     * @var MediaShareModel
     */
    protected $model;

    /**
     * MediaShareController constructor.
     */
    public function __construct()
    {
        $this->model = new MediaShareModel();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return Response
     * @throws \Exception
     */
    public function index(Index $request)
    {
        $input = $request->all();
        $mediaShares = $this->model->search($input, ['folder', 'file'], 10);
        return view('pages.media_shares.index', ['records' => MediaShare::paginate(10)]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return \Dingo\Api\Http\Response
     */
    public function list(Index $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 999);
        $shares = $this->model->search($input, ['folder', 'file'], $limit);
        return fractal($shares, new MediaShareTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param Show $request
     * @param MediaShare $mediashare
     * @return Response
     */
    public function show(Show $request, MediaShare $mediashare)
    {
        $mediashare = $this->model->detail($mediashare->id, ['folder', 'file']);
        return view('pages.media_shares.show', [
            'record' => $mediashare,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Create $request
     * @return Response
     */
    public function create(Create $request)
    {
        $divisions = Division::all(['id']);
        $files = File::all(['id']);
        $folders = Folder::all(['id']);
        $users = User::all(['id']);

        return view('pages.media_shares.create', [
            'model'     => new MediaShare,
            "divisions" => $divisions,
            "files"     => $files,
            "folders"   => $folders,
            "users"     => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        try {
            DB::beginTransaction();
            $mediaShare = $this->model->store($request->all());
            Log::update($this->model->getTable(), 'share');
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.store_failed', ['name' => __('tables.media_shares')])]], 500);
        }
        return response()->json(['messages' => __('messages.store_success', ['name' => __('tables.media_shares')])], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Edit $request
     * @param MediaShare $mediashare
     * @return Response
     */
    public function edit(Edit $request, MediaShare $mediashare)
    {
        $divisions = Division::all(['id']);
        $files = File::all(['id']);
        $folders = Folder::all(['id']);
        $users = User::all(['id']);

        return view('pages.media_shares.edit', [
            'model'     => $mediashare,
            "divisions" => $divisions,
            "files"     => $files,
            "folders"   => $folders,
            "users"     => $users,

        ]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function update(Update $request, $id)
    {
        try {
            // dd($request->all());
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $id;
            $mediaShare = $this->model->update($input);
            $name = $mediaShare->file->name ?? $mediaShare->folder->name;
            Log::update($this->model->getTable(), $name);
            DB::commit();

        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.update_failed', ['name' => __('tables.media_shares')])]], 500);
        }

        return response()->json(['messages' => __('messages.update_success', ['name' => __('tables.media_shares')])], 200);
    }

    /**
     * Delete a  resource from  storage.
     *
     * @param Destroy $request
     * @param $id
     * @return void
     * @throws \Exception
     */
    public function destroy(Destroy $request, $id)
    {
        try {
            DB::beginTransaction();
            $mediaShare = $this->model->checkPermission($id);
            $name = $mediaShare->file->name ?? $mediaShare->folder->name;
            $mediaShare->delete();
            Log::delete($this->model->getTable(), $name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.delete_failed', ['name' => __('tables.media_shares')])]], 500);
        }
        return response()->json(['messages' => __('messages.delete_success', ['name' => __('tables.media_shares')])], 200);

    }

    /**
     * Delete a  resource from  storage.
     *
     * @param Destroy $request
     * @return void
     */
    public function sync(Destroy $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            if(!isset($input['users'])){
                $input['users'] = [];
            }
            if(!isset($input['divisions'])){
                $input['divisions'] = [];
            }
            $this->model->sync($input);
            Log::delete($this->model->getTable(), "Sync media share");
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return response()->json(['messages' => __('messages.delete_success', ['name' => __('tables.media_shares')])], 200);

    }

    public function listApi(Request $request) {
        $input = $request->all();
        $mediaShares = $this->model->search($input, ['folder', 'file'], 10);
        return fractal($mediaShares, new MediaShareTransformer());
    }
}
