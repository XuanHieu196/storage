<?php

namespace App\Web\CMS\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RolePermission;
use App\Models\Role;
use App\Models\Permission;
use App\Web\CMS\Requests\RolePermissions\Index;
use App\Web\CMS\Requests\RolePermissions\Show;
use App\Web\CMS\Requests\RolePermissions\Create;
use App\Web\CMS\Requests\RolePermissions\Store;
use App\Web\CMS\Requests\RolePermissions\Edit;
use App\Web\CMS\Requests\RolePermissions\Update;
use App\Web\CMS\Requests\RolePermissions\Destroy;
use View;


class RolePermissionController extends BaseController
{
       /**
     * Display a listing of the resource.
     *
     * @param  Index  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Index $request)
    {
        dd(345);
        return view('pages.role_permissions.index', ['records' => RolePermission::paginate(10)]);
    }
    public function list_view(Index $request)
    {
        dd(999);
        return View::make('view-user.page.role_permission.list-make',['list'=>RolePermission::get()->toArray()]);
    }   
    /**
     * Display the specified resource.
     *
     * @param  Show  $request
     * @param  RolePermission  $rolepermission
     * @return \Illuminate\Http\Response
     */
    public function show(Show $request, RolePermission $rolepermission)
    {
        return view('pages.role_permissions.show', [
                'record' =>$rolepermission,
        ]);

    }    /**
     * Show the form for creating a new resource.
     *
     * @param  Create  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Create $request)
    {
		$roles = Role::all(['id']);
		$permissions = Permission::all(['id']);

        return view('pages.role_permissions.create', [
            'model' => new RolePermission,
			"roles" => $roles,
			"permissions" => $permissions,

        ]);
    }    /**
     * Store a newly created resource in storage.
     *
     * @param  Store  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $model=new RolePermission;
        $model->fill($request->all());

        if ($model->save()) {

            session()->flash('app_message', 'RolePermission saved successfully');
            return redirect()->route('role_permissions.index');
            } else {
                session()->flash('app_message', 'Something is wrong while saving RolePermission');
            }
        return redirect()->back();
    } /**
     * Show the form for editing the specified resource.
     *
     * @param  Edit  $request
     * @param  RolePermission  $rolepermission
     * @return \Illuminate\Http\Response
     */
    public function edit(Edit $request, RolePermission $rolepermission)
    {
		$roles = Role::all(['id']);
		$permissions = Permission::all(['id']);

        return view('pages.role_permissions.edit', [
            'model' => $rolepermission,
			"roles" => $roles,
			"permissions" => $permissions,

            ]);
    }    /**
     * Update a existing resource in storage.
     *
     * @param  Update  $request
     * @param  RolePermission  $rolepermission
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request,RolePermission $rolepermission)
    {
        $rolepermission->fill($request->all());

        if ($rolepermission->save()) {

            session()->flash('app_message', 'RolePermission successfully updated');
            return redirect()->route('role_permissions.index');
            } else {
                session()->flash('app_error', 'Something is wrong while updating RolePermission');
            }
        return redirect()->back();
    }    /**
     * Delete a  resource from  storage.
     *
     * @param  Destroy  $request
     * @param  RolePermission  $rolepermission
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Destroy $request, RolePermission $rolepermission)
    {
        if ($rolepermission->delete()) {
                session()->flash('app_message', 'RolePermission successfully deleted');
            } else {
                session()->flash('app_error', 'Error occurred while deleting RolePermission');
            }

        return redirect()->back();
    }
}
