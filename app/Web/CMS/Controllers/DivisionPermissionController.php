<?php

namespace App\Web\CMS\Controllers;

use Exception;
use App\Models\User;
use App\Models\Division;
use App\Models\DivisionPermission;
use App\Models\DivisionDetail;
use App\Models\Permission;
use App\Web\CMS\Requests\DivisionPermissions\Index;
use App\Web\CMS\Requests\DivisionPermissions\Show;
use App\Web\CMS\Requests\DivisionPermissions\Create;
use App\Web\CMS\Requests\DivisionPermissions\Store;
use App\Web\CMS\Requests\DivisionPermissions\Edit;
use App\Web\CMS\Requests\DivisionPermissions\Update;
use App\Web\CMS\Requests\DivisionPermissions\Destroy;
use Illuminate\Http\Response;
use View;

class DivisionPermissionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return Response
     */
    public function index(Index $request)
    {
        return view('view-user.page.division_permission.index', ['records' => DivisionPermission::paginate(10)]);
    }

    public function list_view(Index $request)
    {
        return View::make('view-user.page.division_permission.list-make', ['list' => DivisionPermission::get()->toArray()]);
    }

    /**
     * Display the specified resource.
     *
     * @param Show $request
     * @param DivisionPermission $divisionpermission
     * @return Response
     */
    public function show(Show $request, DivisionPermission $divisionpermission)
    {
        return view('pages.division_permissions.show', [
            'record' => $divisionpermission,
        ]);

    }

    public function reload_form_create(){
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return View::make('view-user.page.division_permission.form-create', ['list_user' => $list_user, 'list_division' => $list_division]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param Create $request
     * @return Response
     */
    public function create(Create $request)
    {
        $division_details = DivisionDetail::all(['id']);
        $permissions = Permission::all(['id']);

        return view('pages.division_permissions.create', [
            'model'            => new DivisionPermission,
            "division_details" => $division_details,
            "permissions"      => $permissions,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        $model = new DivisionPermission;
        $model->fill($request->all());

        if ($model->save()) {

            session()->flash('app_message', 'DivisionPermission saved successfully');
            return redirect()->route('division_permissions.index');
        } else {
            session()->flash('app_message', 'Something is wrong while saving DivisionPermission');
        }
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Edit $request
     * @param DivisionPermission $divisionpermission
     * @return Response
     */
    public function edit(Edit $request, DivisionPermission $divisionpermission)
    {
        $division_details = DivisionDetail::all(['id']);
        $permissions = Permission::all(['id']);

        return view('pages.division_permissions.edit', [
            'model'            => $divisionpermission,
            "division_details" => $division_details,
            "permissions"      => $permissions,

        ]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param DivisionPermission $divisionpermission
     * @return Response
     */
    public function update(Update $request, DivisionPermission $divisionpermission)
    {
        $divisionpermission->fill($request->all());

        if ($divisionpermission->save()) {

            session()->flash('app_message', 'DivisionPermission successfully updated');
            return redirect()->route('division_permissions.index');
        } else {
            session()->flash('app_error', 'Something is wrong while updating DivisionPermission');
        }
        return redirect()->back();
    }

    /**
     * Delete a  resource from  storage.
     *
     * @param Destroy $request
     * @param DivisionPermission $divisionpermission
     * @return Response
     * @throws Exception
     */
    public function destroy(Destroy $request, DivisionPermission $divisionpermission)
    {
        if ($divisionpermission->delete()) {
            session()->flash('app_message', 'DivisionPermission successfully deleted');
        } else {
            session()->flash('app_error', 'Error occurred while deleting DivisionPermission');
        }

        return redirect()->back();
    }
}
