<?php

namespace App\Web\CMS\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PermissionGroup;
use App\Web\CMS\Requests\PermissionGroups\Index;
use App\Web\CMS\Requests\PermissionGroups\Show;
use App\Web\CMS\Requests\PermissionGroups\Create;
use App\Web\CMS\Requests\PermissionGroups\Store;
use App\Web\CMS\Requests\PermissionGroups\Edit;
use App\Web\CMS\Requests\PermissionGroups\Update;
use App\Web\CMS\Requests\PermissionGroups\Destroy;
use View;

class PermissionGroupController extends BaseController
{
       /**
     * Display a listing of the resource.
     *
     * @param  Index  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Index $request)
    {
        return view('view-user.page.permission_group.index', ['records' => PermissionGroup::paginate(10)]);
    }
    public function list_view(Index $request)
    {
        return View::make('view-user.page.permission_group.list-make',['list'=>PermissionGroup::get()->toArray()]);
    }
    /**
     * Display the specified resource.
     *
     * @param  Show  $request
     * @param  PermissionGroup  $permissiongroup
     * @return \Illuminate\Http\Response
     */
    public function show(Show $request, PermissionGroup $permissiongroup)
    {
        return view('pages.permission_groups.show', [
                'record' =>$permissiongroup,
        ]);

    }    /**
     * Show the form for creating a new resource.
     *
     * @param  Create  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Create $request)
    {

        return view('pages.permission_groups.create', [
            'model' => new PermissionGroup,

        ]);
    }    /**
     * Store a newly created resource in storage.
     *
     * @param  Store  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $model=new PermissionGroup;
        $model->fill($request->all());

        if ($model->save()) {

            session()->flash('app_message', 'PermissionGroup saved successfully');
            return redirect()->route('permission_groups.index');
            } else {
                session()->flash('app_message', 'Something is wrong while saving PermissionGroup');
            }
        return redirect()->back();
    } /**
     * Show the form for editing the specified resource.
     *
     * @param  Edit  $request
     * @param  PermissionGroup  $permissiongroup
     * @return \Illuminate\Http\Response
     */
    public function edit(Edit $request, PermissionGroup $permissiongroup)
    {

        return view('pages.permission_groups.edit', [
            'model' => $permissiongroup,

            ]);
    }    /**
     * Update a existing resource in storage.
     *
     * @param  Update  $request
     * @param  PermissionGroup  $permissiongroup
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request,PermissionGroup $permissiongroup)
    {
        $permissiongroup->fill($request->all());

        if ($permissiongroup->save()) {

            session()->flash('app_message', 'PermissionGroup successfully updated');
            return redirect()->route('permission_groups.index');
            } else {
                session()->flash('app_error', 'Something is wrong while updating PermissionGroup');
            }
        return redirect()->back();
    }    /**
     * Delete a  resource from  storage.
     *
     * @param  Destroy  $request
     * @param  PermissionGroup  $permissiongroup
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Destroy $request, PermissionGroup $permissiongroup)
    {
        if ($permissiongroup->delete()) {
                session()->flash('app_message', 'PermissionGroup successfully deleted');
            } else {
                session()->flash('app_error', 'Error occurred while deleting PermissionGroup');
            }

        return redirect()->back();
    }
}
