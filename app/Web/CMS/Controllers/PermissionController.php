<?php

namespace App\Web\CMS\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\PermissionGroup;
use App\Web\CMS\Requests\Permissions\Index;
use App\Web\CMS\Requests\Permissions\Show;
use App\Web\CMS\Requests\Permissions\Create;
use App\Web\CMS\Requests\Permissions\Store;
use App\Web\CMS\Requests\Permissions\Edit;
use App\Web\CMS\Requests\Permissions\Update;
use App\Web\CMS\Requests\Permissions\Destroy;
use View;

class PermissionController extends BaseController
{
       /**
     * Display a listing of the resource.
     *
     * @param  Index  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Index $request)
    {
        return view('view-user.page.permission.index', ['records' => Permission::paginate(10)]);
    }
    public function list_view(Index $request)
    {
        return View::make('view-user.page.permission.list-make',['list'=>Permission::get()->toArray()]);
    }
    /**
     * Display the specified resource.
     *
     * @param  Show  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Show $request, Permission $permission)
    {
        return view('pages.permissions.show', [
                'record' =>$permission,
        ]);

    }    /**
     * Show the form for creating a new resource.
     *
     * @param  Create  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Create $request)
    {
		$permission_groups = PermissionGroup::all(['id']);

        return view('pages.permissions.create', [
            'model' => new Permission,
			"permission_groups" => $permission_groups,

        ]);
    }    /**
     * Store a newly created resource in storage.
     *
     * @param  Store  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $model=new Permission;
        $model->fill($request->all());

        if ($model->save()) {

            session()->flash('app_message', 'Permission saved successfully');
            return redirect()->route('permissions.index');
            } else {
                session()->flash('app_message', 'Something is wrong while saving Permission');
            }
        return redirect()->back();
    } /**
     * Show the form for editing the specified resource.
     *
     * @param  Edit  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Edit $request, Permission $permission)
    {
		$permission_groups = PermissionGroup::all(['id']);

        return view('pages.permissions.edit', [
            'model' => $permission,
			"permission_groups" => $permission_groups,

            ]);
    }    /**
     * Update a existing resource in storage.
     *
     * @param  Update  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request,Permission $permission)
    {
        $permission->fill($request->all());

        if ($permission->save()) {

            session()->flash('app_message', 'Permission successfully updated');
            return redirect()->route('permissions.index');
            } else {
                session()->flash('app_error', 'Something is wrong while updating Permission');
            }
        return redirect()->back();
    }    /**
     * Delete a  resource from  storage.
     *
     * @param  Destroy  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Destroy $request, Permission $permission)
    {
        if ($permission->delete()) {
                session()->flash('app_message', 'Permission successfully deleted');
            } else {
                session()->flash('app_error', 'Error occurred while deleting Permission');
            }

        return redirect()->back();
    }
}
