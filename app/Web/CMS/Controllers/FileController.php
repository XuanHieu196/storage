<?php

namespace App\Web\CMS\Controllers;

use App\Log;
use App\Web\CMS\Models\FileModel;
use App\Models\MediaShare;
use App\Web\CMS\Transformers\FileTransformer;
use Exception;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Web\CMS\Requests\Files\Index;
use App\Web\CMS\Requests\Files\Show;
use App\Web\CMS\Requests\Files\Create;
use App\Web\CMS\Requests\Files\Store;
use App\Web\CMS\Requests\Files\Edit;
use App\Web\CMS\Requests\Files\Update;
use App\Web\CMS\Requests\Files\Destroy;
use FilePreviews\FilePreviews;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use View;


class FileController extends BaseController
{
    /**
     * @var FileModel
     */
    protected $model;

    /**
     * FileController constructor.
     */
    public function __construct()
    {
        $this->model = new FileModel();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return Response
     */
    public function index(Index $request)
    {
        $input = $request->all();
        $files = $this->model->search($input, ['folders', 'files'], 10);
        return view('pages.files.index', ['records' => $files]);
    }

    /**
     * @param Index $request
     * @param FileTransformer $fileTransformer
     * @return \Dingo\Api\Http\Response
     */
    // public function list(Index $request, FileTransformer $fileTransformer)
    // {
    //     $input = $request->all();
    //     $files = $this->model->search($input, []);
    //     return $this->response->collection($files, $fileTransformer);
    // }
    public function list_view(Index $request, FileTransformer $fileTransformer)
    {
        $input = $request->all();
        $files = $this->model->search($input, []);
        return View::make('view-user.page.file.list-index-make', ['list' => $files->toArray()]);
    }

    /**
     * Display the specified resource.
     *
     * @param Show $request
     * @param $id
     * @return Response
     */
    public function show(Show $request, $id)
    {
        $file = $this->model->detail($id);
        return view('view-user.page.list.detail-file', [
            'record' => $file,
        ]);

    }
    public function show_view(Show $request, $id)
    {
        $file = $this->model->detail($id);
        return view('view-user.page.file.detail', [
            'record' => $file,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Create $request
     * @return Response
     */
    public function create(Create $request)
    {
        $files = File::all(['id']);

        return view('pages.files.create', [
            'model' => new File,
            "files" => $files,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        try {
            DB::beginTransaction();
            $file = $this->model->store($request->all());
            Log::create($this->model->getTable(), $file->name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['messages' => __('messages.store_success', ['name' => __('tables.files')])], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Edit $request
     * @param File $file
     * @return Response
     */
    public function edit(Edit $request, File $file)
    {
        $files = File::all(['id']);

        return view('pages.files.edit', [
            'model' => $file,
            "files" => $files,

        ]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        try {

            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $id;
            $file = $this->model->update($input);
            Log::update($this->model->getTable(), $file->name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.update_failed', ['name' => __('tables.files')])]], 500);
        }
        return response()->json(['messages' => __('messages.update_success', ['name' => __('tables.files')])], 200);
    }

    /**
     * Delete a  resource from  storage.
     *
     * @param Destroy $request
     * @param $id
     * @return Response
     */
    public function destroy(Destroy $request, $id)
    {

        try {
            DB::beginTransaction();
            //            $input = $request->all();
            $file = $this->model->delete($id);
            Log::delete($this->model->getTable(), $id);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return response()->json(['messages' => __('messages.delete_success', ['name' => __('tables.files')])], 200);
    }

    /**
     * Download a  resource from  storage.
     *
     * @param Destroy $request
     * @param $id
     * @return Response
     */
    public function download(Destroy $request, $id)
    {
        try {
            DB::beginTransaction();
            //            $input = $request->all();
            $file = $this->model->download($id);
            DB::commit();
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }
        return $file;
    }

    /**
     * Download a  resource from  storage.
     *
     * @param Destroy $request
     * @param $id
     * @return Response
     */
    public function copy(Destroy $request, $id)
    {
        $input = $request->all();
        $folder_id = Arr::get($input, 'folderToId', null);
        try {
            DB::beginTransaction();
            $this->model->copy($id, 'COPY', $folder_id);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['messages' => __('messages.copy_success')], 200);
    }

    /**
     * @param $id
     * @return Response
     */
    public function unShare($id)
    {
        try {
            $file = File::find($id);
            if (empty($file) || $file->created_by != auth()->id()) {
                return response()->json(['error' => ['messages' => __('messages.permission')]], 500);
            }
            DB::beginTransaction();
            MediaShare::whereFileId($id)->delete();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => __('messages.app_error')]], 500);
        }

        return response()->json(['messages' => __('messages.unshare_success')], 200);
    }

    public function sharePermission($id)
    {
        $files = $this->model->checkPermission($id);
        $list_user = \App\Models\User::get()->toArray();
        $list_division = \App\Models\Division::get()->toArray();
        return View::make('view-user.page.file.form-edit-file', [
            'data'          => $files->drive_share,
            'list_user'     => $list_user,
            'list_division' => $list_division
        ]);

        $folder = $this->model->checkPermission($id);
        return response()->json(['data' => $folder->drive_share], 200);
    }

    /**
     * Move a  resource from  storage.
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function move(Request $request, $id)
    {
        $input = $request->all();
        $folderId = Arr::get($input, 'folderToId', null);
        try {
            DB::beginTransaction();
            $this->model->move($id, $folderId);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['messages' => __('messages.copy_success')], 200);
    }



    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function shareLink(Request $request,$id)
    {
        try {
            $input = $request->all();
            $file = $this->model->shareLink($input, $id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['data' => $file], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function unShareLink(Request $request,$id)
    {
        try {
            $input = $request->all();
            $file = $this->model->unShareLink($input, $id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        return response()->json(['data' => $file], 200);
    }

    public function preview(Request $request,$id)
    {
        try {
            $input = $request->all();
            $url = $this->model->preview($id);
        } catch (Exception $exception) {
            return response()->json(['error' => ['messages' => $exception->getMessage()]], 500);
        }

        $fp = new FilePreviews([
            'api_key' => env('PREVIEW_KEY'),
            'api_secret' => env('PREVIEW_SECRET')
        ]);



        return view('preview_file', [
            'url'          => 'https://www.filepicker.io/api/file/yiYv3uQeSLWdbRkYz0GG'
        ]);
    }
}
