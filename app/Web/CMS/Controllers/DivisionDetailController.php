<?php

namespace App\Web\CMS\Controllers;

use App\Exceptions\DRIVE_ERROR;
use App\Log;
use App\Models\DivisionDetail;
use App\Models\User;
use App\Models\Division;
use App\Web\CMS\Models\DivisionDetailModel;
use App\Web\CMS\Requests\DivisionDetails\Index;
use App\Web\CMS\Requests\DivisionDetails\Show;
use App\Web\CMS\Requests\DivisionDetails\Create;
use App\Web\CMS\Requests\DivisionDetails\Store;
use App\Web\CMS\Requests\DivisionDetails\Edit;
use App\Web\CMS\Requests\DivisionDetails\Update;
use App\Web\CMS\Requests\DivisionDetails\Destroy;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use View;


class DivisionDetailController extends BaseController
{
    /**
     * DivisionDetailController constructor.
     */
    public function __construct()
    {
        $this->model = new DivisionDetailModel();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @return Response
     */
    public function index(Index $request)
    {
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return view('view-user.page.division_detail.index', ['list_user' => $list_user, 'list_division' => $list_division]);
    }

    public function list_view(Index $request)
    {
        return View::make('view-user.page.division_detail.list-make', ['list' => DivisionDetail::get()->toArray()]);
    }
    /**
     * Display the specified resource.
     *
     * @param Show $request
     * @param DivisionDetail $divisiondetail
     * @return Response
     */
    public function show(Show $request, DivisionDetail $divisiondetail)
    {
        return view('pages.division_details.show', [
            'record' => $divisiondetail,
        ]);

    }

    public function reload_form_create()
    {
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return View::make('view-user.page.division_detail.form-create', ['list_user' => $list_user, 'list_division' => $list_division]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Create $request
     * @return Response
     */
    public function create(Create $request)
    {
        $users = User::all(['id']);
        $divisions = Division::all(['id']);

        return view('pages.division_details.create', [
            'model'     => new DivisionDetail,
            "users"     => $users,
            "divisions" => $divisions,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        try {

            DB::beginTransaction();
            $input = $request->all();
            $this->model->upsert($input);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $response = DRIVE_ERROR::handle($exception);
            return response()->json(['messages' => $response['message']], $response['code']);
        }

        return response()->json(['messages' => __('messages.store_success', ['name' => __('tables.division_details')])], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Edit $request
     * @param DivisionDetail $divisiondetail
     * @return Response
     */
    public function edit(Edit $request, DivisionDetail $divisiondetail)
    {
        $users = User::all(['id']);
        $divisions = Division::all(['id']);

        return view('pages.division_details.edit', [
            'model'     => $divisiondetail,
            "users"     => $users,
            "divisions" => $divisions,

        ]);
    }

    public function edit_view(Edit $request, DivisionDetail $divisiondetail)
    {
        $users = User::all(['id']);
        $divisions = Division::all(['id']);
        return View::make('view-user.page.division_detail.edit-make', [
            'model'     => $divisiondetail,
            "users"     => $users,
            "divisions" => $divisions,
        ]);
    }

    /**
     * Update a existing resource in storage.
     *
     * @param Update $request
     * @param $id
     * @return Response
     * @throws Exception
     */
    public function update(Update $request, $id)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['id'] = $id;
            $this->model->upsert($input);
            Log::update($this->model->getTable(), $this->model->getTable());
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $response = DRIVE_ERROR::handle($exception);
            return $this->response->errorBadRequest($response['message']);
        }

        return response()->json(['messages' => __('messages.update_success', ['name' => $this->model->getTable()])], 200);

    }

    /**
     * Delete a  resource from  storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $divisiondetail = DivisionDetail::model()->find($id);
            if (empty($divisiondetail)) {
                throw new \Exception(__('messages.not_existed', ['name' => '#ID' . $id]));
            }
            $this->model->checkPermision($divisiondetail->division_id);
            $name = $divisiondetail->user->email;
            $divisiondetail->delete();
            Log::delete($this->model->getTable(), $name);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $response = DRIVE_ERROR::handle($exception);
            return $this->response->errorBadRequest($response['message']);
        }

        return response()->json(['messages' => __('messages.delete_success', ['name' => __('tables.division_details')])], 200);
    }

    // --------------------------------------------
    // For Division
    public function list_view_id_division(Index $request, $id_division)
    {
        $data = DivisionDetail::where('division_id',$id_division)->get()->toArray();
        return View::make('view-user.page.divisions.detail.list-make', ['list' => $data]);
    }
    public function reload_form_create_id_division($id_division)
    {
        $list_user = User::get()->toArray();
        $list_division = Division::get()->toArray();
        return View::make('view-user.page.divisions.detail.form-create', ['list_user' => $list_user, 'list_division' => $list_division, 'id_division'=>$id_division]);
    }
    public function edit_view_id_division(Edit $request, DivisionDetail $division_detail, $id_division)
    {
        $users = User::all(['id']);
        $divisions = Division::all(['id']);
        return View::make('view-user.page.divisions.detail.edit-make', [
            'model'     => $division_detail,
            "users"     => $users,
            "divisions" => $divisions,
            'id_division' => $id_division
        ]);
    }
    // --------------------------------------------
}
