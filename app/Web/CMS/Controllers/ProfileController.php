<?php

namespace App\Web\CMS\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\User;
use App\Web\CMS\Requests\Profiles\Index;
use App\Web\CMS\Requests\Profiles\Show;
use App\Web\CMS\Requests\Profiles\Create;
use App\Web\CMS\Requests\Profiles\Store;
use App\Web\CMS\Requests\Profiles\Edit;
use App\Web\CMS\Requests\Profiles\Update;
use App\Web\CMS\Requests\Profiles\Destroy;


class ProfileController extends BaseController
{
       /**
     * Display a listing of the resource.
     *
     * @param  Index  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Index $request)
    {
        return view('pages.profiles.index', ['records' => Profile::paginate(10)]);
    }    /**
     * Display the specified resource.
     *
     * @param  Show  $request
     * @param  Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Show $request, Profile $profile)
    {
        return view('pages.profiles.show', [
                'record' =>$profile,
        ]);

    }    /**
     * Show the form for creating a new resource.
     *
     * @param  Create  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Create $request)
    {
		$users = User::all(['id']);

        return view('pages.profiles.create', [
            'model' => new Profile,
			"users" => $users,

        ]);
    }    /**
     * Store a newly created resource in storage.
     *
     * @param  Store  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $model=new Profile;
        $model->fill($request->all());

        if ($model->save()) {

            session()->flash('app_message', 'Profile saved successfully');
            return redirect()->route('profiles.index');
            } else {
                session()->flash('app_message', 'Something is wrong while saving Profile');
            }
        return redirect()->back();
    } /**
     * Show the form for editing the specified resource.
     *
     * @param  Edit  $request
     * @param  Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Edit $request, Profile $profile)
    {
		$users = User::all(['id']);

        return view('pages.profiles.edit', [
            'model' => $profile,
			"users" => $users,

            ]);
    }    /**
     * Update a existing resource in storage.
     *
     * @param  Update  $request
     * @param  Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request,Profile $profile)
    {
        $profile->fill($request->all());

        if ($profile->save()) {

            session()->flash('app_message', 'Profile successfully updated');
            return redirect()->route('profiles.index');
            } else {
                session()->flash('app_error', 'Something is wrong while updating Profile');
            }
        return redirect()->back();
    }    /**
     * Delete a  resource from  storage.
     *
     * @param  Destroy  $request
     * @param  Profile  $profile
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Destroy $request, Profile $profile)
    {
        if ($profile->delete()) {
                session()->flash('app_message', 'Profile successfully deleted');
            } else {
                session()->flash('app_error', 'Error occurred while deleting Profile');
            }

        return redirect()->back();
    }
}
