<?php

namespace App\Web\CMS\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'        => 'required|unique:roles,code',
            'name'        => 'required|max:100',
            'description' => 'nullable|string',
            'is_active'   => 'required|boolean',
            'level'       => 'nullable|numeric',
            'created_by'  => 'nullable|max:20',
            'updated_by'  => 'nullable|max:20',
            'deleted_by'  => 'nullable|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
