<?php

namespace App\Web\CMS\Requests\Profiles;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'    => 'required|exists:users,id|numeric',
            'email'      => 'nullable|max:50',
            'first_name' => 'required|max:100',
            'last_name'  => 'nullable|max:100',
            'full_name'  => 'required|max:100',
            'address'    => 'nullable|max:500',
            'phone'      => 'nullable|max:20',
            'birthday'   => 'nullable|date',
            'genre'      => 'required|in:Male,Female,Other',
            'avatar'     => 'nullable|string',
            'is_active'  => 'required|boolean',
            'created_by' => 'nullable|max:50',
            'updated_by' => 'nullable|max:50',
            'deleted_by' => 'nullable|max:50',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
