<?php

namespace App\Web\CMS\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'         => 'required',
            'username'     => 'required|max:50',
            'email'        => 'required|max:50',
            'phone'        => 'required|max:14',
            'type'         => 'nullable|in:ADMIN,SUPER_ADMIN,SALE,ACCOUNTANT,UNKNOWN',
            'verify_code'  => 'nullable|max:6',
            'expired_code' => 'nullable|date',
            'role_id'      => 'nullable|exists:roles,id|numeric',
            'note'         => 'nullable|max:300',
            'is_active'    => 'required|boolean',
            'created_by'   => 'nullable|max:50',
            'updated_by'   => 'nullable|max:50',
            'deleted_by'   => 'nullable|max:50',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
