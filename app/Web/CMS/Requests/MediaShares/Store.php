<?php

namespace App\Web\CMS\Requests\MediaShares;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'division_id' => 'nullable|exists:divisions,id|numeric',
            // 'file_id'     => 'nullable|exists:files,id|numeric',
            // 'folder_id'   => 'nullable|exists:folders,id|numeric',
            // 'user_id'     => 'nullable|exists:users,id|numeric',
            // 'permission'  => 'nullable|in:VIEW,COMMENT,UPDATE',
            // 'is_active'   => 'nullable|boolean',
            // 'deleted'     => 'nullable|boolean',
            // 'created_by'  => 'nullable|max:20',
            // 'updated_by'  => 'nullable|max:20',
            // 'deleted_by'  => 'nullable|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
