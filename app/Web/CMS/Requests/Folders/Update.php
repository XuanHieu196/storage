<?php

namespace App\Web\CMS\Requests\Folders;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'nullable|max:200',
            'path'       => 'nullable|max:200',
            'parent_id'  => 'nullable|exists:folders,id|numeric',
            'key'        => 'nullable|unique:folders,key',
            'is_active'  => 'nullable|boolean',
            'created_by' => 'nullable|max:20',
            'updated_by' => 'nullable|max:20',
            'deleted_by' => 'nullable|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
