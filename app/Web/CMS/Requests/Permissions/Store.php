<?php

namespace App\Web\CMS\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|max:200',
            'code'        => 'required|unique:permissions,code',
            'type'        => 'required|in:DEPARTMENT,ROLE',
            'description' => 'nullable|string',
            'group_id'    => 'nullable|exists:permission_groups,id|numeric',
            'is_active'   => 'required|boolean',
            'created_by'  => 'nullable|max:20',
            'updated_by'  => 'nullable|max:20',
            'deleted_by'  => 'nullable|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
