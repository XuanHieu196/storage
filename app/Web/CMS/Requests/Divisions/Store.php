<?php

namespace App\Web\CMS\Requests\Divisions;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'        => 'required',
            'name'        => 'required|max:50',
            'description' => 'nullable|max:255',
            'type'        => 'nullable|in:DEPARTMENT,PROJECT,TEAM',
            'is_active'   => 'required|boolean',
            'created_by'  => 'nullable|max:50',
            'updated_by'  => 'nullable|max:50',
            'deleted_by'  => 'nullable|max:50',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
