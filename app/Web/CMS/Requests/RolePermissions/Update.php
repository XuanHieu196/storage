<?php

namespace App\Web\CMS\Requests\RolePermissions;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id'       => 'required|exists:roles,id|numeric',
            'permission_id' => 'required|exists:permissions,id|numeric',
            'description'   => 'nullable|string',
            'is_active'     => 'nullable|boolean',
            'created_by'    => 'nullable|max:20',
            'updated_by'    => 'nullable|max:20',
            'deleted_by'    => 'nullable|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
