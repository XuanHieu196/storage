<?php

namespace App\Web\CMS\Requests\PermissionGroups;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|max:200',
            'code'        => 'required',
            'description' => 'nullable|string',
            'is_active'   => 'nullable|boolean',
            'created_by'  => 'nullable|max:20',
            'updated_by'  => 'nullable|max:20',
            'deleted_by'  => 'nullable|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

}
