<?php

namespace App\Web\CMS\Models;

use App\Acheckin;
use App\Models\File;
use App\Models\Folder;
use App\Models\MediaShare;
use App\Models\User;
use Illuminate\Support\Arr;

class MediaShareModel extends AbstractModel
{
    /**
     * MediaShareModel constructor.
     * @param MediaShare|null $model
     */
    public function __construct(MediaShare $model = null)
    {
        parent::__construct($model);
    }

    public function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $this->sortBuilder($query, $input);
        $query->whereUserId(auth()->id())
            ->orWhereIn('division_id', Acheckin::divisionDetailIds());

        if (!empty($input['name'])) {
            $query->where(function ($q) use ($input) {
                $q->whereHas('folder', function ($qr) use ($input) {
                    $qr->where('name', $input['name']);
                });
                $q->orWhereHas('file', function ($qr) use ($input) {
                    $qr->where('name', $input['name']);
                });
            });
        }
        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }

    public function detail($id, $with = [])
    {
        $query = $this->make($with);
        $query->where('id', $id);
        return $query->first();
    }

    public function store($input)
    {
        $divisionIds = Arr::get($input, 'divisions', []);
        $fileId = null;
        $folderId = null;
        $userIds = Arr::get($input, 'users', []);
        if (!empty($input['file_id'])) {
            $fileModel = new FileModel();
            $file = $fileModel->checkPermission($input['file_id']);
            $fileId = $file->id;
        }
        if (!empty($input['folder_id'])) {
            $folderModel = new FolderModel();
            $folder = $folderModel->checkPermission($input['folder_id']);
            $folderId = $folder->id;
        }

        foreach ($divisionIds as $divisionId) {
            $param = [
                'division_id' => $divisionId,
                'file_id'     => $fileId,
                'folder_id'   => $folderId,
                'user_id'     => null,
                'permission'  => Arr::get($input, 'permission'),
                'is_active'   => 1,
            ];
            MediaShare::create($param);
        }
        foreach ($userIds as $userId) {
            $param = [
                'division_id' => null,
                'file_id'     => $fileId,
                'folder_id'   => $folderId,
                'user_id'     => $userId,
                'permission'  => Arr::get($input, 'permission'),
                'is_active'   => 1,
            ];
            MediaShare::create($param);
        }
        if (empty($userIds) && empty($divisionIds)) {
            $param = [
                'division_id' => null,
                'file_id'     => $fileId,
                'folder_id'   => $folderId,
                'user_id'     => null,
                'permission'  => Arr::get($input, 'permission'),
                'is_active'   => 1,
            ];
            MediaShare::create($param);
        }
        return true;
    }

    public function update($input)
    {
        $divisionId = null;
        $fileId = null;
        $folderId = null;
        $userId = null;
        if (!empty($input['file_id'])) {
            $fileModel = new FileModel();
            $file = $fileModel->checkPermission($input['file_id'], MediaShare::PERMISSION_UPDATE);
            $fileId = $file->id;
        }
        if (!empty($input['folder_id'])) {
            $folderModel = new FolderModel();
            $folder = $folderModel->checkPermission($input['file_id'], MediaShare::PERMISSION_UPDATE);
            $folderId = $folder->id;
        }
        $mediaShare = MediaShare::find($input['id']);
        if (empty($mediaShare)) {
            throw new \Exception(__('messages.not_existed', ['name' => '#ID ' . $input['id']]));
        }
        $mediaShare->type = $input['type'];
        $mediaShare->save();
        return $mediaShare;
    }

    public function checkPermission($id, $permission = null)
    {
        $divisionDetailIds = Acheckin::divisionDetailIds() ?? [];
        $mediaShare = MediaShare::find($id);
        if (empty($mediaShare)) {
            throw new \Exception(__('messages.permission'));
        }
        $divisionId = !empty($mediaShare->division_id) ? $mediaShare->division_id : 0;
        $userId = !empty($mediaShare->user_id) ? $mediaShare->user_id : 0;

        if (!in_array($divisionId, $divisionDetailIds) && $userId != auth()->id() && $mediaShare->created_by != auth()->id()) {
            throw new \Exception(__('messages.permission'));
        }
        return $mediaShare;
    }

    public function sync($input)
    {
        $divisionIds = Arr::get($input, 'divisions', []);
        $fileId = null;
        $folderId = null;
        $userIds = Arr::get($input, 'users', []);
        $permission = Arr::get($input, 'permission', MediaShare::PERMISSION_VIEW);

        if (!empty($input['file_id'])) {
            $fileModel = new FileModel();
            $file = File::model()->whereId($input['file_id'])->whereCreatedBy(auth()->id())->first();
            if (empty($file)) {
                throw new \Exception(__('messages.permission'));
            }
            $fileId = $file->id;
            MediaShare::model()->whereFileId($fileId)->wherePermission($permission)->forceDelete();
        }
        if (!empty($input['folder_id'])) {
            $folderModel = new FolderModel();
            $folder = Folder::model()->whereId($input['folder_id'])->whereCreatedBy(auth()->id())->first();
            $folderId = $folder->id;
            MediaShare::model()->whereFolderId($folderId)->wherePermission($permission)->forceDelete();
        }

        foreach ($divisionIds as $divisionId) {
            $param = [
                'division_id' => $divisionId,
                'file_id'     => $fileId,
                'folder_id'   => $folderId,
                'user_id'     => null,
                'permission'  => $permission,
                'is_active'   => 1,
            ];
            MediaShare::create($param);
        }

        foreach ($userIds as $userId) {
            $param = [
                'division_id' => null,
                'file_id'     => $fileId,
                'folder_id'   => $folderId,
                'user_id'     => $userId,
                'permission'  => $permission,
                'is_active'   => 1,
            ];
            MediaShare::create($param);
        }
    }

    public function searchApi($input = [], $with = [], $limit = null)
    {
        if (empty($input['email'])) {
            throw new Exception(__('messages.unauthorized'));
        }
        $user = User::model()->whereEmail($input['email'])->first();
        $query = $this->make($with);
        $this->sortBuilder($query, $input);
        $query->whereUserId($user->id)
            ->orWhereIn('division_id', Acheckin::divisionIds($user));

        if (!empty($input['name'])) {
            $query->where(function ($q) use ($input) {
                $q->whereHas('folder', function ($qr) use ($input) {
                    $qr->where('name', $input['name']);
                });
                $q->orWhereHas('file', function ($qr) use ($input) {
                    $qr->where('name', $input['name']);
                });
            });
        }
        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }

    public function loadMediaShare($input, $id)
    {
        $data = [];
        $mediaShares = MediaShare::with(['createdBy', 'file', 'folder'])
            ->where(function ($q) use ($input, $id) {
                $q->whereUserId($id)
                    ->orWhereIn('division_id', Acheckin::divisionDetailIds());
            });
        if (!empty($input['name'])) {
            $search = $input['name'];
            $mediaShares->where(function ($q) use ($input, $search) {
                $q->whereHas('folder', function ($qr) use ($search) {
                    $qr->where('name', "like", "%$search%");
                });
                $q->orWhereHas('file', function ($qr) use ($search) {
                    $qr->where('name', "like", "%$search%");
                });
            });
        }
        if (!empty($input['division'])) {
            $mediaShares->whereNotNull('division_id');
        }

        $mediaShares = $mediaShares->get();
        return $this->getData($mediaShares);
    }

    public function loadMediaShareDivision($input, $id)
    {
        $data = [];
        $mediaShares = MediaShare::with(['createdBy', 'file', 'folder'])
            ->whereDivisionId($id);
        if (!empty($input['name'])) {
            $mediaShares->where(function ($q) use ($input) {
                $q->whereHas('folder', function ($qr) use ($input) {
                    $qr->where('name', $input['name']);
                });
                $q->orWhereHas('file', function ($qr) use ($input) {
                    $qr->where('name', $input['name']);
                });
            });
        }

        $mediaShares = $mediaShares->get();
        return $this->getData($mediaShares);
    }

    public function getData($mediaShares)
    {
        $data = [];
        foreach ($mediaShares as $mediaShare) {
            $file = $mediaShare->file;
            $folder = $mediaShare->folder;
            if (!empty($file)) {
                $data[] = [
                    "id"         => $file->id,
                    "name"       => $file->name,
                    "path"       => $file->path,
                    "full_path"  => null,
                    "key_url"    => $file->key_url,
                    "key"        => $file->key,
                    "type"       => "FILE",
                    "size"       => $file->size,
                    "share"      => $file->share,
                    "is_share"   => $file->is_share,
                    "is_active"  => $file->is_active,
                    "created_by" => $file->createdBy->email,
                    "created_at" => $file->created_at,
                    "updated_at" => $file->updated_at,
                ];
            }

            if (!empty($folder)) {

                $data[] = [
                    "id"         => $folder->id,
                    "name"       => $folder->name,
                    "path"       => $folder->path,
                    "full_path"  => fullPath($folder),
                    "key_url"    => $folder->key_url,
                    "key"        => $folder->key,
                    "type"       => "FOLDER",
                    "size"       => $folder->size,
                    "share"      => $folder->share,
                    "is_share"   => $folder->is_share,
                    "is_active"  => $folder->is_active,
                    "created_by" => $folder->createdBy->email,
                    "created_at" => $folder->created_at,
                    "updated_at" => $folder->updated_at,
                ];
            }
        }
        return $data;
    }


}
