<?php

namespace App\Web\CMS\Models;

use App\Acheckin;
use App\Models\File;
use App\Models\Folder;
use App\Models\MediaShare;
use App\Models\User;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;

class FolderModel extends AbstractModel
{
    /**
     * FolderModel constructor.
     * @param Folder|null $model
     */
    public function __construct(Folder $model = null)
    {
        parent::__construct($model);
    }

    public function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);

        $this->sortBuilder($query, $input);
        $full_columns = DB::getSchemaBuilder()->getColumnListing($this->getTable());

        $input = array_intersect_key($input, array_flip($full_columns));

        foreach ($input as $field => $value) {
            if ($value === "") {
                continue;
            }
            if (is_array($value)) {
                $query->where(function ($q) use ($field, $value) {
                    foreach ($value as $action => $data) {
                        $action = strtoupper($action);
                        if ($data === "") {
                            continue;
                        }
                        switch ($action) {
                            case "LIKE":
                                $q->orWhere(DB::raw($field), "like", "%$data%");
                                break;
                            case "IN":
                                $q->orWhereIn(DB::raw($field), $data);
                                break;
                            case "NOT IN":
                                $q->orWhereNotIn(DB::raw($field), $data);
                                break;
                            case "NULL":
                                $q->orWhereNull(DB::raw($field));
                                break;
                            case "NOT NULL":
                                $q->orWhereNotNull(DB::raw($field));
                                break;
                            case "BETWEEN":
                                $q->orWhereBetween(DB::raw($field), $value);
                                break;
                            default:
                                $q->orWhere(DB::raw($field), $action, $data);
                                break;
                        }
                    }
                });
            } else {
                $query->where(DB::raw($field), 'like', "%$value%");
            }
        }
        $query->where('created_by', auth()->id());

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get()->toTree();
        }
    }

    public function detail($id)
    {
        $folder = $this->checkPermission($id);
        return $folder;
    }

    public function store($input)
    {
        $key = $this->genKeyUrl(32);
        $param = [
            'name'      => $input['name'],
            'parent_id' => Arr::get($input, 'parent_id'),
            'key_url'   => $key,
            'is_active' => 1,
        ];
        $parent_id = Arr::get($input, 'parent_id');
        if (!empty($parent_id)) {
            $folderParent = Folder::find($parent_id);
            $folder = $this->create($param, $folderParent);
        } else {
            $folder = $this->create($param);
        }

        return $folder;
    }

    public function update($input)
    {
        $id = $input['id'];
        $permission = "UPDATE";
        $folder = $this->checkPermission($id, $permission);
        $parent_id = Arr::get($input, 'parent_id');
        if (!empty($parent_id)) {
            $folderParent = Folder::find($parent_id);
            $folder->parent_id = $folderParent->id;
        }
        $folder->name = Arr::get($input, 'name', $folder->name);
        $folder->updated_at = date("Y-m-d H:i:s", time());
        $folder->is_active = Arr::get($input, 'is_active', $folder->is_active);
        $folder->updated_by = auth()->id();
        $folder->save();

        return $folder;
    }

    private function genKeyUrl($length)
    {
        $key = random_str($length);
        $folder = Folder::getKeyUrl($key)->first();
        if ($folder) {
            $this->genKeyUrl($length);
        }
        return $key;
    }

    public function parentIds($id, &$ids)
    {
        $forlder = Folder::find($id);

        if (!empty($forlder) && !empty($forlder->parent)) {
            $forlderParent = $forlder->parent;
            array_push($ids, $forlderParent->id);
            $this->parentIds($forlderParent->id, $ids);
        }
        return $ids;
    }

    public function checkPermission($id, $permission = null)
    {
        $folder = Folder::with(['ancestors', 'files', 'folders'])->descendantsAndSelf($id)->toTree()->first();
        if (empty($folder)) {
            throw new Exception(__('messages.not_existed', ['name' => '#ID ' . $id]));
        }
        if ($folder->created_by == auth()->id()) {
            return $folder;
        }

        if ($folder->is_share == 1) {
            if (!empty($permission) && $folder->permission != $permission && $permission == "UPDATE") {
                throw new Exception(__('messages.permission'));
            }
            return $folder;
        }
        $mediaShare = MediaShare::whereFolderId($id)
            ->whereNull('division_id')
            ->whereNull('user_id')->first();
        if (!empty($mediaShare)) {
            return $folder;
        }
        $parentIds = [];
        $parentIds = $this->parentIds($id, $parentIds);
        $divisionDetailIds = Acheckin::divisionDetailIds() ?? [];
        array_push($parentIds, $folder->id);
        $countFolderShare = MediaShare::where(function ($query) use ($parentIds, $divisionDetailIds, $permission) {
            $query->whereIn('folder_id', $parentIds)
                ->whereIn('division_id', $divisionDetailIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });
        })->orWhere(function ($query) use ($parentIds, $permission) {
            $query->where('user_id', auth()->id());
            $query->whereIn('folder_id', $parentIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });
        })->count();

        if ($countFolderShare == 0 && $folder->created_by != auth()->id()) {
            throw new Exception(__('messages.permission'));
        }

        return $folder;
    }

    public function getFolderPath($id, &$path)
    {
        $folder = Folder::find($id);
        if (empty($folder)) {
            return $path;
        }
        $path = $folder->path . '/' . $path;
        if (!empty($folder->parent_id)) {
            $this->getFolderPath($folder->parent_id, $path);
        }
        $path = rtrim($path, '/');
        return $path;
    }

    public function loadFolderZip($folder, $zip, &$path = null)
    {
        foreach ($folder->files as $file) {
            $file_content = Storage::disk()->get($file->code);
            $fileName = trim($path . '/' . $file->name, '/');
            $zip->put($fileName, $file_content);
        }
        foreach ($folder->children as $children) {
            $path = trim($path . '/' . $children->name, '/');
            $this->loadFolderZip($children, $zip, $path);
        }
    }

    public function download($id)
    {
        $folder = $this->checkPermission($id);
        if (empty($folder)) {
            throw new Exception(__('messages.download_fail'));
        }
        $zip = new Filesystem(new ZipArchiveAdapter(storage_path('app/' . $folder->name . '.zip')));
        $this->loadFolderZip($folder, $zip);
        $zip->getAdapter()->getArchive()->close();
        $fileZip = Storage::disk('local')->exists($folder->name . '.zip');
        if (!$fileZip) {
            throw new Exception(__('messages.file_exist', ['name' => $folder->name . '.zip']));
        }
        return Storage::disk('local')->download($folder->name . '.zip', $folder->name . '.zip');
    }


    public function createChildren(&$forder, $action, &$folderTo = null)
    {
        $key = $this->genKeyUrl(32);
        $param = [
            'name'      => $forder->name,
            'key_url'   => $key,
            //            'parent_id' => $folderTo->id,
            'is_active' => 1,
        ];
        $folderNew = Folder::create($param, $folderTo);
        $this->moveFiles($forder, $folderNew, $action);

        if (!empty($forder->children)) {
            foreach ($forder->children as $chidren) {
                $this->createChildren($chidren, $action, $folderNew);
            }
        }
        return $folderNew;
    }

    public function copy($directoryFrom, $action = "COPY", $directoryTo = 0)
    {
        $directoryFrom = $this->checkPermission($directoryFrom->id);
        if (!empty($directoryTo)) {
            $directoryTo = $this->checkPermission($directoryTo->id);
            if (empty($directoryTo)) {
                throw new Exception(__('messages.fail', ['name' => 'directory']));
            }
        }
        $this->createChildren($directoryFrom, $action, $directoryTo);
        if ($action == "MOVE") {
            $directoryFrom->delete();
        }
    }

    /**
     * @param $directoryFrom
     * @param $directoryTo
     * @param $action
     */
    public function moveFiles($directoryFrom, $directoryTo, $action)
    {
        $files = $directoryFrom->files;
        if (!empty($files)) {
            foreach ($files as $file) {
                $fileModel = new FileModel();
                $code = $fileModel->genCode(32);
                $contents = Storage::get($file->code);
                Storage::disk()->put($code, $contents);
                $fileFrom = File::where('key', $file->key)->first();
                if ($fileFrom) {
                    $param = [
                        'code'      => $code,
                        'name'      => $fileFrom->name,
                        'key_url'   => $fileModel->genKeyUrl(32),
                        'key'       => Storage::disk()->url($code),
                        'folder_id' => $directoryTo->id,
                        'extension' => $fileFrom->extension,
                        'version'   => $fileFrom->version,
                        'size'      => Storage::disk()->size($code),
                        'title'     => $fileFrom->title,
                        'is_active' => 1,
                    ];
                    File::create($param);
                }
                if ($action == "MOVE") {
                    $file->delete();
                }
            }
        }
    }


    public function getSize($folder, &$size = 0)
    {
        if (empty($folder)) {
            return $size;
        }
        foreach ($folder->files as $file) {
            $size += $file->size ?? 0;
        }
        foreach ($folder->children as $children) {
            $this->getSize($children, $size);
        }
        return $size;
    }

    public function detailApi($input, $id)
    {
        if (empty($input['email'])) {
            throw new Exception(__('messages.unauthorized'));
        }
        $user = User::model()->whereEmail($input['email'])->first();
        if (empty($user)) {
            return [];
        }
        $folder = Folder::with(['ancestors', 'files', 'folders'])->descendantsAndSelf($id)->toTree()->first();
        if (empty($folder)) {
            throw new Exception(__('messages.not_existed', ['name' => '#ID ' . $id]));
        }
        if ($folder->created_by == $user->id || $folder->is_share == 1) {
            return $folder;
        }

        $mediaShare = MediaShare::whereFolderId($id)
            ->whereNull('division_id')
            ->whereNull('user_id')->first();
        if (!empty($mediaShare)) {
            return $folder;
        }
        $parentIds = [];
        $parentIds = $this->parentIds($id, $parentIds);
        $divisionDetailIds = Acheckin::apiDivisionDetailIds() ?? [];
        array_push($parentIds, $folder->id);
        $countFolderShare = MediaShare::where(function ($query) use ($parentIds, $divisionDetailIds, $permission) {
            $query->whereIn('folder_id', $parentIds)
                ->whereIn('division_id', $divisionDetailIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });
        })->orWhere(function ($query) use ($parentIds, $permission) {
            $query->where('user_id', auth()->id());
            $query->whereIn('folder_id', $parentIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });
        })->count();

        if ($countFolderShare == 0 && $folder->created_by != $user->id) {
            throw new Exception(__('messages.permission'));
        }

        $data = [
            "id"         => $folder->id,
            "name"       => $folder->name,
            "path"       => $folder->path,
            "full_path"  => fullPath($folder),
            "key_url"    => $folder->key_url,
            "key"        => $folder->key,
            "size"       => $folder->size,
            "children"   => $folder->children,
            "files"      => $folder->files,
            "share"      => $folder->share,
            "is_active"  => $folder->is_active,
            "created_by" => $folder->createdBy->email,
            "created_at" => $folder->created_at,
            "updated_at" => $folder->updated_at,
        ];
        return $data;
    }

    public function move($directoryFromId, $directoryToId = 0)
    {
        $directoryFrom = $this->checkPermission($directoryFromId);
        if (!empty($directoryToId)) {
            $directoryTo = $this->checkPermission($directoryToId);
        }
        $this->createChildren($directoryFrom, 'MOVE', $directoryTo);
        $this->delete($directoryFrom);
    }

    public function delete($folder)
    {
        $folder = $this->checkPermission($folder->id);
        $this->deleteFile($folder);
        $folder->delete();
    }

    public function deleteFile($folder)
    {
        if (!empty($folder->files)) {
            foreach ($folder->files as $file) {
                Storage::disk()->delete($file->code);
                $file->delete();
            }
        }

        if (!empty($folder->children)) {
            foreach ($folder->children as $children) {
                $this->delete($children);
            }
        }
    }

    // Share link
    public function shareLink($input, $id)
    {
        $folder = $this->checkPermission($id);
        $folder->permission = $input['permission'];
        $folder->is_share = 1;
        $folder->save();
        return $folder;
    }

    // Un share link
    public function unShareLink($input, $id)
    {
        $folder = Folder::model()->find($id);
        if (empty($folder)) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        if ($folder->created_by != auth()->id()) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        $folder->permission = Folder::PERMISSION_VIEW;
        $folder->is_share = 0;
        $folder->save();
        return $folder;
    }

    public function listApi($input)
    {
        if (empty($input['email'])) {
            throw new Exception(__('messages.unauthorized'));
        }

        $user = User::model()->whereEmail($input['email'])->first();
        if (empty($user)) {
            return [];
        }

        $data = [];
        $key = Arr::get($input, 'key');
        switch ($key) {
            case 'share' :
                $input['share'] = 'share';
                $data = array_merge($data, $this->loadFolder($input, $user->id));
                $data = array_merge($data, $this->loadFile($input, $user->id));
                break;
            case 'to_shared' :
                $mediaShareModel = new MediaShareModel();
                $data = $mediaShareModel->loadMediaShare($input, $user->id);
                break;
            case 'share_link' :
                $input['share_link'] = 'share_link';
                $data = array_merge($data, $this->loadFolder($input, $user->id));
                $data = array_merge($data, $this->loadFile($input, $user->id));
                break;
            default :
                $data = array_merge($data, $this->loadFolder($input, $user->id));
                $data = array_merge($data, $this->loadFile($input, $user->id));
                $input['division'] = 'division';
                $mediaShareModel = new MediaShareModel();
                $data = array_merge($data, $mediaShareModel->loadMediaShare($input, $user->id));
                break;
        }

        return $data;
    }

    public function load($input)
    {
        $data = [];
        $key = Arr::get($input, 'key');
        switch ($key) {
            case 'share' :
                $input['share'] = 'share';
                $data = array_merge($data, $this->loadFolder($input, auth()->id()));
                $data = array_merge($data, $this->loadFile($input, auth()->id()));
                break;
            case 'to_shared' :
                $mediaShareModel = new MediaShareModel();
                $data = $mediaShareModel->loadMediaShare($input, auth()->id());
                break;
            case 'share_link' :
                $input['share_link'] = 'share_link';
                $data = array_merge($data, $this->loadFolder($input, auth()->id()));
                $data = array_merge($data, $this->loadFile($input, auth()->id()));
                break;
            default :
                $data = array_merge($data, $this->loadFolder($input, auth()->id()));
                $data = array_merge($data, $this->loadFile($input, auth()->id()));
                //                $input['division'] = 'division';
                $mediaShareModel = new MediaShareModel();
                $data = array_merge($data, $mediaShareModel->loadMediaShare($input, auth()->id()));
                break;
        }
        return $data;
    }

    public function loadFolder($input, $id)
    {
        $data = [];
        $folders = Folder::with('createdBy')
            ->whereCreatedBy($id);
        if (!empty($input['share_link'])) {
            $folders->whereIsShare(1);
        }
        if (!empty($input['share'])) {
            $folders->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('media_shares')
                    ->whereRaw('folders.id = media_shares.folder_id');
            });
        }
        if (!empty($input['name'])) {
            $search = $input['name'];
            $folders->where('name', 'like', "%$search%");
        } else {
            $folders->whereNull('parent_id');
        }
        $folders = $folders->get();

        foreach ($folders as $folder) {
            $data[] = [
                "id"         => $folder->id,
                "name"       => $folder->name,
                "path"       => $folder->path,
                "full_path"  => fullPath($folder),
                "key_url"    => $folder->key_url,
                "key"        => $folder->key,
                "type"       => "FOLDER",
                "size"       => $folder->size,
                "share"      => $folder->share,
                "is_share"   => $folder->is_share,
                "is_active"  => $folder->is_active,
                "created_by" => $folder->createdBy->email,
                "created_at" => $folder->created_at,
                "updated_at" => $folder->updated_at,
            ];
        }
        return $data;
    }

    public function loadFile($input, $id)
    {
        $data = [];
        $files = File::with('createdBy')
            ->whereCreatedBy($id);
        if (!empty($input['share_link'])) {
            $files->whereIsShare(1);
        }
        if (!empty($input['share'])) {
            $files->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('media_shares')
                    ->whereRaw('files.id = media_shares.file_id');
            });
        }
        if (!empty($input['name'])) {
            $search = $input['name'];
            $files->where('name', 'like', "%$search%");
        } else {
            $files->whereNull('folder_id');
        }
        $files = $files->get();
        foreach ($files as $file) {
            $data[] = [
                "id"         => $file->id,
                "name"       => $file->name,
                "path"       => $file->path,
                "full_path"  => null,
                "key_url"    => $file->key_url,
                "key"        => $file->key,
                "type"       => "FILE",
                "size"       => $file->size,
                "share"      => $file->share,
                "is_share"   => $file->is_share,
                "is_active"  => $file->is_active,
                "created_by" => $file->createdBy->email,
                "created_at" => $file->created_at,
                "updated_at" => $file->updated_at,
            ];
        }
        return $data;
    }
}
