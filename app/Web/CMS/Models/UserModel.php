<?php

namespace App\Web\CMS\Models;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class UserModel extends AbstractModel
{
    /**
     * UserModel constructor.
     * @param User|null $model
     */
    public function __construct(User $model = null)
    {
        parent::__construct($model);
    }

    /**
     * @param $input
     * @return mixed
     * @throws \Exception
     */
    public function upsert($input)
    {
        DB::beginTransaction();

        $id = !empty($input['id']) ? $input['id'] : 0;

        if ($id) {
            // Update User
            $user = User::find($id);

            if (empty($user)) {
                throw new \Exception(__('messages.not_existed', ['name' => "ID: #$id"]));
            }
            if ($user->id != auth()->id()) {
                if (!in_array(auth()->user()->type, User::typeAdmin())) {
                    throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
                }
                if ($user->type == User::TYPE_SUPER_ADMIN && auth()->user()->type != User::TYPE_SUPER_ADMIN) {
                    throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
                }
            } else {
                $input['permission'] = $user->permission;
            }
            // username unique
            $this->checkUnique(['code' => $input['code']], $id);
            if (!empty($input['password'])) {
                $password = password_hash($input['password'], PASSWORD_BCRYPT);
                $user->password = $password;
            }
            $user->phone = Arr::get($input, 'phone', $user->phone);
            $user->code = Arr::get($input, 'code', $user->code);
            $user->username = Arr::get($input, 'username', $user->code);
            $user->email = Arr::get($input, 'email', $user->email);
            $user->role_id = Arr::get($input, 'role_id', $user->role_id);
            $user->verify_code = Arr::get($input, 'verify_code', $user->verify_code);
            $user->expired_code = Arr::get($input, 'expired_code', $user->expired_code);
            $user->is_active = Arr::get($input, 'is_active', $user->is_active);
            $user->save();
        } else {
            if (!in_array(auth()->user()->type, User::typeAdmin())) {
                throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
            }
            $type = Arr::get($input, 'type', 'UNKNOWN');
            if (auth()->user()->type == User::TYPE_ADMIN && $type == User::TYPE_SUPER_ADMIN) {
                throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
            }

            $phone = "";
            if (!empty($input['phone'])) {
                $phone = str_replace(" ", "", $input['phone']);
                $phone = preg_replace('/\D/', '', $phone);
            }
            $param = [
                'phone'        => $phone,
                'code'         => Arr::get($input, 'code'),
                'username'     => Arr::get($input, 'username', $input['code']),
                'email'        => Arr::get($input, 'email'),
                'role_id'      => Arr::get($input, 'role_id'),
                'note'         => Arr::get($input, 'note'),
                'type'         => Arr::get($input, 'type', 'UNKNOWN'),
                'verify_code'  => mt_rand(100000, 999999),
                'expired_code' => date('Y-m-d H:i:s', strtotime("+5 minutes")),
                'is_active'    => Arr::get($input, 'is_active', 1),
            ];

            if (!empty($input['password'])) {
                $param['password'] = password_hash($input['password'], PASSWORD_BCRYPT);
            }

            $y = date('Y', time());
            $m = date("m", time());
            $d = date("d", time());
            // Create User
            $user = $this->create($param);
        }

        $profile = Profile::where(['user_id' => $user->id])->first();

        $dir = !empty($input['avatar']) ? "$y/$m/$d" : null;
        $file_name = empty($dir) ? null : "avatar_{$input['phone']}";
        if ($file_name) {
            $avatars = explode("base64,", $input['avatar']);
            $input['avatar'] = $avatars[1];
            if (!empty($file_name) && !is_image($avatars[1])) {
                throw new \Exception(__('messages.invalid', ['name' => 'Avatar']));
            }
        }

        $names = explode(" ", trim($input['fullname']));
        $first = $names[count($names) -1];
        unset($names[0]);
        $last = !empty($names) ? implode(" ", $names) : null;
        $prProfile = [
            'email'      => Arr::get($input, 'email'),
            'is_active'  => 1,
            'first_name' => $first,
            'last_name'  => $last,
            'short_name' => $input['fullname'],
            'full_name'  => $input['fullname']
        ];

        $prProfile = [
            'email'      => Arr::get($input, 'email'),
            'address'    => Arr::get($input, 'address', null),
            'phone'      => Arr::get($input, 'phone', null),
            'birthday'   => empty($input['birthday']) ? null : date('Y/m/d', strtotime($input['birthday'])),
            'genre'      => Arr::get($input, 'genre', "Other"),
            'avatar'     => $file_name ? $dir . "/" . $file_name . ".jpg" : null,
            'user_id'    => $user->id,
            'is_active'  => 1,
            'first_name' => $first,
            'last_name'  => $last,
            'short_name' => $input['fullname'],
            'full_name'  => $input['fullname']
        ];

        // Create Profile
        $profileModel = new ProfileModel();
        if (empty($profile)) {
            $profileModel->create($prProfile);
        } else {
            $prProfile['id'] = $profile->id;
            $profileModel->update($prProfile);
        }

        DB::commit();

        return $user;
    }

    public function upsertApi($input)
    {
        DB::beginTransaction();
        $userCurrent = User::model()->whereEmail($input['user_email'])->first();
        if(empty($userCurrent)) {
            throw new \Exception(__('messages.not_existed', ['name' => "email: #$id"]));;
        }
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            // Update User
            $user = User::find($id);
            if ($user->id != $userCurrent->id) {
                if (!in_array($userCurrent->type, User::typeAdmin())) {
                    throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
                }
                if ($user->type == User::TYPE_SUPER_ADMIN && $userCurrent->type != User::TYPE_SUPER_ADMIN) {
                    throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
                }
            } else {
                $input['permission'] = $user->permission;
            }
            // username unique
            $this->checkUnique(['code' => $input['code']], $id);

            if (empty($user)) {
                throw new \Exception(__('messages.not_existed', ['name' => "ID: #$id"]));
            }
            if (!empty($input['password'])) {
                $password = password_hash($input['password'], PASSWORD_BCRYPT);
                $user->password = $password;
            }
            $user->phone = Arr::get($input, 'phone', $user->phone);
            $user->code = Arr::get($input, 'code', $user->code);
            $user->username = Arr::get($input, 'username', $user->code);
            $user->email = Arr::get($input, 'email', $user->email);
            $user->role_id = Arr::get($input, 'role_id', $user->role_id);
            $user->verify_code = Arr::get($input, 'verify_code', $user->verify_code);
            $user->expired_code = Arr::get($input, 'expired_code', $user->expired_code);
            $user->is_active = Arr::get($input, 'is_active', $user->is_active);
            $user->save();
        } else {
            if (!in_array($userCurrent->type, User::typeAdmin())) {
                throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
            }
            $type = Arr::get($input, 'type', 'UNKNOWN');
            if ($userCurrent->type == User::TYPE_ADMIN && $type == User::TYPE_SUPER_ADMIN) {
                throw new \Exception(__('messages.permission', ['name' => __('tables.users')]));
            }

            $phone = "";
            if (!empty($input['phone'])) {
                $phone = str_replace(" ", "", $input['phone']);
                $phone = preg_replace('/\D/', '', $phone);
            }
            $param = [
                'phone'        => $phone,
                'code'         => Arr::get($input, 'code'),
                'username'     => Arr::get($input, 'username', $input['code']),
                'email'        => Arr::get($input, 'email'),
                'role_id'      => Arr::get($input, 'role_id'),
                'note'         => Arr::get($input, 'note'),
                'type'         => Arr::get($input, 'type', 'UNKNOWN'),
                'verify_code'  => mt_rand(100000, 999999),
                'expired_code' => date('Y-m-d H:i:s', strtotime("+5 minutes")),
                'is_active'    => Arr::get($input, 'is_active', 1),
            ];

            if (!empty($input['password'])) {
                $param['password'] = password_hash($input['password'], PASSWORD_BCRYPT);
            }

            $y = date('Y', time());
            $m = date("m", time());
            $d = date("d", time());
            // Create User
            $user = $this->create($param);
        }

        $profile = Profile::where(['user_id' => $user->id])->first();

        $dir = !empty($input['avatar']) ? "$y/$m/$d" : null;
        $file_name = empty($dir) ? null : "avatar_{$input['phone']}";
        if ($file_name) {
            $avatars = explode("base64,", $input['avatar']);
            $input['avatar'] = $avatars[1];
            if (!empty($file_name) && !is_image($avatars[1])) {
                throw new \Exception(__('messages.invalid', ['name' => 'Avatar']));
            }
        }

        $names = explode(" ", trim($input['fullname']));
        $first = $names[count($names) -1];
        unset($names[0]);
        $last = !empty($names) ? implode(" ", $names) : null;
        $prProfile = [
            'email'      => Arr::get($input, 'email'),
            'is_active'  => 1,
            'first_name' => $first,
            'last_name'  => $last,
            'short_name' => $input['fullname'],
            'full_name'  => $input['fullname']
        ];

        $prProfile = [
            'email'      => Arr::get($input, 'email'),
            'address'    => Arr::get($input, 'address', null),
            'phone'      => Arr::get($input, 'phone', null),
            'birthday'   => empty($input['birthday']) ? null : date('Y/m/d', strtotime($input['birthday'])),
            'genre'      => Arr::get($input, 'genre', "Other"),
            'avatar'     => $file_name ? $dir . "/" . $file_name . ".jpg" : null,
            'user_id'    => $user->id,
            'is_active'  => 1,
            'first_name' => $first,
            'last_name'  => $last,
            'short_name' => $input['fullname'],
            'full_name'  => $input['fullname']
        ];

        // Create Profile
        $profileModel = new ProfileModel();
        if (empty($profile)) {
            $profileModel->create($prProfile);
        } else {
            $prProfile['id'] = $profile->id;
            $profileModel->update($prProfile);
        }

        DB::commit();

        return $user;
    }
}
