<?php

namespace App\Web\CMS\Models;

use App\Acheckin;
use App\Models\File;
use App\Models\Folder;
use App\Models\MediaShare;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class FileModel extends AbstractModel
{
    /**
     * FilerModel constructor.
     * @param File|null $model
     */
    public function __construct(File $model = null)
    {
        parent::__construct($model);
    }

    public function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $this->sortBuilder($query, $input);
        $query->whereFolderId(null)->whereCreatedBy(auth()->id());
        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }

    public function detail($id, $with = [])
    {
        $file = $this->checkPermission($id);
        return $file;
    }

    public function update($input)
    {
        $id = $input['id'] ? $input['id'] : 0;
        $file = $this->checkPermission($id, 'UPDATE');
        $file->name = Arr::get($input, 'name', $file->name);
        $file->folder_id = Arr::get($input, 'folder_id', $file->folder_id);
        $file->extension = Arr::get($input, 'extension', $file->extension);
        $file->title = Arr::get($input, 'title', $file->title);
        $file->save();
        return $file;
    }

    public function store($input)
    {
        $file = $input['file'];
        $folder = !empty($input['folder_id']) ? $input['folder_id'] : 0;
        $folderId = null;
        $folder = Folder::find($folder);
        if (!empty($folder)) {
            $folderId = $folder->id;
        }
        $fileName = $file->getClientOriginalName();
        $key = $this->genCode(32);
        Storage::disk()->put($key, file_get_contents($file));


        //        $code = $this->genKeyUrl(32);

        $param = [
            'code'      => $key,
            'name'      => $fileName,
            'key_url'   => $this->genKeyUrl(32),
            'key'       => Storage::disk()->url($key),
            'folder_id' => $folderId,
            'extension' => Arr::get($input, 'extension', null),
            'version'   => Arr::get($input, 'version', null),
            'size'      => Storage::disk()->size($key),
            'title'     => Arr::get($input, 'title', null),
            'is_active' => 1,
        ];
        $file = $this->create($param);
        return $file;
    }

    public function genKeyUrl($length)
    {
        $key = random_str($length);
        $file = File::getKeyUrl($key)->first();
        if ($file) {
            $this->genKeyUrl($length);
        }
        return $key;
    }

    public function genCode($length)
    {
        $key = random_str($length);
        $file = File::getCode($key)->first();
        if ($file) {
            $this->genCode($length);
        }
        return $key;
    }

    public function checkPermission($id, $permission = null)
    {
        $file = File::find($id);
        if (empty($file)) {
            throw new \Exception(__('messages.not_existed', ['name' => '#ID' . $id]));
        }
        if ($file->created_by == auth()->id()) {
            return $file;
        }
        if ($file->is_share == 1) {
            if(!empty($permission) && $file->permission != $permission && $permission == "UPDATE") {
                throw new Exception(__('messages.permission'));
            }
            return $file;
        }
        $parentIds = [];
        if (!empty($file->folder_id)) {
            $folderModel = new FolderModel();
            $parentIds = $folderModel->parentIds($file->folder_id, $parentIds);
            array_push($parentIds, $file->folder_id);
        }
        $divisionDetailIds = Acheckin::divisionDetailIds() ?? [];
        $countFileShare = MediaShare::where(function ($query) use ($parentIds, $divisionDetailIds, $permission) {
            $query->whereIn('folder_id', $parentIds)
                ->whereIn('division_id', $divisionDetailIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });
        })->orWhere(function ($query) use ($parentIds, $permission) {
            $query->where('user_id', auth()->id())
                ->whereIn('folder_id', $parentIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });
        })->orWhere(function ($query) use ($id, $permission) {
            $query->where('user_id', auth()->id())
                ->where('file_id', $id)
                ->when($permission, function ($query) {
                    return $query->wherePermission(MediaShare::PERMISSION_UPDATE);
                });

        })->count();
        if ($countFileShare == 0 && $file->created_by != auth()->id()) {
            throw new \Exception(__('messages.permission'));
        }

        return $file;
    }

    public function move($id, $folder_id = null)
    {
        $file = $this->checkPermission($id);
        if (empty($file)) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        $file->folder_id = $folder_id;
        $file->save();
        return $file;
    }

    public function copy($id, $action = "COPY", $folder_id = null)
    {
        $file = $this->checkPermission($id);
        if (empty($file)) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        $name = $file->name;
        $code = $this->genCode(32);
        $contents = Storage::get($file->code);
        Storage::disk()->put($code,$contents);
        $param = [
            'code'      => $code,
            'name'      => $name,
            'key_url'   => $this->genKeyUrl(32),
            'key'       => Storage::disk()->url($code),
            'folder_id' => $folder_id,
            'extension' => $file->extension,

            'version'   => $file->version,
            'size'      => $file->size,
            'title'     => $file->title,
            'is_active' => 1,
        ];
        $fileNew = $this->create($param);
        if($action == "MOVE")
        {
            Storage::disk()->delete($file->code);
            $file->delete();
        }
        return $fileNew;
    }

    public function delete($id)
    {
        $file = $this->checkPermission($id);
        if (empty($file)) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        Storage::disk()->delete($file->code);
        return $file->delete(); // TODO: Change the autogenerated stub
    }

    public function download($id)
    {
        $file = $this->checkPermission($id);
        if (empty($file)) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        return Storage::disk()->download($file->code, $file->name);
    }

    // Share link
    public function shareLink($input, $id) {
        $file = $this->checkPermission($id);
        $file->permission = $input['permission'];
        $file->is_share = 1;
        $file->save();
        return $file;
    }

    // Un share link
    public function unShareLink($input, $id) {
        $file = File::model()->find($id);
        if (empty($file)) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        if ($file->created_by != auth()->id()) {
            throw new \Exception(__('messages.not_existed', ['name' => "#ID $id"]));
        }
        $file->permission = Folder::PERMISSION_VIEW;
        $file->is_share = 0;
        $file->save();
        return $file;
    }

    public function preview($id)
    {
        $file = $this->checkPermission($id);
        $url = Storage::disk('s3')->temporaryUrl(
            $file->key,
            now()->addMinutes(5),
            ['ResponseContentType' => 'application/octet-stream']
        );
        return $url;
    }
}
