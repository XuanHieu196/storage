<?php

namespace App\Web\CMS\Models;

use App\Models\Division;
use App\Models\DivisionDetail;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DivisionModel extends AbstractModel
{
    /**
     * DivisionModel constructor.
     * @param Division|null $model
     */
    public function __construct(Division $model = null)
    {
        parent::__construct($model);
    }

    /**
     * @param $input
     * @return mixed
     * @throws \Exception
     */
    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        $action = "CREATE";
        if ($id) {
            $division = Division::find($id);
            if (empty($division)) {
                throw new \Exception(__('messages.not_existed', ['name' => '#ID' . $id]));
            }

            $division->name = Arr::get($input, 'name', $division->name);
            $division->description = Arr::get($input, 'description', $division->description);
            $division->type = Arr::get($input, 'type', $division->type);
            $division->updated_at = date("Y-m-d H:i:s", strtotime($input['updated_at'] ?? $division->updated_at));
            $division->updated_by = auth()->id();
            $division->save();
        } else {
            $param = [
                'code'        => $input['code'],
                'name'        => $input['name'],
                'description' => Arr::get($input, 'description'),
                'type'        => Arr::get($input, 'type', Division::TYPE_DEPARTMENT),
            ];

            $division = $this->create($param);
        }
        return $division;
    }

    public function driveShare($input, $id) {
        $mediaShareModel = new MediaShareModel();
        $data = $mediaShareModel->loadMediaShareDivision($input, $id);
        return $data;
    }
}
