<?php

namespace App\Web\CMS\Models;

use App\Acheckin;
use App\Models\File;
use App\Models\Folder;
use App\Models\MediaShare;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FolderModel2 extends AbstractModel
{
    /**
     * FolderModel constructor.
     * @param Folder|null $model
     */
    public function __construct(Folder $model = null)
    {
        parent::__construct($model);
    }

    public function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);

        $this->sortBuilder($query, $input);
        $full_columns = DB::getSchemaBuilder()->getColumnListing($this->getTable());

        $input = array_intersect_key($input, array_flip($full_columns));

        foreach ($input as $field => $value) {
            if ($value === "") {
                continue;
            }
            if (is_array($value)) {
                $query->where(function ($q) use ($field, $value) {
                    foreach ($value as $action => $data) {
                        $action = strtoupper($action);
                        if ($data === "") {
                            continue;
                        }
                        switch ($action) {
                            case "LIKE":
                                $q->orWhere(DB::raw($field), "like", "%$data%");
                                break;
                            case "IN":
                                $q->orWhereIn(DB::raw($field), $data);
                                break;
                            case "NOT IN":
                                $q->orWhereNotIn(DB::raw($field), $data);
                                break;
                            case "NULL":
                                $q->orWhereNull(DB::raw($field));
                                break;
                            case "NOT NULL":
                                $q->orWhereNotNull(DB::raw($field));
                                break;
                            case "BETWEEN":
                                $q->orWhereBetween(DB::raw($field), $value);
                                break;
                            default:
                                $q->orWhere(DB::raw($field), $action, $data);
                                break;
                        }
                    }
                });
            } else {
                $query->where(DB::raw($field), 'like', "%$value%");
            }
        }
        $query->where('created_by', auth()->id());

        if ($limit) {
            return $query->paginate($limit);
        } else {
            return $query->get()->toTree();
        }
    }

    public function detail($id)
    {
        $folder = $this->checkPermission($id);
        return $folder;
    }

    public function store($input)
    {
        $key = $this->genKeyUrl(32);
        $folderPath = str_replace(' ', '-', $input['name']);
        $param = [
            'name'      => $input['name'],
            'path'      => strtoupper($folderPath),
            'parent_id' => Arr::get($input, 'parent_id'),
            'key_url'   => $key,
            'is_active' => 1,
        ];
        $parent_id = Arr::get($input, 'parent_id');
        if (!empty($parent_id)) {
            $folderParent = Folder::find($parent_id);
            $path =  trim($folderParent->path . '/' . $folderPath,'/');
            $folderPath = str_replace(' ', '-', $input['path']);
            Storage::disk()->makeDirectory($folderPath);
            $param['path'] = $folderPath;
            $param['key'] = Storage::disk()->url($folderPath);
            $folder = $this->create($param, $folderParent);
        } else {
            $folder = $this->create($param);
        }

        return $folder;
    }

    public function updateFolder($id, $input)
    {
        $permission = "UPDATE";
        $folder = $this->checkPermission($id, $permission);
        $parent_id = Arr::get($input, 'parent_id');
        if (!empty($parent_id)) {
            $folderParent = Folder::find($parent_id);
            $folder->parent_id = $folderParent->id;
            $this->moveFolders($folder->path, $folderParent, 'MOVE');
        }
        $folder->name = Arr::get($input, 'name', $folder->name);
        $folder->key = Arr::get($input, 'key', $folder->key);
        $folder->updated_at = date("Y-m-d H:i:s", time());
        $folder->is_active = Arr::get($input, 'is_active', $folder->is_active);
        $folder->updated_by = auth()->id();
        $folder->save();

        return $folder;
    }

    private function genKeyUrl($length)
    {
        $key = random_str($length);
        $folder = Folder::getKeyUrl($key)->first();
        if ($folder) {
            $this->genKeyUrl($length);
        }
        return $key;
    }

    public function parentIds($id, &$ids)
    {
        $forlder = Folder::find($id);

        if (!empty($forlder) && !empty($forlder->parent)) {
            $forlderParent = $forlder->parent;
            array_push($ids, $forlderParent->id);
            $this->parentIds($forlderParent->id, $ids);
        }
        return $ids;
    }

    public function checkPermission($id, $permission = null)
    {
        $folder = Folder::with(['ancestors', 'files'])->descendantsAndSelf($id)->toTree()->first();
        if (empty($folder)) {
            throw new \Exception(__('messages.not_existed', ['name' => '#ID ' . $id]));
        }
        if ($folder->created_by == auth()->id()) {
            return $folder;
        }
        $parentIds = [];
        $parentIds = $this->parentIds($id, $parentIds);
        $divisionDetailIds = Acheckin::divisionDetailIds() ?? [];
        array_push($parentIds, $folder->id);
        $countFolderShare = MediaShare::where(function ($query) use ($parentIds, $divisionDetailIds, $permission) {
            $query->whereIn('folder_id', $parentIds)
                ->whereIn('division_id', $divisionDetailIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission('UPDATE');
                });
        })->orWhere(function ($query) use ($parentIds, $permission) {
            $query->where('user_id', auth()->id());
            $query->whereIn('folder_id', $parentIds)
                ->when($permission, function ($query) {
                    return $query->wherePermission('UPDATE');
                });
        })->count();

        if ($countFolderShare == 0 && $folder->created_by != auth()->id()) {
            throw new \Exception(__('messages.permission'));
        }

        return $folder;
    }

    public function getFolderPath($id, &$path)
    {
        $folder = Folder::find($id);
        if (empty($folder)) {
            return $path;
        }
        $path = $folder->path . '/' . $path;
        if (!empty($folder->parent_id)) {
            $this->getFolderPath($folder->parent_id, $path);
        }
        $path = rtrim($path, '/');
        return $path;
    }

    public function moveFiles($directoryFrom, $directoryTo, $action = "COPY")
    {
        $files = Storage::disk()->allFiles($directoryFrom);
        foreach ($files as $file) {
            $moveTo = str_replace($directoryFrom, $directoryTo, $file);
            if ($action == "COPY") {
                Storage::disk()->copy($file, $moveTo);
                $fileFrom = File::where('key', $file)->first();
                if ($fileFrom) {
                    $fileModel = new FileModel();
                    $param = [
                        'code'      => $fileModel->genCode(10),
                        'name'      => $fileFrom->name,
                        'key_url'   => $fileModel->genKeyUrl(32),
                        'key'       => Storage::disk()->url($moveTo),
                        'folder_id' => $fileFrom->folder_id,
                        'extension' => $fileFrom->extension,
                        'version'   => $fileFrom->version,
                        'size'      => Storage::disk()->size($moveTo),
                        'title'     => $fileFrom->title,
                        'is_active' => 1,
                    ];
                    File::create($param);
                }
            } else {
                Storage::disk()->move($file, $moveTo);
            }
        }
    }

    public function moveFolders($directoryFrom, $folderTo, $action = 'COPY')
    {
        $folders = Storage::disk()->allDirectories($directoryFrom);
        foreach ($folders as $folder) {
            $moveTo = str_replace($directoryFrom, $folderTo->path, $folder);
            Storage::disk()->makeDirectory($moveTo);
            $folderPath = str_replace(' ', '-', $folder->name);
            $folder->path = trim($folderTo->path . '/' . $folderPath,'/');
            $this->moveFiles($folder, $moveTo, $action);
        }
        if ($action == 'MOVE') {
            Storage::disk()->deleteDirectory($directoryFrom);
        }
    }

    private function loadFolderZip($id, \ZipArchive $zip, $zipName)
    {
        $folder = Folder::descendantsAndSelf($id)->toTree()->first();
        if ($zip->open($zipName) === TRUE) {
            if (!$zip->addEmptyDir($folder->name)) {
                throw new \Exception(__('messages.zip_addFolder'));
            }
            foreach ($folder->files as $file) {
                $zip->addFile($file->key, $file->name);
                $zip->close();
            }
            foreach ($folder->children as $children) {
                $this->loadFolder($children->id, $zip, $zipName);
            }
        } else {
            throw new \Exception(__('messages.zip_add_older_fail'));
        }
    }

    public function download($id)
    {
        $folder = Folder::descendantsAndSelf($id)->toTree()->first();
        if (empty($folder)) {
            throw new \Exception(__('messages.download_fail'));
        }
        $zipname = $folder->name . '.zip';
        $zip = new \ZipArchive();
        $zip->open($zipname, \ZipArchive::CREATE);
        $this->loadFolderZip($folder->id, $zip, $zipname);

        $zip->close();
        return $zipname;
    }


    public function childrens($forder, $folderTo)
    {
        $key = $this->genKeyUrl(32);
        $path = explode('/', $forder->path);
        $path = $folderTo . '/' . array_pop($path);
        $param = [
            'name'      => $forder->name,
            'path'      => $path,
            'key_url'   => $key,
            'is_active' => 1,
        ];

        $param['key'] = Storage::disk()->url($path);
        $folderNew = $this->create($param, $folderTo);
        foreach ($forder->chidren as $chidren) {
            $this->childrens($chidren, $folderNew);
        }
        return $folderNew;
    }

    public function copy($directoryFromId, $directoryToId)
    {
        $directoryFrom = Folder::descendantsAndSelf($directoryFromId)->toTree()->first();
        $directoryTo = Folder::descendantsAndSelf($directoryToId)->toTree()->first();
        if(empty($directoryFrom) || empty($directoryTo)) {
            throw new \Exception(__('messages.copy_fail', ['name'=>'folder']));
        }
        $this->childrens($directoryFrom, $directoryTo);
        $this->moveFolders($directoryFrom->path, $directoryTo, 'COPY');
        return true;
    }
}
