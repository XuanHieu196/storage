<?php

namespace App\Web\CMS\Models;


use App\Profile;

class ProfileModel extends AbstractModel
{
    /**
     * CityModel constructor.
     * @param Profile|null $model
     */
    public function __construct(Profile $model = null)
    {
        parent::__construct($model);
    }
}
