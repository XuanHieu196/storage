<?php

namespace App\Web\CMS\Models;

use App\Models\Division;
use App\Models\DivisionDetail;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DivisionDetailModel extends AbstractModel
{
    /**
     * DivisionDetailModel constructor.
     * @param DivisionDetail $model
     */
    public function __construct(DivisionDetail $model = null)
    {
        parent::__construct($model);
    }

    /**
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        $this->checkPermision($input['division_id']);
        if ($id) {
            $divisionDetail = DivisionDetail::find($id);
            if (empty($divisionDetail)) {
                throw new \Exception(__('messages.not_existed', ['name' => '#ID' . $id]));
            }
            $divisionDetail->type = Arr::get($input, 'type', DivisionDetail::TYPE_EMPLOYEE);
            $divisionDetail->description = Arr::get($input, 'description', $divisionDetail->description);
            $divisionDetail->start_time = date("Y-m-d H:i:s", strtotime($input['start_time'] ?? $divisionDetail->start_time));
            $divisionDetail->end_time = date("Y-m-d H:i:s", strtotime($input['end_time'] ?? $divisionDetail->end_time));
            $divisionDetail->save();
        } else {
            $userIds = $input['user_id'];
            foreach ($userIds as $userId) {
                $divisionDetail = DivisionDetail::whereDivisionId($input['division_id'])->whereUserId($userId)->first();
                if(!empty($divisionDetail)) {
                    $user = User::model()->find($userId);
                    throw new \Exception(__('messages.existed', ['name' => '#user ' . $user->email]));
                }
                $param = [
                    'user_id'     => $userId,
                    'division_id' => $input['division_id'],
                    'type'        => Arr::get($input, 'type', DivisionDetail::TYPE_EMPLOYEE),
                    'description' => Arr::get($input, 'description'),
                    'start_time'  => !empty($input['start_time']) ? date('Y-m-d H:i:s', strtotime($input['start_time'])) : null,
                    'end_time'    => !empty($input['end_time']) ? date('Y-m-d H:i:s', strtotime($input['end_time'])) : null,
                ];
                 DivisionDetail::create($param);
            }
        }

        return true;
    }

    public static final function checkPermision($division_id)
    {
        $division = Division::find($division_id);
        if(empty($division)) {
            throw new \Exception(__('messages.not_existed', ['name' => '#ID' . $division_id]));
        }
        $divisionDetailLeader = DivisionDetail::whereDivisionId($division_id)
            ->whereUserId(auth()->id())
            ->whereType(DivisionDetail::TYPE_LEADER)->first();

        if ($division->created_by != auth()->id() && empty($divisionDetailLeader)) {
            throw new \Exception(__('messages.permission'));
        }

        return $division;
    }
}
