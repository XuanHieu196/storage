<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property char $code code
 * @property varchar $name name
 * @property text $description description
 * @property tinyint $is_active is active
 * @property int $level level
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Collection $rolePermission hasMany
 * @property Collection $user hasMany
 */
class Role extends BaseModel
{

    /**
     * Database table name
     */
    protected $table = 'roles';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'code',
                           'name',
                           'description',
                           'is_active',
                           'level',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * rolePermissions
     *
     * @return HasMany
     */
    public function rolePermissions()
    {
        return $this->hasMany(RolePermission::class, 'role_id');
    }

    /**
     * users
     *
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'role_id');
    }


}
