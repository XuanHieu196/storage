<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property varchar $name name
 * @property char $code code
 * @property text $description description
 * @property tinyint $is_active is active
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Collection $permission hasMany
 */
class PermissionGroup extends BaseModel
{

    /**
     * Database table name
     */
    protected $table = 'permission_groups';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'name',
                           'code',
                           'description',
                           'is_active',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * permissions
     *
     * @return HasMany
     */
    public function permissions()
    {
        return $this->hasMany(Permission::class, 'group_id');
    }


}
