<?php

namespace App\Models;


/**
 * @property varchar $name name
 * @property char $code code
 * @property enum $type type
 * @property text $description description
 * @property bigint $group_id group id
 * @property tinyint $is_active is active
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Group $permissionGroup belongsTo
 * @property \Illuminate\Database\Eloquent\Collection $divisionPermission hasMany
 * @property \Illuminate\Database\Eloquent\Collection $rolePermission hasMany
 */
class Permission extends BaseModel
{
    const TYPE_DEPARTMENT = 'DEPARTMENT';

    const TYPE_ROLE = 'ROLE';

    /**
     * Database table name
     */
    protected $table = 'permissions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'name',
                           'code',
                           'type',
                           'description',
                           'group_id',
                           'is_active',
                           'created_by',
                           'updated_by',
    ];
    /**
     * casts
     */

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(PermissionGroup::class, 'group_id');
    }

    /**
     * divisionPermissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function divisionPermissions()
    {
        return $this->hasMany(DivisionPermission::class, 'permission_id');
    }

    /**
     * rolePermissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rolePermissions()
    {
        return $this->hasMany(RolePermission::class, 'permission_id');
    }


}
