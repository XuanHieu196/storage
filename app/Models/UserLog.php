<?php

namespace App\Models;


/**
 * @property varchar $action action
 * @property varchar $target target
 * @property varchar $ip ip
 * @property text $description description
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 */
class UserLog extends BaseModel
{

    /**
     * Database table name
     */
    protected $table = 'user_logs';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'action',
                           'target',
                           'ip',
                           'description',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = [];


}
