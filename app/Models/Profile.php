<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property bigint $user_id user id
 * @property varchar $email email
 * @property varchar $first_name first name
 * @property varchar $last_name last name
 * @property varchar $full_name full name
 * @property varchar $address address
 * @property varchar $phone phone
 * @property datetime $birthday birthday
 * @property enum $genre genre
 * @property text $avatar avatar
 * @property tinyint $is_active is active
 * @property timestamp $created_at created at
 * @property varchar $created_by created by
 * @property timestamp $updated_at updated at
 * @property varchar $updated_by updated by
 * @property timestamp $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property User $user belongsTo
 */
class Profile extends BaseModel
{
    const GENRE_MALE = 'Male';

    const GENRE_FEMALE = 'Female';

    const GENRE_OTHER = 'Other';

    /**
     * Database table name
     */
    protected $table = 'profiles';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'user_id',
                           'email',
                           'first_name',
                           'last_name',
                           'full_name',
                           'address',
                           'phone',
                           'birthday',
                           'genre',
                           'avatar',
                           'is_active',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = ['birthday'];

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


}
