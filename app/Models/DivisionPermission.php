<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property bigint $detail_id detail id
 * @property bigint $permission_id permission id
 * @property text $description description
 * @property tinyint $is_active is active
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Permission $permission belongsTo
 * @property Detail $divisionDetail belongsTo
 */
class DivisionPermission extends BaseModel
{

    /**
     * Database table name
     */
    protected $table = 'division_permissions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'detail_id',
                           'permission_id',
                           'description',
                           'is_active',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * permission
     *
     * @return BelongsTo
     */
    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }

    /**
     * detail
     *
     * @return BelongsTo
     */
    public function detail()
    {
        return $this->belongsTo(DivisionDetail::class, 'detail_id');
    }


}
