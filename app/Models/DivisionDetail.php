<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property bigint $user_id user id
 * @property bigint $division_id division id
 * @property enum $type type
 * @property varchar $description description
 * @property datetime $start_time start time
 * @property datetime $end_time end time
 * @property tinyint $is_active is active
 * @property timestamp $created_at created at
 * @property varchar $created_by created by
 * @property timestamp $updated_at updated at
 * @property varchar $updated_by updated by
 * @property timestamp $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Division $division belongsTo
 * @property User $user belongsTo
 * @property Collection $divisionPermission hasMany
 */
class DivisionDetail extends BaseModel
{
    const TYPE_LEADER = 'LEADER';

    const TYPE_EMPLOYEE = 'EMPLOYEE';

    /**
     * Database table name
     */
    protected $table = 'division_details';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['user_id',
                           'division_id',
                           'type',
                           'description',
                           'start_time',
                           'end_time',
                           'is_active',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = ['start_time',
                        'end_time'];

    /**
     * division
     *
     * @return BelongsTo
     */
    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * divisionPermissions
     *
     * @return HasMany
     */
    public function divisionPermissions()
    {
        return $this->hasMany(DivisionPermission::class, 'detail_id');
    }
}
