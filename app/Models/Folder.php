<?php

namespace App\Models;

use App\Acheckin;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kalnoy\Nestedset\NodeTrait;

/**
 * @property varchar $name name
 * @property varchar $path path
 * @property bigint $parent_id parent id
 * @property char $key key
 * @property tinyint $is_active is active
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Parent $folder belongsTo
 * @property Collection $file hasMany
 * @property Collection $folders hasMany
 * @property Collection $mediaShare hasMany
 */
class Folder extends BaseModel
{
    use NodeTrait;

    /**
     * Database table name
     */
    protected $table = 'folders';

    const PERMISSION_VIEW = 'VIEW';

    const PERMISSION_COMMENT = 'COMMENT';

    const PERMISSION_UPDATE = 'UPDATE';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'path',
        'parent_id',
        'key_url',
        'key',
        'is_active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * parent
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Folder::class, 'parent_id');
    }

    /**
     * files
     *
     * @return HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class, 'folder_id');
    }

    /**
     * folders
     *
     * @return HasMany
     */
    public function folders()
    {
        return $this->hasMany(Folder::class, 'parent_id');
    }

    /**
     * mediaShares
     *
     * @return HasMany
     */
    public function mediaShares()
    {
        return $this->hasMany(MediaShare::class, 'folder_id');
    }

    /**
     * children
     *
     * @return HasMany
     */

    public function getLftName()
    {
        return '_lft';
    }

    public function getRgtName()
    {
        return '_rgt';
    }

    public function getParentIdName()
    {
        return 'parent_id';
    }

    // Specify parent id attribute mutator
    public function setParentAttribute($value)
    {
        $this->setParentIdAttribute($value);
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'id', 'created_by');
    }

    public function getSizeAttribute()
    {
        return Acheckin::sizeTotalFolder($this->id);
    }

    public function getShareAttribute()
    {
        $result = [];
        foreach ($this->mediaShares as $mediaShare) {
            $user = $mediaShare->user;
            $division = $mediaShare->division;
            $permission = $mediaShare->permission;
            $result[] = [
                'user_id'     => $user->id ?? null,
                'division_id' => $division->id ?? null,
                'permission'  => $permission,
                'id'          => $mediaShare->id,
            ];
        }
        return $result;
    }

    public function getDriveShareAttribute()
    {
        $result = [];
        $userViews = [];
        $userComments = [];
        $userUpdates = [];
        $divisionViews = [];
        $divisionComments = [];
        $divisionUpdates = [];
        foreach ($this->mediaShares as $mediaShare) {
            if ($mediaShare->permission == MediaShare::PERMISSION_UPDATE) {
                if (!empty($mediaShare->user_id)) {
                    array_push($userUpdates, $mediaShare->user_id);
                }
                if (!empty($mediaShare->division_id)) {
                    array_push($divisionUpdates, $mediaShare->division_id);
                }
            }
            if ($mediaShare->permission == MediaShare::PERMISSION_VIEW) {
                if (!empty($mediaShare->user_id)) {
                    array_push($userViews, $mediaShare->user_id);
                }
                if (!empty($mediaShare->division_id)) {
                    array_push($divisionViews, $mediaShare->division_id);
                }
            }
            if ($mediaShare->permission == MediaShare::PERMISSION_COMMENT) {
                if (!empty($mediaShare->user_id)) {
                    array_push($userComments, $mediaShare->user_id);
                }
                if (!empty($mediaShare->division_id)) {
                    array_push($divisionComments, $mediaShare->division_id);
                }
            };
        }
        $result[] = [
            'view'    => [
                'users'       => $userViews,
                'division_id' => $divisionViews,
            ],
            'update'  => [
                'users'       => $userUpdates,
                'division_id' => $divisionUpdates,
            ],
            'comment' => [
                'users'       => $userComments,
                'division_id' => $divisionComments,
            ]
        ];
        return $result;
    }
}
