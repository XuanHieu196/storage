<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property varchar $code code
 * @property varchar $name name
 * @property varchar $title title
 * @property bigint $folder_id folder id
 * @property varchar $extension extension
 * @property int $version version
 * @property varchar $size size
 * @property tinyint $is_active is active
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Folder $folder belongsTo
 * @property Collection $mediaShare hasMany
 */
class File extends BaseModel
{
    const PERMISSION_VIEW = 'VIEW';

    const PERMISSION_COMMENT = 'COMMENT';

    const PERMISSION_UPDATE = 'UPDATE';

    /**
     * Database table name
     */
    protected $table = 'files';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
                           'code',
                           'name',
                           'key_url',
                           'key',
                           'title',
                           'folder_id',
                           'extension',
                           'version',
                           'size',
                           'is_active'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * folder
     *
     * @return BelongsTo
     */
    public function folder()
    {
        return $this->belongsTo(Folder::class, 'folder_id');
    }

    /**
     * mediaShares
     *
     * @return HasMany
     */
    public function mediaShares()
    {
        return $this->hasMany(MediaShare::class, 'file_id');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'key_url';
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'id', 'created_by');
    }

    public function getShareAttribute()
    {
        $result = [];
        foreach ($this->mediaShares as $mediaShare) {
            $user = $mediaShare->user;
            $division = $mediaShare->division;
            $permission = $mediaShare->permission;
            $result[] = [
                'user_id'     => $user->id ?? null,
                'division_id' => $division->id ?? null,
                'permission'  => $permission,
            ];
        }
        return $result;
    }

    public function getDriveShareAttribute()
    {
        $result = [];
        $userViews = [];
        $userComments = [];
        $userUpdates = [];
        $divisionViews = [];
        $divisionComments = [];
        $divisionUpdates = [];
        foreach ($this->mediaShares as $mediaShare) {
            if ($mediaShare->permission == MediaShare::PERMISSION_UPDATE) {
                if (!empty($mediaShare->user_id)) {
                   array_push($userUpdates, $mediaShare->user_id);
                }
                if (!empty($mediaShare->division_id)) {
                    array_push($divisionUpdates, $mediaShare->division_id);
                }
            }
            if ($mediaShare->permission == MediaShare::PERMISSION_VIEW) {
                if (!empty($mediaShare->user_id)) {
                    array_push($userViews, $mediaShare->user_id);
                }
                if (!empty($mediaShare->division_id)) {
                    array_push($divisionViews, $mediaShare->division_id);
                }
            }
            if ($mediaShare->permission == MediaShare::PERMISSION_COMMENT) {
                if (!empty($mediaShare->user_id)) {
                    array_push($userComments, $mediaShare->user_id);
                }
                if (!empty($mediaShare->division_id)) {
                    array_push($divisionComments, $mediaShare->division_id);
                }
            };
        }
        $result[] = [
            'view'    => [
                'users'       => $userViews,
                'division_id' => $divisionViews,
            ],
            'update'  => [
                'users'       => $userUpdates,
                'division_id' => $divisionUpdates,
            ],
            'comment' => [
                'users'       => $userComments,
                'division_id' => $divisionComments,
            ]
        ];
        return $result;
    }
}
