<?php

namespace App\Models;

/**
 * @property char $code code
 * @property varchar $name name
 * @property varchar $description description
 * @property enum $type type
 * @property tinyint $is_active is active
 * @property timestamp $created_at created at
 * @property varchar $created_by created by
 * @property timestamp $updated_at updated at
 * @property varchar $updated_by updated by
 * @property timestamp $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property \Illuminate\Database\Eloquent\Collection $divisionDetail hasMany
 * @property \Illuminate\Database\Eloquent\Collection $mediaShare hasMany
 */
class Division extends BaseModel
{
    const TYPE_DEPARTMENT = 'DEPARTMENT';

    const TYPE_PROJECT = 'PROJECT';

    const TYPE_TEAM = 'TEAM';

    /**
     * Database table name
     */
    protected $table = 'divisions';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'deleted_by',
        'code',
        'name',
        'description',
        'type',
        'is_active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * divisionDetails
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function divisionDetails()
    {
        return $this->hasMany(DivisionDetail::class, 'division_id');
    }

    /**
     * mediaShares
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mediaShares()
    {
        return $this->hasMany(MediaShare::class, 'division_id');
    }


}
