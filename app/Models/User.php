<?php

namespace App\Models;

use App\Acheckin;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;

/**
 * @property char $code code
 * @property varchar $username username
 * @property varchar $email email
 * @property varchar $password password
 * @property varchar $phone phone
 * @property enum $type type
 * @property varchar $verify_code verify code
 * @property datetime $expired_code expired code
 * @property bigint $role_id role id
 * @property varchar $note note
 * @property tinyint $is_active is active
 * @property timestamp $created_at created at
 * @property varchar $created_by created by
 * @property timestamp $updated_at updated at
 * @property varchar $updated_by updated by
 * @property timestamp $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Role $role belongsTo
 * @property Collection $divisionDetail hasMany
 * @property Collection $mediaShare hasMany
 * @property Collection $profile hasMany
 */
class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use \Illuminate\Auth\Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable;
    const TYPE_ADMIN = 'ADMIN';

    const TYPE_SUPER_ADMIN = 'SUPER_ADMIN';

    const TYPE_SALE = 'SALE';

    const TYPE_ACCOUNTANT = 'ACCOUNTANT';

    const TYPE_UNKNOWN = 'UNKNOWN';

    /**
     * Database table name
     */
    protected $table = 'users';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'code',
                           'username',
                           'email',
                           'phone',
                           'password',
                           'type',
                           'verify_code',
                           'expired_code',
                           'role_id',
                           'note',
                           'is_active',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = ['expired_code'];

    protected $hidden = ['password'];

    protected $appends = ['size'];

    /**
     * role
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    /**
     * divisionDetails
     *
     * @return HasMany
     */
    public function divisionDetails()
    {
        return $this->hasMany(DivisionDetail::class, 'user_id');
    }

    /**
     * mediaShares
     *
     * @return HasMany
     */
    public function mediaShares()
    {
        return $this->hasMany(MediaShare::class, 'user_id');
    }

    /**
     * profiles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profiles()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public static final function typeAdmin()
    {
        $result = [self::TYPE_SUPER_ADMIN, self::TYPE_ADMIN];

        return $result;
    }

    public function getSizeAttribute()
    {
        return Acheckin::sizeTotal();
    }

    public static final function getTypesAttribute()
    {
        $result = [
            self::TYPE_SUPER_ADMIN,
            self::TYPE_ADMIN,
            self::TYPE_ACCOUNTANT,
            self::TYPE_SALE,
            self::TYPE_UNKNOWN
        ];
        return $result;
    }

}
