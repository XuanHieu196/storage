<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property bigint $division_id division id
 * @property bigint $file_id file id
 * @property bigint $folder_id folder id
 * @property bigint $user_id user id
 * @property enum $permission permission
 * @property tinyint $is_active is active
 * @property tinyint $deleted deleted
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property User $user belongsTo
 * @property Folder $folder belongsTo
 * @property File $file belongsTo
 * @property Division $division belongsTo
 */
class MediaShare extends BaseModel
{
    const PERMISSION_VIEW = 'VIEW';

    const PERMISSION_COMMENT = 'COMMENT';

    const PERMISSION_UPDATE = 'UPDATE';

    /**
     * Database table name
     */
    protected $table = 'media_shares';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'division_id',
                           'file_id',
                           'folder_id',
                           'user_id',
                           'permission',
                           'is_active',
                           'deleted',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * folder
     *
     * @return BelongsTo
     */
    public function folder()
    {
        return $this->belongsTo(Folder::class, 'folder_id');
    }

    /**
     * file
     *
     * @return BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    /**
     * division
     *
     * @return BelongsTo
     */
    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }

    public function createdBy()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'id', 'created_by');
    }

    public static final function types()
    {
        return [self::PERMISSION_VIEW, self::PERMISSION_COMMENT, self::PERMISSION_UPDATE];
    }
}
