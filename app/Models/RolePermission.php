<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property bigint $role_id role id
 * @property bigint $permission_id permission id
 * @property text $description description
 * @property tinyint $is_active is active
 * @property datetime $created_at created at
 * @property varchar $created_by created by
 * @property datetime $updated_at updated at
 * @property varchar $updated_by updated by
 * @property datetime $deleted_at deleted at
 * @property varchar $deleted_by deleted by
 * @property Permission $permission belongsTo
 * @property Role $role belongsTo
 */
class RolePermission extends BaseModel
{

    /**
     * Database table name
     */
    protected $table = 'role_permissions';

    /**
     * Mass assignable columns
     */
    protected $fillable = ['deleted_by',
                           'role_id',
                           'permission_id',
                           'description',
                           'is_active',
                           'created_by',
                           'updated_by',
                           'deleted_by'];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * permission
     *
     * @return BelongsTo
     */
    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }

    /**
     * role
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }


}
