<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class DRIVE_ERROR
{
    public static function handle(\Exception $exception)
    {
        $errorCode = $exception->getCode();
        $errorCode = empty($errorCode) ? Response::HTTP_BAD_REQUEST : $errorCode;

        if (env('APP_DEBUG', false) == true) {

            $request = Request::capture();
            $param = $request->all();
            $data = [
                'time'    => date("Y-m-d H:i:s", time()),
                'user_id' => auth()->id(),
                'param'   => json_encode($param),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine(),
//                'error'   => $exception->getMessage(),
            ];
            return ['message' => $exception->getMessage(), 'code' => $data];
        }
        if (env('APP_ENV') == 'local') {
            return ['message' => $exception->getMessage(), 'code' => $errorCode];
        } else {
            return ['message' => __('messages.app_fail'), 'code' => Response::HTTP_BAD_REQUEST];
        }

    }
}
