<?php

namespace App\Exceptions;

use Cassandra\Exception\UnpreparedException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;
use Illuminate\Http\Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
         if ($exception instanceof \ErrorException) {
             $response =  DRIVE_ERROR::handle($exception);
             return response()->json([
                 'message' => $response['message'],
                 'code' => $response['code'],
                 'status_code' => Response::HTTP_BAD_REQUEST,
             ], Response::HTTP_BAD_REQUEST);
         }
         if ($exception instanceof QueryException) {
             $response =  DRIVE_ERROR::handle($exception);
             return response()->json([
                 'message' => $response['message'],
                 'code' => $response['code'],
                 'status_code' => Response::HTTP_BAD_REQUEST,
             ], Response::HTTP_BAD_REQUEST);
         }
//         if ($exception instanceof \Exception) {
//             $response =  DRIVE_ERROR::handle($exception);
//             return response()->json([
//                 'message' => $response['message'],
//                 'code' => $response['code'],
//                 'status_code' => Response::HTTP_BAD_REQUEST,
//             ], Response::HTTP_BAD_REQUEST);
//         }

        return parent::render($request, $exception);
    }
}
